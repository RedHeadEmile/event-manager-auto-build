# EventManager

## Project dependencies

- Java 21
- Gradle 8.3

## Development setup

- You need to install [NSwag](https://github.com/RicoSuter/NSwag/releases) to be able to debug the project. It permits to generate the Angular Api service file.
- Add the plugin/extension `manifold` to your IDE to permit the use of extension methods in the Java project.
- Create a MySQL database with InnoDB as default engine, `utf8mb4_unicode_520_ci` as default collation and default row format as `Dynamic`.
- In the directory `src/main/resources`, duplicate the file `application-template.properties` under the same `application.properties`. Fill the file you just duplicated.
- Run `npm install` in the directory `src/main/angular` then to run the client website, run `ng serve`.

## Deployment

- At the same level as `application.properties`, create `application.prop.properties` with the production settings.
- Run the `build.bat` script
- The `output` directory contains the built project which can be run with `docker compose up` for the backend part
