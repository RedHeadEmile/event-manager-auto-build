package fr.iutdijon.eventmanager.exceptions;

import lombok.Getter;

/**
 * Must be thrown if a logging is required
 */
@Getter
public class UnauthorizedException extends IllegalStateException {
    private final Integer remainingLoginAttempts;

    public UnauthorizedException() {
        super();
        this.remainingLoginAttempts = null;
    }

    public UnauthorizedException(int remainingLoginAttempts) {
        super();
        this.remainingLoginAttempts = remainingLoginAttempts;
    }
}
