package fr.iutdijon.eventmanager.exceptions;

public enum BadRequestDetail {

    INVALID_PARAMETER("Missing or incorrect type parameter"),
    EMAIL_ALREADY_IN_USE("Email already in use"),
    INVALID_DOCUMENT_MIME_TYPE("The document mime type is not allowed"),
    INVALID_EMAIL("The email parameters are invalid"),
    UNKNOWN_AUTHENTICATION_TYPE("Unknown authentication request type"),
    INVALID_DOCUMENT_CONTENT("Invalid document content"),
    DOCUMENT_TOO_BIG("The document is too big"),
    INVALID_EMAIL_CONFIRMATION_TOKEN("Invalid email confirmation token"),
    EMAIL_ALREADY_CONFIRMED("Email already confirmed"),
    INVALID_TIMELINE_EVENT_DATES("Invalid event dates"),
    USER_ALREADY_IN_A_TEAM("Cannot change the team of the user"),
    INVALID_EMAIL_FORMAT("Invalid email format"),
    INVALID_EVENT_SIZE_TEAM("Invalid event size team"),
    MISSING_TEAM_REQUIREMENT("Missing team requirements"),
    UNKNOWN_TEAM("Unknown team"),
    EVENT_ALREADY_JOINED("Event already joined"),
    CANNOT_BE_DELETED("Cannot delete this resource"),
    UNKNOWN_EVENT("Unknown event"),
    UNKNOWN_EVENT_DOCUMENT_REQUIREMENT("Unknown event document requirement"),
    MEMBER_OF_A_TEAM("Cannot leave the event because the user is member of a team");

    public final String errorMessage;

    /**
     * Represent a bad request detailed explanation
     * @param errorMessage A human-readable message to find out what caused the bad request
     */
    BadRequestDetail(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Get an exception instance from this {@link BadRequestDetail}
     * @return An exception instance corresponding to this {@link BadRequestDetail}
     */
    public BadRequestException toException() {
        return new BadRequestException(this);
    }
}
