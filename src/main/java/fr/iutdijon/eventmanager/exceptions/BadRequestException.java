package fr.iutdijon.eventmanager.exceptions;

import lombok.Getter;

/**
 * Must be thrown in case of wrong user-input.
 * Can be instantiated with {@link BadRequestDetail#toException()}
 */
@Getter
public class BadRequestException extends IllegalStateException {
    private final String message;
    private final BadRequestDetail detail;

    BadRequestException(BadRequestDetail detail) {
        this.message = null;
        this.detail = detail;
    }

    public BadRequestException(String message) {
        this.message = message;
        this.detail = null;
    }
}
