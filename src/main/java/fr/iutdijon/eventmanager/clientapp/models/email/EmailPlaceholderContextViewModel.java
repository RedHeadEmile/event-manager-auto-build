package fr.iutdijon.eventmanager.clientapp.models.email;

import fr.iutdijon.eventmanager.services.email.models.business.EmailPlaceholderContextBusinessModel;

public enum EmailPlaceholderContextViewModel {
    ANY,
    EVENT,
    TEAM;

    public EmailPlaceholderContextBusinessModel toBusinessModel() {
        return switch (this) {
            case ANY -> EmailPlaceholderContextBusinessModel.ANY;
            case EVENT -> EmailPlaceholderContextBusinessModel.EVENT;
            case TEAM -> EmailPlaceholderContextBusinessModel.TEAM;
        };
    }
}
