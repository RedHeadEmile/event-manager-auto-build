package fr.iutdijon.eventmanager.clientapp.models.user;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailConfirmationRequestViewModel {
    @NotNull
    @NotBlank
    private String emailConfirmationToken;
}
