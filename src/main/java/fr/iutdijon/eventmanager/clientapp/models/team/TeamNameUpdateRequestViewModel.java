package fr.iutdijon.eventmanager.clientapp.models.team;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamNameUpdateRequestViewModel {
    private String newTeamName;
}
