package fr.iutdijon.eventmanager.clientapp.models.event;

import fr.iutdijon.eventmanager.services.event.models.business.EventUserDocumentBusinessModel;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventUserDocumentViewModel {
    @NotNull
    private Long eventDocumentRequirementId;
    private Long documentId;
    private String documentMimeType;
    private String newDocumentContent;

    public EventUserDocumentBusinessModel toBusinessModel() {
        return new EventUserDocumentBusinessModel() {{
            setEventDocumentRequirementId(EventUserDocumentViewModel.this.getEventDocumentRequirementId());
            setDocumentId(EventUserDocumentViewModel.this.getDocumentId());
            setDocumentMimeType(EventUserDocumentViewModel.this.getDocumentMimeType());
            setNewDocumentContent(EventUserDocumentViewModel.this.getNewDocumentContent());
        }};
    }

    public static EventUserDocumentViewModel fromBusinessModel(EventUserDocumentBusinessModel businessModel) {
        return new EventUserDocumentViewModel() {{
            setEventDocumentRequirementId(businessModel.getEventDocumentRequirementId());
            setDocumentId(businessModel.getDocumentId());
            setDocumentMimeType(businessModel.getDocumentMimeType());
            setNewDocumentContent(businessModel.getNewDocumentContent());
        }};
    }
}
