package fr.iutdijon.eventmanager.clientapp.models.team;

import fr.iutdijon.eventmanager.services.team.models.business.TeamLiteBusinessModel;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamLiteViewModel {
    @NotNull
    private Long id;
    @NotNull
    private Long eventId;

    public static TeamLiteViewModel fromBusinessModel(TeamLiteBusinessModel businessModel) {
        return new TeamLiteViewModel() {{
            setId(businessModel.getId());
            setEventId(businessModel.getEventId());
        }};
    }
}
