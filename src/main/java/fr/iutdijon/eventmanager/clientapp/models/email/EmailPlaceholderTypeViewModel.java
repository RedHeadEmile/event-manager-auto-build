package fr.iutdijon.eventmanager.clientapp.models.email;

import fr.iutdijon.eventmanager.services.email.models.business.EmailPlaceholderTypeBusinessModel;

public enum EmailPlaceholderTypeViewModel {
    LINK,
    TEXT;

    public static EmailPlaceholderTypeViewModel fromBusinessModel(EmailPlaceholderTypeBusinessModel businessModel) {
        return switch (businessModel) {
            case LINK -> LINK;
            case TEXT -> TEXT;
        };
    }
}
