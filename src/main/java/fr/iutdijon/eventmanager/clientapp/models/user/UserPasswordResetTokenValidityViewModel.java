package fr.iutdijon.eventmanager.clientapp.models.user;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPasswordResetTokenValidityViewModel {
    @NotNull
    private Boolean valid;
}
