package fr.iutdijon.eventmanager.clientapp.models.email;

import fr.iutdijon.eventmanager.utils.SimpleInputDataSource;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

import java.util.Base64;

@Getter
@Setter
public class EmailAttachmentViewModel {
    @NotBlank
    private String base64EncodedContent;
    @NotBlank
    private String contentType;
    @NotBlank
    private String name;

    public SimpleInputDataSource toDataSource() {
        return new SimpleInputDataSource(
                Base64.getDecoder().decode(base64EncodedContent),
                contentType,
                name
        );
    }
}
