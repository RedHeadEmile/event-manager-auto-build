package fr.iutdijon.eventmanager.clientapp.models.team;

import fr.iutdijon.eventmanager.clientapp.models.event.EventViewModel;
import fr.iutdijon.eventmanager.services.team.models.business.TeamBusinessModel;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamStoreRequestViewModel {
    @NotNull
    private EventViewModel event;
    private String name;

    public TeamBusinessModel toBusinessModel() {
        return new TeamBusinessModel() {{
            setEvent(TeamStoreRequestViewModel.this.getEvent().toBusinessModel());
            setName(TeamStoreRequestViewModel.this.getName());
        }};
    }
}
