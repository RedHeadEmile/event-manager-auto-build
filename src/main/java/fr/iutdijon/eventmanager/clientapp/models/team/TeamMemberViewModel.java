package fr.iutdijon.eventmanager.clientapp.models.team;

import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamMemberViewModel {
    @NotNull
    public Long userId;

    private Long profilePictureId;

    @NotNull
    private String email;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;

    public static TeamMemberViewModel fromBusinessModel(UserBusinessModel businessModel) {
        return new TeamMemberViewModel() {{
            setUserId(businessModel.getId());
            setProfilePictureId(businessModel.getProfilePictureId());
            setEmail(businessModel.getEmail());
            setFirstName(businessModel.getFirstName());
            setLastName(businessModel.getLastName());
        }};
    }
}
