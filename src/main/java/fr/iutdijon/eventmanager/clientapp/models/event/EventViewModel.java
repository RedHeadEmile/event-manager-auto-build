package fr.iutdijon.eventmanager.clientapp.models.event;

import fr.iutdijon.eventmanager.services.event.models.business.EventBusinessModel;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class EventViewModel {

    private Long id;
    private Long ownerId;
    private String code;
    private String name;
    private String description;
    private Date beginning;
    private Date registerBeginning;
    private Date registerEnding;
    private boolean allowTeamName;
    private Integer minimalTeamSize;
    private Integer maximalTeamSize;

    @NotNull
    private List<EventDocumentRequirementViewModel> documentRequirements = new ArrayList<>();

    public static EventViewModel fromBusinessModel(EventBusinessModel businessModel) {
        return new EventViewModel() {{
            setId(businessModel.getId());
            setOwnerId(businessModel.getOwnerId());
            setAllowTeamName(businessModel.isAllowTeamName());
            setBeginning(businessModel.getBeginning());
            setCode(businessModel.getCode());
            setDescription(businessModel.getDescription());
            setMaximalTeamSize(businessModel.getMaximalTeamSize());
            setMinimalTeamSize(businessModel.getMinimalTeamSize());
            setName(businessModel.getName());
            setRegisterBeginning(businessModel.getRegisterBeginning());
            setRegisterEnding(businessModel.getRegisterEnding());

            setDocumentRequirements(businessModel.getDocumentRequirements().map(EventDocumentRequirementViewModel::fromBusinessModel));
        }};
    }

    public EventBusinessModel toBusinessModel() {
        return new EventBusinessModel() {{
            setId(id);
            setAllowTeamName(allowTeamName);
            setOwnerId(ownerId);
            setBeginning(beginning);
            setCode(code);
            setDescription(description);
            setMaximalTeamSize(maximalTeamSize);
            setMinimalTeamSize(minimalTeamSize);
            setName(name);
            setRegisterBeginning(registerBeginning);
            setRegisterEnding(registerEnding);

            setDocumentRequirements(documentRequirements.map(EventDocumentRequirementViewModel::toBusinessModel));
        }};
    }

}
