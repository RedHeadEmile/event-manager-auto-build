package fr.iutdijon.eventmanager.clientapp.models.user;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPasswordResetRequestViewModel {
    @NotBlank
    private String passwordResetToken;

    @NotBlank
    private String newPassword;
}
