package fr.iutdijon.eventmanager.clientapp.models.user;

import fr.iutdijon.eventmanager.services.user.models.business.UserCreateRequestBusinessModel;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRegistrationRequestViewModel {
    @NotBlank
    @Email
    private String email;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @NotBlank
    private String password;

    public UserCreateRequestBusinessModel toBusinessModel() {
        return new UserCreateRequestBusinessModel() {{
            setEmail(email);
            setFirstName(firstName);
            setLastName(lastName);
            setPassword(password);
        }};
    }
}
