package fr.iutdijon.eventmanager.clientapp.models.team;

import fr.iutdijon.eventmanager.clientapp.models.event.EventViewModel;
import fr.iutdijon.eventmanager.clientapp.models.user.UserViewModel;
import fr.iutdijon.eventmanager.services.team.models.business.TeamBusinessModel;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class TeamViewModel extends TeamStoreRequestViewModel {
    @NotNull
    private Long id;
    @NotNull
    @NotEmpty
    private String code;
    @NotEmpty
    @NotNull
    private boolean validated;

    @NotNull
    private List<TeamDocumentViewModel> documents = new ArrayList<>();
    @NotNull
    private List<TeamInvitationViewModel> invitations = new ArrayList<>();
    @NotNull
    private List<TeamMemberViewModel> members = new ArrayList<>();

    @Override
    public TeamBusinessModel toBusinessModel() {
        TeamBusinessModel businessModel = super.toBusinessModel();
        businessModel.setId(id);
        businessModel.setCode(code);
        businessModel.setValidated(validated);

        return businessModel;
    }

    public static TeamViewModel fromBusinessModel(TeamBusinessModel team) {
        return new TeamViewModel() {{
            setId(team.getId());
            setEvent(EventViewModel.fromBusinessModel(team.getEvent()));
            setCode(team.getCode());
            setName(team.getName());
            setValidated(team.isValidated());

            setInvitations(team.getInvitations().map(TeamInvitationViewModel::fromBusinessModel));
            setMembers(team.getMembers().map(TeamMemberViewModel::fromBusinessModel));
        }};
    }
}
