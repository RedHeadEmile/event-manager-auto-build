package fr.iutdijon.eventmanager.clientapp.models.user;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserAuthenticationMagicLinkRequestViewModel extends UserAuthenticationRequestViewModel {
    @NotBlank
    private String magicLinkCode;
}
