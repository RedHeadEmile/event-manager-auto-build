package fr.iutdijon.eventmanager.clientapp.models.team;

import fr.iutdijon.eventmanager.services.team.models.business.TeamEmailBusinessModel;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class TeamEmailViewModel {
    @NotNull
    private Long id;
    @NotNull
    private int attachmentAmount;
    @NotBlank
    private String content;
    @NotNull
    private Date createdAt;
    @NotBlank
    private String subject;

    public static TeamEmailViewModel fromBusinessModel(TeamEmailBusinessModel businessModel) {
        return new TeamEmailViewModel() {{
            setId(businessModel.getId());
            setAttachmentAmount(businessModel.getAttachmentAmount());
            setContent(businessModel.getContent());
            setCreatedAt(businessModel.getCreatedAt());
            setSubject(businessModel.getSubject());
        }};
    }
}
