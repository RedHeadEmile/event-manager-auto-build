package fr.iutdijon.eventmanager.clientapp.models.team;

import fr.iutdijon.eventmanager.services.team.models.business.TeamDocumentBusinessModel;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamDocumentViewModel {
    @NotNull
    private Long eventDocumentRequirementId;
    private Long documentId;
    private String documentMimeType;
    private String newDocumentContent;

    public TeamDocumentBusinessModel toBusinessModel() {
        return new TeamDocumentBusinessModel() {{
            setEventDocumentRequirementId(TeamDocumentViewModel.this.getEventDocumentRequirementId());
            setDocumentId(TeamDocumentViewModel.this.getDocumentId());
            setDocumentMimeType(TeamDocumentViewModel.this.getDocumentMimeType());
            setNewDocumentContent(TeamDocumentViewModel.this.getNewDocumentContent());
        }};
    }

    public static TeamDocumentViewModel fromBusinessModel(TeamDocumentBusinessModel businessModel) {
        return new TeamDocumentViewModel() {{
            setEventDocumentRequirementId(businessModel.getEventDocumentRequirementId());
            setDocumentId(businessModel.getDocumentId());
            setDocumentMimeType(businessModel.getDocumentMimeType());
            setNewDocumentContent(businessModel.getNewDocumentContent());
        }};
    }
}
