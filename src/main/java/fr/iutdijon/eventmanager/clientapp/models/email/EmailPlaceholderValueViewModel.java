package fr.iutdijon.eventmanager.clientapp.models.email;

import fr.iutdijon.eventmanager.exceptions.BadRequestException;
import fr.iutdijon.eventmanager.services.email.models.business.EmailPlaceholderBusinessModel;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailPlaceholderValueViewModel {
    @NotBlank
    private String placeholder;
    @NotBlank
    private String value;

    public EmailPlaceholderBusinessModel toPlaceholder() {
        for (EmailPlaceholderBusinessModel emailPlaceholderBusinessModel : EmailPlaceholderBusinessModel.values())
            if (emailPlaceholderBusinessModel.getPlaceholder().equalsIgnoreCase(placeholder))
                return emailPlaceholderBusinessModel;
        throw new BadRequestException("Invalid email placeholder");
    }
}
