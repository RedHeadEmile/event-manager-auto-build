package fr.iutdijon.eventmanager.clientapp.controllers.event;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.event.EventCodeJoinedByUserIdRequestViewModel;
import fr.iutdijon.eventmanager.clientapp.models.event.EventViewModel;
import fr.iutdijon.eventmanager.services.event.IEventService;
import fr.iutdijon.eventmanager.services.event.models.business.EventBusinessModel;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


@RestController
@RequestMapping("/events")
@AuthorizeAnyPolicy(AuthorizationPolicy.USER)
public class EventController extends EventManagerController {

    private final IEventService eventService;

    @Autowired
    public EventController(IEventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping( "moderator")
    @AuthorizeAnyPolicy(AuthorizationPolicy.MODERATOR)
    public ResponseEntity<List<EventViewModel>> indexEventsToModerator() {
        List<EventViewModel> events = eventService.getEventsToModerator().map(EventViewModel::fromBusinessModel);
        return ok(events);
    }

    @GetMapping("joined")
    public ResponseEntity<List<EventViewModel>> indexEventsJoinedByUser() {
        List<EventViewModel> events = eventService.getEventsJoinedByCurrentUser().map(EventViewModel::fromBusinessModel);
        return ok(events);
    }

    @GetMapping("{id}")
    public ResponseEntity<EventViewModel> showEvent(@PathVariable Long id) {
        EventViewModel event = EventViewModel.fromBusinessModel(eventService.getEventById(id));
        return ok(event);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @AuthorizeAnyPolicy(AuthorizationPolicy.MODERATOR)
    public ResponseEntity<EventViewModel> createEvent(@RequestBody @Valid EventViewModel registrationRequest) throws URISyntaxException {
        EventBusinessModel createdEvent = this.eventService.addEvent(registrationRequest.toBusinessModel());
        String uri = createdEvent.getId().toString();
        return created(new URI(uri), EventViewModel.fromBusinessModel(createdEvent));
    }

    @DeleteMapping("{eventId}")
    @AuthorizeAnyPolicy(AuthorizationPolicy.MODERATOR)
    public void deleteEvent(@PathVariable Long eventId) {
        this.eventService.removeEvent(eventId);
    }

    @PutMapping()
    @AuthorizeAnyPolicy(AuthorizationPolicy.MODERATOR)
    public void updateEvent(@RequestBody @Valid EventViewModel event) {
        this.eventService.setEvent(event.toBusinessModel());
    }

    /*
    @PutMapping("{eventId}/{userId}")
    @RequestGuard(RequestGuardScope.LOGGED)
    public void addUserToEvent(@PathVariable Long eventId, @PathVariable Long userId) {
        this.eventService.addUserToEvent(eventId, userId);
    }
    */

    @PutMapping("events/join")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> addCurrentUserToEventByCode(@RequestBody @Valid EventCodeJoinedByUserIdRequestViewModel eventCodeJoinedByUserIdRequestViewModel) {

        EventBusinessModel eventBusinessModel = this.eventService.getEventByCode(eventCodeJoinedByUserIdRequestViewModel.getEventCode());
        if (eventBusinessModel == null)
            return notFound();

        EventViewModel eventToJoin = EventViewModel.fromBusinessModel(eventBusinessModel);

        this.eventService.addCurrentUserToEvent(eventToJoin.getId());

        return noContent();
    }

    @DeleteMapping()
    public void removeCurrentUserToEvent(@RequestBody @Valid Long eventId) {
        this.eventService.removeCurrentUserToEvent(eventId);
    }
}
