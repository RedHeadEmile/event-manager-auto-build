package fr.iutdijon.eventmanager.clientapp.controllers.team;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.email.EmailStoreRequestViewModel;
import fr.iutdijon.eventmanager.clientapp.models.team.TeamEmailViewModel;
import fr.iutdijon.eventmanager.services.team.ITeamService;
import fr.iutdijon.eventmanager.services.team.models.business.TeamEmailBusinessModel;
import fr.iutdijon.eventmanager.services.team.models.business.TeamEmailFiltersBusinessModel;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/teams/{teamId}/teams")
@AuthorizeAnyPolicy({AuthorizationPolicy.MODERATOR, AuthorizationPolicy.ADMINISTRATOR})
public class TeamEmailController extends EventManagerController {
    private final ITeamService teamService;

    @Autowired
    public TeamEmailController(ITeamService teamService) {
        this.teamService = teamService;
    }

    @PostMapping
    public ResponseEntity<TeamEmailViewModel> storeTeamEmail(@PathVariable Long teamId, @RequestBody @Valid EmailStoreRequestViewModel email) {
        TeamEmailBusinessModel businessModel = teamService.addTeamEmail(teamId, email.toTeamBusinessModel());
        return ok(TeamEmailViewModel.fromBusinessModel(businessModel));
    }

    @GetMapping
    public ResponseEntity<List<TeamEmailViewModel>> indexTeamEmails(@PathVariable Long teamId,
                                                                    @RequestParam(required = false) Date minDate,
                                                                    @RequestParam(required = false) Date maxDate
    ) {
        return ok(teamService.getTeamEmails(teamId, new TeamEmailFiltersBusinessModel() {{
            setMinDate(minDate);
            setMaxDate(maxDate);
        }}).map(TeamEmailViewModel::fromBusinessModel));
    }
}
