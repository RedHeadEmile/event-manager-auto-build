package fr.iutdijon.eventmanager.clientapp.controllers.statistics;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.services.statistics.IStatisticsService;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@RestController
@AuthorizeAnyPolicy(AuthorizationPolicy.MODERATOR)
@RequestMapping("/statistics")
public class StatisticsController extends EventManagerController {
    private final IStatisticsService statisticsService;

    @Autowired
    public StatisticsController(IStatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @GetMapping
    public ResponseEntity<byte[]> showStatistics() {
        byte[] statistics = statisticsService.getStatisticsExcelFile(Collections.emptyList());
        if (statistics == null)
            return notFound();

        return new ResponseEntity<>(
                statistics,
                new LinkedMultiValueMap<>() {{
                    add(HttpHeaders.CONTENT_TYPE, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=statistics.xlsx");
                }},
                HttpStatus.OK
        );
    }
}
