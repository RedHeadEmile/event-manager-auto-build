package fr.iutdijon.eventmanager.clientapp.controllers.team;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.team.*;
import fr.iutdijon.eventmanager.services.team.ITeamService;
import fr.iutdijon.eventmanager.services.team.models.business.TeamBusinessModel;
import fr.iutdijon.eventmanager.services.team.models.business.TeamInvitationBusinessModel;
import fr.iutdijon.eventmanager.services.team.models.business.TeamLiteBusinessModel;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

@RestController
@AuthorizeAnyPolicy(AuthorizationPolicy.USER)
@RequestMapping("teams")
public class TeamController extends EventManagerController {
    private final ITeamService teamService;

    @Autowired
    public TeamController(ITeamService teamService) {
        this.teamService = teamService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<TeamViewModel> storeTeam(@RequestBody @Valid TeamStoreRequestViewModel team) throws URISyntaxException {
        TeamBusinessModel businessModel = teamService.addTeam(team.toBusinessModel());
        return created(new URI("/teams/" + businessModel.getId()), TeamViewModel.fromBusinessModel(businessModel));
    }

    @GetMapping
    public ResponseEntity<List<TeamLiteViewModel>> indexCurrentUserTeams(@RequestParam(required = false) List<Long> eventIds) {
        if (eventIds == null)
            eventIds = Collections.emptyList();

        List<TeamLiteBusinessModel> teams = teamService.getCurrentUserTeams(eventIds);
        return ok(teams.map(TeamLiteViewModel::fromBusinessModel));
    }

    @PostMapping("/join-from-code/{teamCode}")
    public ResponseEntity<TeamViewModel> storeTeamJoinFromCode(@PathVariable String teamCode) {
        TeamBusinessModel team = teamService.addCurrentUserToTeam(teamCode);
        return ok(TeamViewModel.fromBusinessModel(team));
    }

    @GetMapping("{teamId}")
    public ResponseEntity<TeamViewModel> showTeam(@PathVariable Long teamId) {
        TeamBusinessModel businessModel = teamService.getTeam(teamId);
        if (businessModel == null)
            return notFound();

        return ok(TeamViewModel.fromBusinessModel(businessModel));
    }

    @PutMapping("{teamId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> updateTeamName(@PathVariable Long teamId, @RequestBody @Valid TeamNameUpdateRequestViewModel nameUpdateRequest) {
        teamService.setTeam(teamId, team ->
            team.setName(nameUpdateRequest.getNewTeamName())
        );
        return noContent();
    }

    @PostMapping("{teamId}/invitations")
    public ResponseEntity<TeamInvitationViewModel> storeTeamInvitation(@PathVariable Long teamId, @RequestBody @Valid TeamInvitationStoreRequestViewModel invitationRequest) {
        TeamInvitationBusinessModel invitation = teamService.addTeamInvitation(teamId, invitationRequest.getRecipient());
        return ok(TeamInvitationViewModel.fromBusinessModel(invitation));
    }

    @DeleteMapping("{teamId}/invitations/{teamInvitationId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> deleteTeamInvitation(@PathVariable Long teamId, @PathVariable Long teamInvitationId) {
        teamService.removeTeamInvitation(teamInvitationId);
        return noContent();
    }

    @PostMapping("{teamId}/leave")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> storeCurrentUserLeavingFromTeam(@PathVariable Long teamId) {
        teamService.removeCurrentUserFromTeam(teamId);
        return noContent();
    }

    @PostMapping("{teamId}/validate")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> storeTeamValidation(@PathVariable Long teamId) {
        teamService.setTeamValidated(teamId);
        return noContent();
    }
}
