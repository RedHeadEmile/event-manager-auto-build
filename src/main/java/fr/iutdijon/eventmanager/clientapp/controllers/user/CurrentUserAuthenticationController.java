package fr.iutdijon.eventmanager.clientapp.controllers.user;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.user.UserAuthenticationBasicRequestViewModel;
import fr.iutdijon.eventmanager.clientapp.models.user.UserAuthenticationMagicLinkRequestViewModel;
import fr.iutdijon.eventmanager.clientapp.models.user.UserAuthenticationRequestViewModel;
import fr.iutdijon.eventmanager.clientapp.models.user.UserViewModel;
import fr.iutdijon.eventmanager.exceptions.BadRequestDetail;
import fr.iutdijon.eventmanager.services.user.IUserService;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import fr.iutdijon.eventmanager.utils.AuthorizeAnonymousPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/current-user/authentication")
public class CurrentUserAuthenticationController extends EventManagerController {
    private final IUserService userService;

    @Autowired
    public CurrentUserAuthenticationController(IUserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @AuthorizeAnonymousPolicy(rejectLoggedUsers = true)
    public ResponseEntity<UserViewModel> storeCurrentAuthenticationRequest(@RequestBody @Valid UserAuthenticationRequestViewModel authenticationRequest) {
        if (authenticationRequest instanceof UserAuthenticationBasicRequestViewModel basicRequest) {
            UserBusinessModel businessModel = userService.setCurrentUser(basicRequest.getEmail(), basicRequest.getPassword());
            return ok(UserViewModel.fromBusinessModel(businessModel));
        }

        if (authenticationRequest instanceof UserAuthenticationMagicLinkRequestViewModel magicLinkRequest) {
            UserBusinessModel businessModel = userService.setCurrentUser(magicLinkRequest.getMagicLinkCode());
            return ok(UserViewModel.fromBusinessModel(businessModel));
        }

        throw BadRequestDetail.UNKNOWN_AUTHENTICATION_TYPE.toException();
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AuthorizeAnyPolicy(AuthorizationPolicy.USER)
    public ResponseEntity<Void> deleteCurrentUserAuthentication() {
        userService.removeCurrentUserCookie();
        return noContent();
    }
}
