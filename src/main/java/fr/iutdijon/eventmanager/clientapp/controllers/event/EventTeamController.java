package fr.iutdijon.eventmanager.clientapp.controllers.event;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.team.TeamViewModel;
import fr.iutdijon.eventmanager.services.team.ITeamService;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/events/{id}/teams")
@AuthorizeAnyPolicy({AuthorizationPolicy.MODERATOR, AuthorizationPolicy.ADMINISTRATOR})
public class EventTeamController extends EventManagerController {
    private final ITeamService teamService;

    @Autowired
    public EventTeamController(ITeamService teamService) {
        this.teamService = teamService;
    }

    @GetMapping
    public ResponseEntity<List<TeamViewModel>> indexEventTeams(@PathVariable Long id) {
        return ok(teamService.getTeams(id).map(TeamViewModel::fromBusinessModel));
    }
}
