package fr.iutdijon.eventmanager.clientapp.controllers.user;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.user.UserRegistrationRequestViewModel;
import fr.iutdijon.eventmanager.clientapp.models.user.UserViewModel;
import fr.iutdijon.eventmanager.services.user.IUserService;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import fr.iutdijon.eventmanager.utils.AuthorizeAnonymousPolicy;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@AuthorizeAnonymousPolicy(rejectLoggedUsers = true)
@RequestMapping("/user-registration")
public class UserRegistrationController extends EventManagerController {
    private final IUserService userService;

    @Autowired
    public UserRegistrationController(IUserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<UserViewModel> storeUserRegistration(@RequestBody @Valid UserRegistrationRequestViewModel registrationRequest) throws URISyntaxException {
        UserBusinessModel createdUser = this.userService.addCurrentUser(registrationRequest.toBusinessModel());
        return created(new URI("/current-user"), UserViewModel.fromBusinessModel(createdUser));
    }
}
