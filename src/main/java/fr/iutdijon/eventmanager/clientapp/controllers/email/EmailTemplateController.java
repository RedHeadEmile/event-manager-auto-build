package fr.iutdijon.eventmanager.clientapp.controllers.email;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.email.EmailPlaceholderContextViewModel;
import fr.iutdijon.eventmanager.clientapp.models.email.EmailTemplateViewModel;
import fr.iutdijon.eventmanager.clientapp.models.email.EmailTemplatesViewModel;
import fr.iutdijon.eventmanager.services.email.IEmailService;
import fr.iutdijon.eventmanager.services.email.models.business.EmailPlaceholderContextBusinessModel;
import fr.iutdijon.eventmanager.services.email.models.business.EmailTemplateBusinessModel;
import fr.iutdijon.eventmanager.services.user.IUserService;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AuthorizeAnyPolicy({AuthorizationPolicy.ADMINISTRATOR, AuthorizationPolicy.MODERATOR})
@RequestMapping("/email-templates")
public class EmailTemplateController extends EventManagerController {
    private final IUserService userService;
    private final IEmailService emailService;

    @Autowired
    public EmailTemplateController(IUserService userService, IEmailService emailService) {
        this.userService = userService;
        this.emailService = emailService;
    }

    @GetMapping
    public ResponseEntity<EmailTemplatesViewModel> indexEmailTemplate(@RequestParam(required = false, defaultValue = "ANY") EmailPlaceholderContextViewModel placeholderContextViewModel) {
        List<EmailTemplateBusinessModel> businessModels = emailService.getTemplates(userService.getCurrentUser(), placeholderContextViewModel.toBusinessModel());
        return ok(EmailTemplatesViewModel.fromBusinessModel(businessModels, placeholderContextViewModel));
    }

    @GetMapping("{templateIdentifier}")
    public ResponseEntity<EmailTemplateViewModel> showEmailTemplate(@PathVariable @Valid @NotBlank String templateIdentifier) {
        try {
            Long templateId = Long.parseLong(templateIdentifier);
            EmailTemplateBusinessModel businessModel = emailService.getTemplate(userService.getCurrentUser(), templateId);
            if (businessModel == null)
                return notFound();

            return ok(EmailTemplateViewModel.fromBusinessModel(businessModel));
        }
        catch (NumberFormatException e) {
            EmailTemplateBusinessModel businessModel = emailService.getTemplate(userService.getCurrentUser(), templateIdentifier);
            if (businessModel == null)
                return notFound();

            return ok(EmailTemplateViewModel.fromBusinessModel(businessModel));
        }
    }

    @PutMapping
    public ResponseEntity<EmailTemplateViewModel> setEmailTemplate(@RequestBody @Valid EmailTemplateViewModel emailTemplate) {
        EmailTemplateBusinessModel businessModel = emailService.setTemplate(userService.getCurrentUser(), emailTemplate.toBusinessModelModel());
        return ok(EmailTemplateViewModel.fromBusinessModel(businessModel));
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> deleteEmailTemplate(@PathVariable Long id) {
        emailService.removeTemplate(userService.getCurrentUser(), id);
        return noContent();
    }

    @PostMapping("send")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> storeSendEmailTemplateRequest(@RequestBody @Valid EmailTemplateViewModel emailTemplate) {
        emailService.sendEmailTemplateToUserAsync(userService.getCurrentUser(), emailTemplate.toBusinessModelModel());
        return noContent();
    }
}
