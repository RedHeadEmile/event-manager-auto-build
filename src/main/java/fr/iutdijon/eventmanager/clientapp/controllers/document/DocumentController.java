package fr.iutdijon.eventmanager.clientapp.controllers.document;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.services.document.IDocumentService;
import fr.iutdijon.eventmanager.services.document.models.business.DocumentBusinessModel;
import fr.iutdijon.eventmanager.utils.AuthorizeAnonymousPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/documents")
public class DocumentController extends EventManagerController {
    private final IDocumentService documentService;

    @Autowired
    public DocumentController(IDocumentService documentService) {
        this.documentService = documentService;
    }

    @GetMapping("{id}")
    @AuthorizeAnonymousPolicy(rejectLoggedUsers = false)
    public ResponseEntity<byte[]> showDocument(@PathVariable Long id, @RequestParam(required = false, defaultValue = "false") boolean download) {
        DocumentBusinessModel businessModel = documentService.getDocument(id, true);
        if (businessModel == null)
            return notFound();

        return new ResponseEntity<>(
                businessModel.getContent(),
                new LinkedMultiValueMap<>() {{
                    add(HttpHeaders.CONTENT_TYPE, businessModel.getType());
                    add(HttpHeaders.CONTENT_DISPOSITION, download ? "attachment; filename=" + businessModel.getFullName() : "inline");
                }},
                HttpStatus.OK
        );
    }
}
