package fr.iutdijon.eventmanager.clientapp.controllers.team;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.team.TeamDocumentViewModel;
import fr.iutdijon.eventmanager.services.team.ITeamService;
import fr.iutdijon.eventmanager.services.team.models.business.TeamDocumentBusinessModel;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AuthorizeAnyPolicy(AuthorizationPolicy.USER)
@RequestMapping("teams")
public class TeamDocumentController extends EventManagerController {
    private final ITeamService teamService;

    @Autowired
    public TeamDocumentController(ITeamService teamService) {
        this.teamService = teamService;
    }

    @PutMapping("{teamId}/documents")
    public ResponseEntity<TeamDocumentViewModel> updateTeamDocument(@PathVariable Long teamId, @RequestBody @Valid TeamDocumentViewModel teamDocument) {
        TeamDocumentBusinessModel businessModel = teamDocument.toBusinessModel();
        businessModel.setTeamId(teamId);

        TeamDocumentBusinessModel result = teamService.setTeamDocument(businessModel);
        if (result == null)
            return noContent();
        return ok(TeamDocumentViewModel.fromBusinessModel(result));
    }
}
