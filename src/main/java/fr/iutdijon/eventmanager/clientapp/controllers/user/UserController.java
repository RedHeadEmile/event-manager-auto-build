package fr.iutdijon.eventmanager.clientapp.controllers.user;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.user.UserViewModel;
import fr.iutdijon.eventmanager.services.user.IUserService;
import fr.iutdijon.eventmanager.services.user.models.business.UserPermissionBusinessModel;
import fr.iutdijon.eventmanager.services.user.models.business.UsersFiltersBusinessModel;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AuthorizeAnyPolicy(AuthorizationPolicy.ADMINISTRATOR)
@RequestMapping("/users")
public class UserController extends EventManagerController {
    private final IUserService userService;

    @Autowired
    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<List<UserViewModel>> indexUsers(
            @RequestParam(required = false) String query,
            @RequestParam(required = false) Long requiredPermission,
            @RequestParam(required = false) Boolean emailConfirmed,
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) Integer offset) {

        UsersFiltersBusinessModel filtersBusinessModel = new UsersFiltersBusinessModel();
        filtersBusinessModel.setQuery(query);
        filtersBusinessModel.setRequiredPermission(requiredPermission);
        filtersBusinessModel.setEmailConfirmed(emailConfirmed);
        filtersBusinessModel.setLimit(limit);
        filtersBusinessModel.setOffset(offset);

        List<UserViewModel> users = userService.getUsers(filtersBusinessModel).map(UserViewModel::fromBusinessModel);
        return ok(users);
    }

    @PostMapping("{id}/grant-administrator")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> storeUserGrantAdministrator(@PathVariable Long id) {
        userService.setUserPermission(id, UserPermissionBusinessModel.ADMINISTRATOR, true);
        return noContent();
    }

    @PostMapping("{id}/grant-moderator")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> storeUserGrantModerator(@PathVariable Long id) {
        userService.setUserPermission(id, UserPermissionBusinessModel.MODERATOR, true);
        return noContent();
    }

    @PostMapping("{id}/revoke-moderator")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> storeUserRevokeModerator(@PathVariable Long id) {
        userService.setUserPermission(id, UserPermissionBusinessModel.MODERATOR, false);
        return noContent();
    }

    @PostMapping("{id}/confirmation-email")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> storeUserConfirmationEmail(@PathVariable Long id) {
        userService.sendUserConfirmationEmail(id);
        return noContent();
    }
}
