package fr.iutdijon.eventmanager.clientapp.controllers.event;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.email.EmailStoreRequestViewModel;
import fr.iutdijon.eventmanager.clientapp.models.event.EventEmailViewModel;
import fr.iutdijon.eventmanager.services.event.IEventService;
import fr.iutdijon.eventmanager.services.event.models.business.EventEmailBusinessModel;
import fr.iutdijon.eventmanager.services.event.models.business.EventEmailFiltersBusinessModel;
import fr.iutdijon.eventmanager.utils.AuthorizationPolicy;
import fr.iutdijon.eventmanager.utils.AuthorizeAnyPolicy;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/events/{eventId}/emails")
@AuthorizeAnyPolicy({AuthorizationPolicy.MODERATOR, AuthorizationPolicy.ADMINISTRATOR})
public class EventEmailController extends EventManagerController {
    private final IEventService eventService;

    @Autowired
    public EventEmailController(IEventService eventService) {
        this.eventService = eventService;
    }

    @PostMapping
    public ResponseEntity<EventEmailViewModel> storeEventEmail(@PathVariable Long eventId, @RequestBody @Valid EmailStoreRequestViewModel email) {
        EventEmailBusinessModel businessModel = eventService.addEventEmail(eventId, email.toEventBusinessModel());
        return ok(EventEmailViewModel.fromBusinessModel(businessModel));
    }

    @GetMapping
    public ResponseEntity<List<EventEmailViewModel>> indexEventEmails(@PathVariable Long eventId,
                                                                      @RequestParam(required = false) Date minDate,
                                                                      @RequestParam(required = false) Date maxDate
    ) {
        return ok(eventService.getEventEmails(eventId, new EventEmailFiltersBusinessModel() {{
            setMinDate(minDate);
            setMaxDate(maxDate);
        }}).map(EventEmailViewModel::fromBusinessModel));
    }
}
