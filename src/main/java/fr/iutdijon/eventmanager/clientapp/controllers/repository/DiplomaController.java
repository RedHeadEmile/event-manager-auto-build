package fr.iutdijon.eventmanager.clientapp.controllers.repository;

import fr.iutdijon.eventmanager.clientapp.controllers.EventManagerController;
import fr.iutdijon.eventmanager.clientapp.models.repository.DiplomaViewModel;
import fr.iutdijon.eventmanager.services.repository.IRepositoryService;
import fr.iutdijon.eventmanager.utils.AuthorizeAnonymousPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/diploma")
@AuthorizeAnonymousPolicy(rejectLoggedUsers = false)
public class DiplomaController extends EventManagerController {
    private final IRepositoryService repositoryService;

    @Autowired
    public DiplomaController(IRepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    @GetMapping
    public ResponseEntity<List<DiplomaViewModel>> indexDiplomas(@RequestParam String query) {
        return ok(this.repositoryService.getDiplomas(query).map(DiplomaViewModel::fromBusinessModel));
    }
}
