package fr.iutdijon.eventmanager.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface AuthorizeAnonymousPolicy {
    /**
     * Define if logged user should be rejected or if both logged and anonymous users can access this resource.
     * @return True to reject logged users, False to allow both logged and anonymous logged.
     */
    boolean rejectLoggedUsers();
}
