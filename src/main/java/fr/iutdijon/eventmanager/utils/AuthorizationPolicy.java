package fr.iutdijon.eventmanager.utils;

public enum AuthorizationPolicy {
    ADMINISTRATOR,
    DEVELOPER,
    MODERATOR,
    USER,
}
