package fr.iutdijon.eventmanager.services.document;

import fr.iutdijon.eventmanager.services.document.models.business.DocumentBusinessModel;

import java.util.List;

public interface IDocumentService {
    DocumentBusinessModel addDocument(DocumentBusinessModel document);

    DocumentBusinessModel getDocument(Long documentId, boolean fetchContent);
    List<DocumentBusinessModel> getDocuments(List<Long> documentIds, boolean fetchContent);

    void removeDocument(Long documentId);
    void removeDocuments(List<Long> documentIds);
}
