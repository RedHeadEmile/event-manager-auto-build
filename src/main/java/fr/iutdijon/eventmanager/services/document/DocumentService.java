package fr.iutdijon.eventmanager.services.document;

import fr.iutdijon.eventmanager.exceptions.BadRequestDetail;
import fr.iutdijon.eventmanager.services.document.models.business.DocumentBusinessModel;
import fr.iutdijon.eventmanager.services.document.models.data.DocumentDataModel;
import fr.iutdijon.eventmanager.services.document.repository.IDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import java.util.Arrays;
import java.util.List;

@Service
@RequestScope
public class DocumentService implements IDocumentService {

    private final IDocumentRepository repository;

    private final List<String> allowedContentType = Arrays.asList(
            MediaType.IMAGE_GIF_VALUE,
            MediaType.IMAGE_JPEG_VALUE,
            MediaType.IMAGE_PNG_VALUE,
            MediaType.APPLICATION_PDF_VALUE
    );
    private static final int MAX_DOCUMENT_SIZE = 1000 * 1000 * 3; // 3 Mo

    @Autowired
    public DocumentService(IDocumentRepository repository) {
        this.repository = repository;
    }

    @Override
    public DocumentBusinessModel addDocument(DocumentBusinessModel document) {
        if (document.getContent() == null || document.getContent().length == 0 || !allowedContentType.contains(document.getType()))
            throw BadRequestDetail.INVALID_DOCUMENT_CONTENT.toException();

        if (document.getContent().length > MAX_DOCUMENT_SIZE)
            throw BadRequestDetail.DOCUMENT_TOO_BIG.toException();

        document.setCreatedAt(System.currentTimeMillis());

        DocumentDataModel createdDocument = this.repository.createDocument(document.toDataModel());
        return DocumentBusinessModel.fromDataModel(createdDocument);
    }

    private String contentTypeToExtension(String contentType) {
        return switch (contentType) {
            case MediaType.IMAGE_GIF_VALUE -> "gif";
            case MediaType.IMAGE_JPEG_VALUE -> "jpeg";
            case MediaType.IMAGE_PNG_VALUE -> "png";
            case MediaType.APPLICATION_PDF_VALUE -> "pdf";
            default -> throw new IllegalStateException("No extension known for content type " + contentType);
        };
    }

    @Override
    public DocumentBusinessModel getDocument(Long documentId, boolean fetchContent) {
        DocumentDataModel dataModel = this.repository.readDocument(documentId, fetchContent);
        if (dataModel == null)
            return null;

        DocumentBusinessModel businessModel = DocumentBusinessModel.fromDataModel(dataModel);

        String fileExtension = contentTypeToExtension(businessModel.getType());
        businessModel.setFullName(businessModel.getName() + '.' + fileExtension);

        return businessModel;
    }

    @Override
    public List<DocumentBusinessModel> getDocuments(List<Long> documentIds, boolean fetchContent) {
        List<DocumentDataModel> dataModels = this.repository.readDocuments(documentIds, fetchContent);

        return dataModels.map(dataModel -> {
            DocumentBusinessModel businessModel = DocumentBusinessModel.fromDataModel(dataModel);

            String fileExtension = contentTypeToExtension(businessModel.getType());
            businessModel.setFullName(businessModel.getName() + '.' + fileExtension);

            return businessModel;
        });
    }

    @Override
    public void removeDocument(Long documentId) {
        this.repository.deleteDocument(documentId);
    }

    @Override
    public void removeDocuments(List<Long> documentIds) {
        this.repository.deleteDocuments(documentIds);
    }
}
