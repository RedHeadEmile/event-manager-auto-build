package fr.iutdijon.eventmanager.services.email.repositories;

import fr.iutdijon.eventmanager.services.email.models.data.EmailTemplateDataModel;

import java.util.List;

public interface IEmailRepository {
    EmailTemplateDataModel createTemplate(EmailTemplateDataModel dataModel);
    EmailTemplateDataModel readTemplate(String templateCode);
    EmailTemplateDataModel readTemplate(Long templateId);
    List<EmailTemplateDataModel> readTemplates(Long ownerId, boolean includeAdminTemplates);
    void updateTemplate(EmailTemplateDataModel dataModel);
    void deleteTemplate(Long templateId);
}
