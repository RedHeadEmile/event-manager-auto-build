package fr.iutdijon.eventmanager.services.email.repositories;

import fr.iutdijon.eventmanager.infrastructure.SingletonDatabaseConnection;
import fr.iutdijon.eventmanager.services.email.models.data.EmailTemplateDataModel;
import net.redheademile.jdapper.JDapper;
import org.intellij.lang.annotations.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.RequestScope;

import java.util.ArrayList;
import java.util.List;

@Repository
@RequestScope
public class EmailRepository implements IEmailRepository {
    private final SingletonDatabaseConnection databaseConnection;

    @Autowired
    public EmailRepository(SingletonDatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    @Override
    public EmailTemplateDataModel createTemplate(EmailTemplateDataModel dataModel) {
        Long newId = databaseConnection.insertAndGetGeneratedLongKey(
                "INSERT INTO emailtemplate (user_userid, code, content, label, shared, subject) VALUES (?, ?, ?, ?, ?, ?)",
                dataModel.getOwnerId(), dataModel.getCode(), dataModel.getContent(), dataModel.getLabel(), dataModel.getShared(), dataModel.getSubject()
        );

        dataModel.setId(newId);
        return dataModel;
    }

    @Override
    public EmailTemplateDataModel readTemplate(String templateCode) {
        return databaseConnection.query(
                "SELECT * from emailtemplate WHERE code = ?",
                JDapper.getMapper(EmailTemplateDataModel.class),
                templateCode).singleOrNull();
    }

    @Override
    public EmailTemplateDataModel readTemplate(Long templateId) {
        return databaseConnection.query(
                "SELECT * from emailtemplate WHERE emailtemplateid = ?",
                JDapper.getMapper(EmailTemplateDataModel.class),
                templateId).singleOrNull();
    }

    @Override
    public List<EmailTemplateDataModel> readTemplates(Long ownerId, boolean includeAdminTemplates) {
        @Language("SQL") String query = "SELECT * FROM emailtemplate";

        List<String> whereClauses = new ArrayList<>();
        List<Object> params = new ArrayList<>();

        if (ownerId != null) {
            whereClauses.add("(user_userid = ? OR shared = 1)");
            params.add(ownerId);
        }

        if (!includeAdminTemplates)
            whereClauses.add("code IS NULL");

        if (!whereClauses.isEmpty())
            query += " WHERE " + String.join(" AND ", whereClauses);

        return databaseConnection.query(query, JDapper.getMapper(EmailTemplateDataModel.class), params.toArray());
    }

    @Override
    public void updateTemplate(EmailTemplateDataModel dataModel) {
        databaseConnection.update(
                "UPDATE emailtemplate SET code = ?, content = ?, label = ?, shared = ?, subject = ? WHERE emailtemplateid = ?",
                dataModel.getCode(), dataModel.getContent(), dataModel.getLabel(), dataModel.getShared(), dataModel.getSubject(), dataModel.getId()
        );
    }

    @Override
    public void deleteTemplate(Long templateId) {
        databaseConnection.update(
                "DELETE FROM emailtemplate WHERE emailtemplateid = ?",
                templateId
        );
    }
}
