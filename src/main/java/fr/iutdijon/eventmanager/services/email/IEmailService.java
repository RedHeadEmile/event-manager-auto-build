package fr.iutdijon.eventmanager.services.email;

import fr.iutdijon.eventmanager.services.email.models.business.EmailBusinessModel;
import fr.iutdijon.eventmanager.services.email.models.business.EmailPlaceholderBusinessModel;
import fr.iutdijon.eventmanager.services.email.models.business.EmailPlaceholderContextBusinessModel;
import fr.iutdijon.eventmanager.services.email.models.business.EmailTemplateBusinessModel;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public interface IEmailService {
    CompletableFuture<Void> sendEmailAsync(EmailBusinessModel email, Map<EmailPlaceholderBusinessModel, String> placeholdersValue);
    CompletableFuture<Void> sendEmailTemplateToUserAsync(UserBusinessModel user, EmailTemplateBusinessModel emailTemplate);

    EmailTemplateBusinessModel getTemplate(UserBusinessModel currentUser, String templateCode);
    EmailTemplateBusinessModel getTemplate(UserBusinessModel currentUser, Long templateId);
    List<EmailTemplateBusinessModel> getTemplates(UserBusinessModel currentUser, EmailPlaceholderContextBusinessModel context);
    EmailTemplateBusinessModel setTemplate(UserBusinessModel currentUser, EmailTemplateBusinessModel template);
    void removeTemplate(UserBusinessModel currentUser, Long templateId);
}
