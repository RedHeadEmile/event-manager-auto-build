package fr.iutdijon.eventmanager.services.email.models.business;

public enum EmailPlaceholderContextBusinessModel {
    ANY,
    EVENT,
    TEAM
}
