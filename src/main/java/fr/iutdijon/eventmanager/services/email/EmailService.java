package fr.iutdijon.eventmanager.services.email;

import fr.iutdijon.eventmanager.exceptions.BadRequestDetail;
import fr.iutdijon.eventmanager.exceptions.ForbiddenException;
import fr.iutdijon.eventmanager.services.email.models.business.*;
import fr.iutdijon.eventmanager.services.email.models.data.EmailTemplateDataModel;
import fr.iutdijon.eventmanager.services.email.repositories.IEmailRepository;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import fr.iutdijon.eventmanager.services.user.models.business.UserPermissionBusinessModel;
import fr.iutdijon.eventmanager.utils.SimpleInputDataSource;
import jakarta.activation.DataHandler;
import jakarta.activation.DataSource;
import jakarta.mail.*;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@RequestScope
public class EmailService implements IEmailService {

    private final JavaMailSender mailSender;
    private final IEmailRepository repository;
    private final EmailServiceProperties properties;

    private final int MAXIMUM_TEMPLATE_LENGTH = 16777216;

    @Autowired
    public EmailService(JavaMailSender mailSender, IEmailRepository repository, EmailServiceProperties properties) {
        this.mailSender = mailSender;
        this.repository = repository;
        this.properties = properties;
    }

    private final Pattern dataUriPattern = Pattern.compile("<img src=\"data:(?<mimeType>[\\w/\\-.]+);(?<encoding>\\w+),(?<data>[^\"]*)\"");
    private String replaceDataUri(String content, Multipart multipart) throws MessagingException {
        StringBuilder newContent = new StringBuilder();

        int nextImageNumber = 0;
        int lastContentIndex = 0;
        Matcher matcher = dataUriPattern.matcher(content);
        while (matcher.find(lastContentIndex)) {
            newContent.append(content, lastContentIndex, matcher.start());
            lastContentIndex = matcher.end();

            String encoding = matcher.group("encoding");
            if (!encoding.equalsIgnoreCase("base64"))
                throw new IllegalArgumentException("Unknown format");

            String mimeType = matcher.group("mimeType");
            String data = matcher.group("data");

            String cid = "part" + (++nextImageNumber);

            MimeBodyPart part = new MimeBodyPart();
            part.setHeader("Content-Type", mimeType);
            part.setHeader("Content-Transfer-Encoding", encoding);
            part.setDataHandler(new DataHandler(new SimpleInputDataSource(Base64.getDecoder().decode(data), mimeType, cid)));
            part.setContentID("<" + cid + ">");
            part.setDisposition(MimeBodyPart.INLINE);

            newContent.append("<img src=\"cid:").append(cid).append("\"");

            multipart.addBodyPart(part);
        }

        newContent.append(content.substring(lastContentIndex));
        return newContent.toString();
    }

    @Override
    @Async
    public CompletableFuture<Void> sendEmailAsync(EmailBusinessModel email, Map<EmailPlaceholderBusinessModel, String> placeholdersValue) {
        if (email.getTo().isEmpty() || email.getSubject().isNullOrEmpty() || email.getContent().isNullOrEmpty() || email.getContentType().isNullOrEmpty())
            throw BadRequestDetail.INVALID_EMAIL.toException();

        try {
            MimeMessage message = mailSender.createMimeMessage();

            message.setRecipients(Message.RecipientType.TO, email.getTo().map(EmailAddressBusinessModel::toInternetAddress).toArray(new InternetAddress[0]));
            if (!email.getCc().isEmpty()) message.setRecipients(Message.RecipientType.CC, email.getCc().map(EmailAddressBusinessModel::toInternetAddress).toArray(new InternetAddress[0]));
            if (!email.getBcc().isEmpty()) message.setRecipients(Message.RecipientType.BCC, email.getBcc().map(EmailAddressBusinessModel::toInternetAddress).toArray(new InternetAddress[0]));

            message.setFrom(email.getFrom() != null ? email.getFrom().toInternetAddress() : new InternetAddress(this.properties.getEmailFrom(), this.properties.getEmailDisplayName()));
            if (email.getSender() != null) message.setSender(email.getSender().toInternetAddress());
            if (!email.getReplyTo().isEmpty()) message.setReplyTo(email.getReplyTo().map(EmailAddressBusinessModel::toInternetAddress).toArray(new InternetAddress[0]));

            message.setSubject(email.getSubject());

            Multipart multipart = new MimeMultipart("related");

            String newContent = replaceDataUri(email.getContent(), multipart);
            for (Map.Entry<EmailPlaceholderBusinessModel, String> placeholderWithValue : placeholdersValue.entrySet())
                newContent = newContent.replace("{" + placeholderWithValue.getKey().getPlaceholder() + "}", placeholderWithValue.getValue());

            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(newContent, email.getContentType());
            messageBodyPart.setDisposition(MimeBodyPart.INLINE);
            multipart.addBodyPart(messageBodyPart, 0);

            for (DataSource attachment : email.getAttachments()) {
                MimeBodyPart attachmentPart = new MimeBodyPart();
                attachmentPart.setDataHandler(new DataHandler(attachment));
                attachmentPart.setFileName(attachment.getName());
                attachmentPart.setDisposition(MimeBodyPart.ATTACHMENT);
                multipart.addBodyPart(attachmentPart);
            }

            message.setContent(multipart);
            mailSender.send(message);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }

        return CompletableFuture.completedFuture(null);
    }

    @Override
    @Async
    public CompletableFuture<Void> sendEmailTemplateToUserAsync(UserBusinessModel user, EmailTemplateBusinessModel emailTemplate) {
        return sendEmailAsync(new EmailBusinessModel() {{
            setTo(Collections.singletonList(user.toEmailAddress()));

            setSubject(emailTemplate.getSubject());
            setContent(emailTemplate.getContent());
        }}, Collections.emptyMap());
    }

    /**
     * Should fill the {@link EmailTemplateBusinessModel#getPlaceholders()} with the supported email placeholders like firstname and lastname of the user.
     * @param template The {@link EmailTemplateBusinessModel} to fill
     * @return The same instance of {@link EmailTemplateBusinessModel} after its placeholders has been filled.
     */
    private EmailTemplateBusinessModel concatPlaceholders(EmailTemplateBusinessModel template) {
        template.getPlaceholders().add(EmailPlaceholderBusinessModel.FIRSTNAME);
        template.getPlaceholders().add(EmailPlaceholderBusinessModel.LASTNAME);

        if (template.getCode() != null)
            switch (template.getCode()) {
                case EmailServiceConstants.EMAIL_TEMPLATE_CODE_EMAIL_CONFIRMATION:
                    template.getPlaceholders().add(EmailPlaceholderBusinessModel.EMAIL_CONFIRMATION_LINK);
                    break;

                case EmailServiceConstants.EMAIL_TEMPLATE_CODE_MAGIC_LINK:
                    template.getPlaceholders().add(EmailPlaceholderBusinessModel.MAGIC_LINK);
                    break;

                case EmailServiceConstants.EMAIL_TEMPLATE_CODE_PASSWORD_RESET:
                    template.getPlaceholders().add(EmailPlaceholderBusinessModel.PASSWORD_RESET_LINK);
                    break;

                case EmailServiceConstants.EMAIL_TEMPLATE_CODE_TEAM_INVITATION:
                    template.getPlaceholders().add(EmailPlaceholderBusinessModel.TEAM_INVITATION_LINK);
                    template.getPlaceholders().add(EmailPlaceholderBusinessModel.TEAM_NAME);
                    template.getPlaceholders().add(EmailPlaceholderBusinessModel.EVENT_NAME);
                    break;
            }
        else {
            template.getPlaceholders().add(EmailPlaceholderBusinessModel.TEAM_NAME);
            template.getPlaceholders().add(EmailPlaceholderBusinessModel.EVENT_NAME);
        }

        return template;
    }

    @Override
    public EmailTemplateBusinessModel getTemplate(UserBusinessModel currentUser, String templateCode) {
        if (currentUser != null && !currentUser.havePermission(UserPermissionBusinessModel.ADMINISTRATOR))
            throw new ForbiddenException();

        EmailTemplateDataModel dataModel = this.repository.readTemplate(templateCode);
        if (dataModel == null)
            return null;

        return concatPlaceholders(EmailTemplateBusinessModel.fromDataModel(dataModel));
    }

    @Override
    public EmailTemplateBusinessModel getTemplate(UserBusinessModel currentUser, Long templateId) {
        if (currentUser != null && !currentUser.havePermission(UserPermissionBusinessModel.MODERATOR) && !currentUser.havePermission(UserPermissionBusinessModel.ADMINISTRATOR))
            throw new ForbiddenException();

        EmailTemplateDataModel dataModel = this.repository.readTemplate(templateId);
        if (dataModel == null)
            return null;

        if (currentUser != null && !currentUser.havePermission(UserPermissionBusinessModel.ADMINISTRATOR) && !Objects.equals(dataModel.getOwnerId(), currentUser.getId()) && !dataModel.getShared())
            throw new ForbiddenException();

        return concatPlaceholders(EmailTemplateBusinessModel.fromDataModel(dataModel));
    }

    @Override
    public List<EmailTemplateBusinessModel> getTemplates(UserBusinessModel currentUser, EmailPlaceholderContextBusinessModel context) {
        boolean isAdmin = currentUser == null || currentUser.havePermission(UserPermissionBusinessModel.ADMINISTRATOR);
        List<EmailTemplateDataModel> dataModels = this.repository.readTemplates(isAdmin ? null : currentUser.getId(), isAdmin && context == EmailPlaceholderContextBusinessModel.ANY);
        return dataModels.map(dataModel -> concatPlaceholders(EmailTemplateBusinessModel.fromDataModel(dataModel)));
    }

    @Override
    public EmailTemplateBusinessModel setTemplate(UserBusinessModel currentUser, EmailTemplateBusinessModel template) {
        if (template.getContent() != null && template.getContent().length() > MAXIMUM_TEMPLATE_LENGTH)
            throw BadRequestDetail.DOCUMENT_TOO_BIG.toException();

        EmailTemplateDataModel existingTemplate = null;

        if (template.getId() != null && template.getId() > 0)
            existingTemplate = this.repository.readTemplate(template.getId());

        if (existingTemplate == null && template.getCode() != null)
            existingTemplate = this.repository.readTemplate(template.getCode());

        if (existingTemplate == null) {
            template.setCode(null);
            template.setOwnerId(currentUser.getId());

            EmailTemplateDataModel createdTemplate = this.repository.createTemplate(template.toDataModel());
            return concatPlaceholders(EmailTemplateBusinessModel.fromDataModel(createdTemplate));
        }
        else {
            boolean isUserAdmin = currentUser == null || currentUser.havePermission(UserPermissionBusinessModel.ADMINISTRATOR);
            boolean isTemplateAdmin = existingTemplate.getCode() != null || existingTemplate.getOwnerId() == null;

            if (currentUser != null && !Objects.equals(existingTemplate.getOwnerId(), currentUser.getId()))
                throw new ForbiddenException();

            if (!isUserAdmin && isTemplateAdmin)
                throw new ForbiddenException();

            template.setId(existingTemplate.getId());
            template.setCode(existingTemplate.getCode());
            template.setOwnerId(existingTemplate.getOwnerId());

            this.repository.updateTemplate(template.toDataModel());

            if (template.getPlaceholders().isEmpty())
                return concatPlaceholders(template);
            return template;
        }
    }

    @Override
    public void removeTemplate(UserBusinessModel currentUser, Long templateId) {
        EmailTemplateDataModel existingTemplate = repository.readTemplate(templateId);
        if (existingTemplate == null)
            return;

        if (existingTemplate.getOwnerId() == null || existingTemplate.getCode() != null)
            throw BadRequestDetail.CANNOT_BE_DELETED.toException();

        if (!Objects.equals(existingTemplate.getOwnerId(), currentUser.getId()))
            throw new ForbiddenException();

        repository.deleteTemplate(templateId);
    }
}
