package fr.iutdijon.eventmanager.services.email;

public class EmailServiceConstants {
    public static final String EMAIL_TEMPLATE_CODE_EMAIL_CONFIRMATION = "email-confirmation";
    public static final String EMAIL_TEMPLATE_CODE_LOGIN_FAILED = "login-failed";
    public static final String EMAIL_TEMPLATE_CODE_LOGIN_SUCCESSFUL = "login-successful";
    public static final String EMAIL_TEMPLATE_CODE_MAGIC_LINK = "magic-link";
    public static final String EMAIL_TEMPLATE_CODE_PASSWORD_RESET = "password-reset";
    public static final String EMAIL_TEMPLATE_CODE_TEAM_INVITATION = "team-invitation";
}
