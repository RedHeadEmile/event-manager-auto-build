package fr.iutdijon.eventmanager.services.email.models.business;

import jakarta.mail.internet.InternetAddress;

import java.io.UnsupportedEncodingException;

public class EmailAddressBusinessModel {
    private final String address;
    private final String name;

    public EmailAddressBusinessModel(String address, String name) {
        if (address.isNullOrEmpty())
            throw new IllegalArgumentException("Invalid email address");

        this.address = address;
        this.name = name;
    }

    public InternetAddress toInternetAddress() {
        try {
            return new InternetAddress(this.address, this.name);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
