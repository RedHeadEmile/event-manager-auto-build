package fr.iutdijon.eventmanager.services.migration.repositories;

import java.util.Set;

public interface IMigrationRepository {
    void playScript(String script);
    void createPassedScript(String scriptName);
    Set<String> readPassedScripts();
}
