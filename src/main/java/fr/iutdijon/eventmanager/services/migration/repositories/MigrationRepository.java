package fr.iutdijon.eventmanager.services.migration.repositories;

import com.mysql.cj.exceptions.MysqlErrorNumbers;
import fr.iutdijon.eventmanager.infrastructure.SingletonDatabaseConnection;
import org.intellij.lang.annotations.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Repository
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class MigrationRepository implements IMigrationRepository {

    private final SingletonDatabaseConnection databaseConnection;

    @Autowired
    public MigrationRepository(SingletonDatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    @Override
    public void playScript(@Language("SQL") String script) {
        this.databaseConnection.update(script);
    }

    @Override
    public void createPassedScript(String scriptName) {
        this.databaseConnection.update(
                "INSERT INTO datamigration (name, passedat) VALUES (?, ?)",
                scriptName, new java.sql.Timestamp(System.currentTimeMillis())
        );
    }

    @Override
    public Set<String> readPassedScripts() {
        try {
            return new HashSet<>(this.databaseConnection.query(
                    "SELECT name FROM datamigration",
                    (rs, unused) -> rs.getString(1)
            ));
        }
        catch (BadSqlGrammarException e) {
            if (e.getSQLException().getErrorCode() == MysqlErrorNumbers.ER_NO_SUCH_TABLE) {
                this.databaseConnection.update(
                        "CREATE TABLE datamigration (" +
                                "name VARCHAR(50) NOT NULL PRIMARY KEY," +
                                "passedat DATETIME(6) NOT NULL" +
                            ")"
                );
                return Collections.emptySet();
            }

            throw e;
        }
    }
}
