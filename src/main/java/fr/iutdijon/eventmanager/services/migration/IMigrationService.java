package fr.iutdijon.eventmanager.services.migration;

public interface IMigrationService {
    void doMigration();
}
