package fr.iutdijon.eventmanager.services.user.repositories;

import fr.iutdijon.eventmanager.services.user.models.data.UserDataModel;
import fr.iutdijon.eventmanager.services.user.models.data.UserMagicLinkDataModel;
import fr.iutdijon.eventmanager.services.user.models.data.UsersFiltersDataModel;

import java.util.List;

public interface IUserRepository {
    UserDataModel createUser(UserDataModel user);
    UserDataModel readUser(Long userId);
    List<UserDataModel> readUsers(List<Long> userIds);
    List<UserDataModel> readUsers(UsersFiltersDataModel filters);
    UserDataModel readUserByAuthToken(String authToken);
    UserDataModel readUserByEmail(String email);
    UserDataModel readUserByEmailConfirmationToken(String emailConfirmationToken);
    UserDataModel readUserByMagicLinkCode(String magicLinkCode, boolean ignoreUsed);
    UserDataModel readUserByPasswordResetToken(String passwordResetToken);
    void updateUser(UserDataModel user);

    UserMagicLinkDataModel createUserMagicLink(UserMagicLinkDataModel magicLink);
    void updateUserMagicLink(String magicLinkCode, boolean used);
}
