package fr.iutdijon.eventmanager.services.user.models.business;

import fr.iutdijon.eventmanager.services.email.models.business.EmailAddressBusinessModel;
import fr.iutdijon.eventmanager.services.repository.IRepositoryService;
import fr.iutdijon.eventmanager.services.repository.models.business.CityBusinessModel;
import fr.iutdijon.eventmanager.services.repository.models.business.DiplomaBusinessModel;
import fr.iutdijon.eventmanager.services.user.models.data.UserDataModel;
import fr.iutdijon.eventmanager.utils.FlagUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class UserBusinessModel {
    private Long id;

    private CityBusinessModel city;
    private DiplomaBusinessModel diploma;
    private Long profilePictureId;

    private String authToken;
    private Long createdAt;
    private String email;
    private String emailConfirmationToken;
    private boolean emailConfirmed;
    private String firstName;
    private String lastName;
    private Long nextLoginDate;
    private String password;
    private String passwordResetToken;
    private Long passwordResetTokenCreationDate;
    private Long permission;
    private Short remainingPasswordTries;
    private String trainingLabel;
    private String trainingLocation;
    private String trainingYear;

    private List<UserMagicLinkBusinessModel> magicLinks = new ArrayList<>();

    public boolean havePermission(UserPermissionBusinessModel permission) {
        // Developer > all
        return FlagUtils.haveFlag(this.permission, permission.value) || FlagUtils.haveFlag(this.permission, UserPermissionBusinessModel.DEVELOPER.value);
    }

    public EmailAddressBusinessModel toEmailAddress() {
        return new EmailAddressBusinessModel(email, firstName +' ' + lastName);
    }

    public UserDataModel toDataModel() {
        UserDataModel dataModel = new UserDataModel() {{
            setId(id);
            setProfilePictureId(profilePictureId);
            setAuthToken(authToken);
            setCreatedAt(new java.sql.Timestamp(createdAt));
            setEmail(email);
            setEmailConfirmationToken(emailConfirmationToken);
            setEmailConfirmed(emailConfirmed);
            setFirstName(firstName);
            setLastName(lastName);
            setNextLoginDate(nextLoginDate != null ? new java.sql.Timestamp(nextLoginDate) : null);
            setPassword(password);
            setPasswordResetToken(passwordResetToken);
            setPasswordResetTokenCreationDate(passwordResetTokenCreationDate != null ? new java.sql.Timestamp(passwordResetTokenCreationDate) : null);
            setPermission(permission);
            setRemainingPasswordTries(remainingPasswordTries);
            setTrainingLabel(trainingLabel);
            setTrainingLocation(trainingLocation);
            setTrainingYear(trainingYear);

            setMagicLinks(magicLinks.map(UserMagicLinkBusinessModel::toDataModel));
        }};

        if (city != null) dataModel.setCityId(city.getId());
        if (diploma != null) dataModel.setDiplomaId(diploma.getId());

        return dataModel;
    }

    public static UserBusinessModel fromDataModel(UserDataModel dataModel, IRepositoryService repositoryService) {
        return new UserBusinessModel() {{
            setId(dataModel.getId());

            setCity(dataModel.getCityId() != null ? repositoryService.getCity(dataModel.getCityId()) : null);
            setDiploma(dataModel.getDiplomaId() != null ? repositoryService.getDiploma(dataModel.getDiplomaId()) : null);
            setProfilePictureId(dataModel.getProfilePictureId());

            setAuthToken(dataModel.getAuthToken());
            setCreatedAt(dataModel.getCreatedAt().getTime());
            setEmail(dataModel.getEmail());
            setEmailConfirmationToken(dataModel.getEmailConfirmationToken());
            setEmailConfirmed(dataModel.isEmailConfirmed());
            setFirstName(dataModel.getFirstName());
            setLastName(dataModel.getLastName());
            setNextLoginDate(dataModel.getNextLoginDate() != null ? dataModel.getNextLoginDate().getTime() : null);
            setPassword(dataModel.getPassword());
            setPasswordResetToken(dataModel.getPasswordResetToken());
            setPasswordResetTokenCreationDate(dataModel.getPasswordResetTokenCreationDate() != null ? dataModel.getPasswordResetTokenCreationDate().getTime() : null);
            setPermission(dataModel.getPermission());
            setRemainingPasswordTries(dataModel.getRemainingPasswordTries());
            setTrainingLabel(dataModel.getTrainingLabel());
            setTrainingLocation(dataModel.getTrainingLocation());
            setTrainingYear(dataModel.getTrainingYear());

            setMagicLinks(dataModel.getMagicLinks().map(UserMagicLinkBusinessModel::fromDataModel));
        }};
    }
}
