package fr.iutdijon.eventmanager.services.user;

import fr.iutdijon.eventmanager.services.email.models.business.EmailAddressBusinessModel;
import fr.iutdijon.eventmanager.services.user.models.business.*;

import java.util.List;

public interface IUserService {
    //#region CurrentUser
    UserBusinessModel addCurrentUser(UserCreateRequestBusinessModel userCreateRequest);
    UserBusinessModel getCurrentUser();
    UserBusinessModel setCurrentUser(String email, String password);
    UserBusinessModel setCurrentUser(String magicLinkCode);
    UserBusinessModel setCurrentUserPassword(String oldPassword, String newPassword);
    UserBusinessModel setCurrentUserSettings(UserSettingsUpdateRequestBusinessModel settingsUpdateRequest);
    void sendCurrentUserConfirmationEmail();
    void removeCurrentUserCookie();
    //#endregion

    //#region ResetPassword
    void addResetUserPassword(String userEmail);
    void setUserPassword(String passwordResetToken, String newPassword);
    boolean isUserPasswordTokenValid(String passwordResetToken);
    //#endregion

    void addUserMagicLink(String userEmail, String redirectUrl);
    void setUserEmailConfirmed(String emailConfirmationToken);

    List<UserBusinessModel> getUsers(List<Long> userIds);
    List<UserBusinessModel> getUsers(UsersFiltersBusinessModel filters);
    void setUserPermission(Long userId, UserPermissionBusinessModel permission, boolean enabled);
    void sendUserConfirmationEmail(Long userId);
}
