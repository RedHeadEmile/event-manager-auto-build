package fr.iutdijon.eventmanager.services.user;

import at.favre.lib.crypto.bcrypt.BCrypt;
import fr.iutdijon.eventmanager.EventManagerProperties;
import fr.iutdijon.eventmanager.exceptions.BadRequestDetail;
import fr.iutdijon.eventmanager.exceptions.BadRequestException;
import fr.iutdijon.eventmanager.exceptions.TooManyRequestException;
import fr.iutdijon.eventmanager.exceptions.UnauthorizedException;
import fr.iutdijon.eventmanager.infrastructure.DatabaseTransaction;
import fr.iutdijon.eventmanager.infrastructure.RequestDatabaseConnection;
import fr.iutdijon.eventmanager.services.document.IDocumentService;
import fr.iutdijon.eventmanager.services.document.models.business.DocumentBusinessModel;
import fr.iutdijon.eventmanager.services.email.EmailServiceConstants;
import fr.iutdijon.eventmanager.services.email.IEmailService;
import fr.iutdijon.eventmanager.services.email.models.business.EmailAddressBusinessModel;
import fr.iutdijon.eventmanager.services.email.models.business.EmailBusinessModel;
import fr.iutdijon.eventmanager.services.email.models.business.EmailPlaceholderBusinessModel;
import fr.iutdijon.eventmanager.services.email.models.business.EmailTemplateBusinessModel;
import fr.iutdijon.eventmanager.services.repository.IRepositoryService;
import fr.iutdijon.eventmanager.services.user.models.business.*;
import fr.iutdijon.eventmanager.services.user.models.data.UserDataModel;
import fr.iutdijon.eventmanager.services.user.models.data.UserMagicLinkDataModel;
import fr.iutdijon.eventmanager.services.user.repositories.IUserRepository;
import fr.iutdijon.eventmanager.utils.FlagUtils;
import fr.iutdijon.eventmanager.utils.TokenUtils;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.*;

@Service
@RequestScope
public class UserService implements IUserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    private static final String AUTH_COOKIE_NAME = "EventManagerToken";
    private static final long PASSWORD_RESET_TOKEN_TIMEOUT = 60000 * 10; // 10 minutes
    private static final long MAGIC_LINK_TIMEOUT = 60000 * 10; // 10 minutes
    private static final long AUTHENTICATION_TIMEOUT = 60000 * 10; // 10 minutes

    private static String bypassToken;

    /**
     * As the service is {@link RequestScope}, it permits to know if the current user has already been fetch.
     */
    private boolean isInitialized = false;
    private UserBusinessModel currentUser;

    private final HttpServletRequest request;
    private final HttpServletResponse response;

    private final IUserRepository repository;
    private final RequestDatabaseConnection databaseConnection;

    private final EventManagerProperties properties;

    private final IDocumentService documentService;
    private final IEmailService emailService;
    private final IRepositoryService repositoryService;

    @Autowired
    public UserService(
            HttpServletRequest request,
            HttpServletResponse response,

            IUserRepository repository,
            RequestDatabaseConnection databaseConnection,

            EventManagerProperties properties,

            IDocumentService documentService,
            IEmailService emailService,
            IRepositoryService repositoryService,

            @Qualifier("devMode") boolean devMode
    ) {
        this.request = request;
        this.response = response;
        this.repository = repository;
        this.databaseConnection = databaseConnection;
        this.properties = properties;
        this.documentService = documentService;
        this.emailService = emailService;
        this.repositoryService = repositoryService;

        if (bypassToken == null && devMode) {
            bypassToken = TokenUtils.generateRandomString(32);
            logger.info("Bypass user token: " + bypassToken);
            logger.info("Query params: `bypasstoken` and `userid`");
        }
    }

    //#region Private

    /**
     * Update the auth token cookie of the current user.
     * @param authToken New auth token of the current user. Use null to remove the auth token.
     */
    private void setCurrentUserCookie(String authToken) {
        Cookie cookie = new Cookie(AUTH_COOKIE_NAME, authToken);
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        cookie.setMaxAge(authToken == null ? 0 : 60 * 60 * 24 * 30);
        cookie.setAttribute("SameSite", "Lax");
        response.addCookie(cookie);
    }

    /**
     * Generate a valid auth token for a new user.<br/>
     * An auth token is valid when it is unique for each user.
     * @return The generated valid token.
     */
    private String generateValidAuthToken() {
        String authToken;
        UserDataModel existingUser;
        do {
            authToken = TokenUtils.generateRandomString(128);
            existingUser = this.repository.readUserByAuthToken(authToken);
        }
        while (existingUser != null);

        return authToken;
    }

    /**
     * Generate a valid email confirmation token<br/>
     * An email confirmation token is valid when it is unique for each user.
     * @return The generated valid token.
     */
    private String generateValidEmailConfirmationToken() {
        String emailConfirmationToken;
        UserDataModel existingUser;
        do {
            emailConfirmationToken = TokenUtils.generateRandomString(32);
            existingUser = this.repository.readUserByEmailConfirmationToken(emailConfirmationToken);
        }
        while (existingUser != null);

        return emailConfirmationToken;
    }

    /**
     * Hash a given password with the {@link BCrypt} library
     * @param password The password to hash
     * @return The hashed password
     */
    private String hashPassword(String password) {
        return BCrypt.withDefaults().hashToString(6, password.toCharArray());
    }

    /**
     * Check if a given password match a hashed password
     * @param clearPassword The given password without hash or encryption
     * @param hashedPassword The hashed password
     * @return True if they match, False otherwise
     */
    private boolean verifyPassword(String clearPassword, String hashedPassword) {
        return BCrypt.verifyer().verify(clearPassword.toCharArray(), hashedPassword.toCharArray()).verified;
    }
    //#endregion

    //#region CurrentUser
    @Override
    public UserBusinessModel addCurrentUser(UserCreateRequestBusinessModel userCreateRequest) {
        UserDataModel conflictingUser = this.repository.readUserByEmail(userCreateRequest.getEmail());
        if (conflictingUser != null)
            throw BadRequestDetail.EMAIL_ALREADY_IN_USE.toException();

        String authToken = this.generateValidAuthToken();

        UserBusinessModel user = new UserBusinessModel();
        user.setAuthToken(authToken);
        user.setCreatedAt(System.currentTimeMillis());
        user.setEmail(userCreateRequest.getEmail().toLowerCase());
        user.setEmailConfirmationToken(generateValidEmailConfirmationToken());
        user.setEmailConfirmed(false);
        user.setFirstName(userCreateRequest.getFirstName());
        user.setLastName(userCreateRequest.getLastName());
        user.setPassword(hashPassword(userCreateRequest.getPassword()));
        user.setPermission(0L);
        user.setRemainingPasswordTries((short) 3);

        EmailTemplateBusinessModel template = emailService.getTemplate(null, EmailServiceConstants.EMAIL_TEMPLATE_CODE_EMAIL_CONFIRMATION);
        if (template == null)
            throw new IllegalStateException("Unable to read the email template");

        UserDataModel dataModel = this.repository.createUser(user.toDataModel());
        UserBusinessModel businessModel = UserBusinessModel.fromDataModel(dataModel, repositoryService);
        this.isInitialized = true;
        this.currentUser = businessModel;
        this.setCurrentUserCookie(authToken);

        emailService.sendEmailAsync(new EmailBusinessModel() {{
            setTo(Collections.singletonList(user.toEmailAddress()));
            setSubject(template.getSubject());
            setContent(template.getContent());
        }}, new HashMap<>() {{
            put(EmailPlaceholderBusinessModel.LASTNAME, user.getLastName());
            put(EmailPlaceholderBusinessModel.FIRSTNAME, user.getFirstName());
            put(EmailPlaceholderBusinessModel.EMAIL_CONFIRMATION_LINK, properties.getUiBaseUrl() + "/email-confirm?code=" + user.getEmailConfirmationToken());
        }});

        return businessModel;
    }

    @Override
    public UserBusinessModel getCurrentUser() {
        if (isInitialized)
            return currentUser;

        if (bypassToken != null) {
            String requestBypassToken = request.getParameter("bypasstoken");
            String userId = request.getParameter("userid");

            if (requestBypassToken != null) {
                if (!bypassToken.equals(requestBypassToken))
                    throw new BadRequestException("Invalid bypass token");

                long parsedUserId;
                try {
                    parsedUserId = Long.parseLong(userId);
                } catch (NumberFormatException ignore) {
                    throw new BadRequestException("Invalid user id");
                }

                UserDataModel dataModel = this.repository.readUser(parsedUserId);
                if (dataModel == null)
                    throw new BadRequestException("Invalid user id");

                UserBusinessModel businessModel = UserBusinessModel.fromDataModel(dataModel, repositoryService);
                this.isInitialized = true;
                this.currentUser = businessModel;
                return businessModel;
            }
        }

        if (request.getCookies() == null)
            return null;

        Cookie tokenCookie = null;
        for (Cookie cookie : request.getCookies())
            if (cookie.getName().equals(AUTH_COOKIE_NAME)) {
                tokenCookie = cookie;
                break;
            }

        if (tokenCookie == null || tokenCookie.getValue() == null)
            return null;

        String authToken = tokenCookie.getValue();
        UserDataModel dataModel = this.repository.readUserByAuthToken(authToken);
        if (dataModel == null)
            return null;

        UserBusinessModel businessModel = UserBusinessModel.fromDataModel(dataModel, repositoryService);
        this.isInitialized = true;
        this.currentUser = businessModel;
        this.setCurrentUserCookie(dataModel.getAuthToken());

        return businessModel;
    }

    private void sendConnectionNotification(UserDataModel user) {
        EmailTemplateBusinessModel template = emailService.getTemplate(null, EmailServiceConstants.EMAIL_TEMPLATE_CODE_LOGIN_SUCCESSFUL);
        if (template == null)
            throw new IllegalStateException("Unable to read the email template");

        emailService.sendEmailAsync(new EmailBusinessModel() {{
            setTo(Collections.singletonList(new EmailAddressBusinessModel(user.getEmail(), user.getFirstName() + ' ' + user.getLastName())));
            setSubject(template.getSubject());
            setContent(template.getContent());
        }}, new HashMap<>() {{
            put(EmailPlaceholderBusinessModel.LASTNAME, user.getLastName());
            put(EmailPlaceholderBusinessModel.FIRSTNAME, user.getFirstName());
        }});
    }

    @Override
    public UserBusinessModel setCurrentUser(String email, String password) {
        UserDataModel dataModel = this.repository.readUserByEmail(email.toLowerCase());
        if (dataModel == null)
            throw new UnauthorizedException();

        boolean userUpdateRequired = false;
        if (dataModel.getNextLoginDate() != null) {
            long retryAfter = dataModel.getNextLoginDate().getTime() - System.currentTimeMillis();
            if (retryAfter > 0)
                throw new TooManyRequestException(retryAfter / 1000);

            dataModel.setNextLoginDate(null);
            dataModel.setRemainingPasswordTries((short) 3);
            userUpdateRequired = true;
        }

        if (!verifyPassword(password, dataModel.getPassword())) {
            int remainingTries = 0;
            if (dataModel.getRemainingPasswordTries() == 0) {
                dataModel.setNextLoginDate(new java.sql.Timestamp(System.currentTimeMillis() + AUTHENTICATION_TIMEOUT));

                EmailTemplateBusinessModel template = emailService.getTemplate(null, EmailServiceConstants.EMAIL_TEMPLATE_CODE_LOGIN_FAILED);
                if (template == null)
                    throw new IllegalStateException("Unable to read the email template");

                emailService.sendEmailAsync(new EmailBusinessModel() {{
                    setTo(Collections.singletonList(new EmailAddressBusinessModel(dataModel.getEmail(), dataModel.getFirstName() + ' ' + dataModel.getLastName())));
                    setSubject(template.getSubject());
                    setContent(template.getContent());
                }}, new HashMap<>() {{
                    put(EmailPlaceholderBusinessModel.LASTNAME, dataModel.getLastName());
                    put(EmailPlaceholderBusinessModel.FIRSTNAME, dataModel.getFirstName());
                }});
            }
            else
                dataModel.setRemainingPasswordTries((short) ((remainingTries = dataModel.getRemainingPasswordTries()) - 1));
            repository.updateUser(dataModel);

            throw new UnauthorizedException(remainingTries);
        }

        sendConnectionNotification(dataModel);

        if (userUpdateRequired || dataModel.getRemainingPasswordTries() != 3) {
            dataModel.setRemainingPasswordTries((short) 3);
            repository.updateUser(dataModel);
        }

        UserBusinessModel businessModel = UserBusinessModel.fromDataModel(dataModel, repositoryService);
        this.isInitialized = true;
        this.currentUser = businessModel;
        this.setCurrentUserCookie(dataModel.getAuthToken());

        return businessModel;
    }

    @Override
    public UserBusinessModel setCurrentUser(String magicLinkCode) {
        UserDataModel dataModel = this.repository.readUserByMagicLinkCode(magicLinkCode, true);
        if (dataModel == null)
            throw new UnauthorizedException();

        boolean isValidCode = false;
        for (UserMagicLinkDataModel magicLink : dataModel.getMagicLinks()) {
            if (!magicLink.getCode().equals(magicLinkCode))
                continue;

            isValidCode = magicLink.getCreatedAt().getTime() >= System.currentTimeMillis() - MAGIC_LINK_TIMEOUT;
            break;
        }

        if (!isValidCode)
            throw new UnauthorizedException();

        try (DatabaseTransaction trx = databaseConnection.beginTransaction()) {
            sendConnectionNotification(dataModel);

            dataModel.setNextLoginDate(null);
            dataModel.setRemainingPasswordTries((short) 3);
            repository.updateUser(dataModel);

            UserBusinessModel businessModel = UserBusinessModel.fromDataModel(dataModel, repositoryService);
            this.isInitialized = true;
            this.currentUser = businessModel;
            this.setCurrentUserCookie(dataModel.getAuthToken());

            this.repository.updateUserMagicLink(magicLinkCode, true);

            trx.commit();
            return businessModel;
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public UserBusinessModel setCurrentUserPassword(String oldPassword, String newPassword) {
        UserBusinessModel oldModel = this.getCurrentUser();
        if (oldModel == null)
            throw new UnauthorizedException();

        if (!verifyPassword(oldPassword, oldModel.getPassword()))
            throw new UnauthorizedException();

        // Renew auth token on password change
        String newToken = this.generateValidAuthToken();
        oldModel.setAuthToken(newToken);
        oldModel.setPassword(hashPassword(newPassword));
        this.repository.updateUser(oldModel.toDataModel());

        setCurrentUserCookie(newToken);

        return oldModel;
    }

    private final List<String> allowedProfilePictureMimeType = Arrays.asList(MediaType.IMAGE_GIF_VALUE, MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE);

    @Override
    public UserBusinessModel setCurrentUserSettings(UserSettingsUpdateRequestBusinessModel settingsUpdateRequest) {
        UserBusinessModel oldModel = this.getCurrentUser();
        if (oldModel == null)
            throw new UnauthorizedException();

        EmailTemplateBusinessModel emailConfirmationTemplate = null;
        boolean renewEmailConfirmation = false;
        try (DatabaseTransaction trx = databaseConnection.beginTransaction()) {
            if (settingsUpdateRequest.getEmail() != null && !settingsUpdateRequest.getEmail().isBlank() && !oldModel.getEmail().equals(settingsUpdateRequest.getEmail().toLowerCase())) {
                oldModel.setEmail(settingsUpdateRequest.getEmail().toLowerCase());
                oldModel.setEmailConfirmationToken(generateValidEmailConfirmationToken());
                oldModel.setEmailConfirmed(false);

                UserDataModel conflictingUser = repository.readUserByEmail(settingsUpdateRequest.getEmail());
                if (conflictingUser != null)
                    throw BadRequestDetail.EMAIL_ALREADY_IN_USE.toException();

                emailConfirmationTemplate = emailService.getTemplate(null, EmailServiceConstants.EMAIL_TEMPLATE_CODE_EMAIL_CONFIRMATION);
                if (emailConfirmationTemplate == null)
                    throw new IllegalStateException("Unable to read the email template");
                renewEmailConfirmation = true;
            }

            Long documentIdToDeleteAfterUserUpdate = null;
            if (settingsUpdateRequest.isPushProfilePicture()) {
                if (settingsUpdateRequest.getBase64ProfilePicture() == null && oldModel.getProfilePictureId() != null) {
                    documentIdToDeleteAfterUserUpdate = oldModel.getProfilePictureId();
                    oldModel.setProfilePictureId(null);
                }
                else if (settingsUpdateRequest.getBase64ProfilePicture() != null) {
                    if (settingsUpdateRequest.getProfilePictureMimeType() == null || !allowedProfilePictureMimeType.contains(settingsUpdateRequest.getProfilePictureMimeType()))
                        throw BadRequestDetail.INVALID_DOCUMENT_MIME_TYPE.toException();

                    DocumentBusinessModel newProfilePicture = documentService.addDocument(new DocumentBusinessModel() {{
                        setName("User profile picture");
                        setType(settingsUpdateRequest.getProfilePictureMimeType());
                        setContent(Base64.getDecoder().decode(settingsUpdateRequest.getBase64ProfilePicture()));
                    }});

                    documentIdToDeleteAfterUserUpdate = oldModel.getProfilePictureId();
                    oldModel.setProfilePictureId(newProfilePicture.getId());
                }
            }

            oldModel.setCity(settingsUpdateRequest.getCity());
            oldModel.setDiploma(settingsUpdateRequest.getDiploma());

            oldModel.setTrainingLabel(settingsUpdateRequest.getTrainingLabel());
            oldModel.setTrainingLocation(settingsUpdateRequest.getTrainingLocation());
            oldModel.setTrainingYear(settingsUpdateRequest.getTrainingYear());

            this.repository.updateUser(oldModel.toDataModel());
            if (documentIdToDeleteAfterUserUpdate != null)
                documentService.removeDocument(documentIdToDeleteAfterUserUpdate);

            if (renewEmailConfirmation) {
                EmailBusinessModel emailBusinessModel = new EmailBusinessModel() {{
                    setTo(Collections.singletonList(oldModel.toEmailAddress()));
                }};
                emailBusinessModel.setContent(emailConfirmationTemplate.getContent());
                emailBusinessModel.setSubject(emailConfirmationTemplate.getSubject());
                emailService.sendEmailAsync(emailBusinessModel, new HashMap<>() {{
                    put(EmailPlaceholderBusinessModel.LASTNAME, oldModel.getLastName());
                    put(EmailPlaceholderBusinessModel.FIRSTNAME, oldModel.getFirstName());
                    put(EmailPlaceholderBusinessModel.EMAIL_CONFIRMATION_LINK, properties.getUiBaseUrl() + "/email-confirm?code=" + oldModel.getEmailConfirmationToken());
                }});
            }

            trx.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return oldModel;
    }

    @Override
    public void sendCurrentUserConfirmationEmail() {
        UserBusinessModel currentUser = this.getCurrentUser();
        if (currentUser == null)
            throw new UnauthorizedException();

        sendUserConfirmationEmail(currentUser.toDataModel());
    }

    @Override
    public void removeCurrentUserCookie() {
        setCurrentUserCookie(null);
        this.isInitialized = true;
        this.currentUser = null;
    }
    //#endregion

    //#region ResetPassword
    @Override
    public void addResetUserPassword(String userEmail) {
        UserDataModel existingUser = repository.readUserByEmail(userEmail.toLowerCase());
        if (existingUser == null)
            return;

        EmailTemplateBusinessModel template = emailService.getTemplate(null, EmailServiceConstants.EMAIL_TEMPLATE_CODE_PASSWORD_RESET);
        if (template == null)
            throw new IllegalStateException("Unable to read the email template");

        String availablePasswordResetToken;
        UserDataModel conflictingUser;
        do {
            availablePasswordResetToken = TokenUtils.generateRandomString(32);
            conflictingUser = repository.readUserByPasswordResetToken(availablePasswordResetToken);
        }
        while (conflictingUser != null);

        existingUser.setPasswordResetToken(availablePasswordResetToken);
        existingUser.setPasswordResetTokenCreationDate(new java.sql.Timestamp(System.currentTimeMillis()));
        repository.updateUser(existingUser);

        String passwordResetUrl = properties.getUiBaseUrl() + "/password-reset?code=" + availablePasswordResetToken;
        emailService.sendEmailAsync(new EmailBusinessModel() {{
            setTo(Collections.singletonList(new EmailAddressBusinessModel(existingUser.getEmail(), existingUser.getFirstName() + ' ' + existingUser.getLastName())));
            setSubject(template.getSubject());
            setContent(template.getContent());
        }}, new HashMap<>() {{
            put(EmailPlaceholderBusinessModel.LASTNAME, existingUser.getLastName());
            put(EmailPlaceholderBusinessModel.FIRSTNAME, existingUser.getFirstName());
            put(EmailPlaceholderBusinessModel.PASSWORD_RESET_LINK,  passwordResetUrl);
        }});
    }

    @Override
    public void setUserPassword(String passwordResetToken, String newPassword) {
        UserDataModel user = this.repository.readUserByPasswordResetToken(passwordResetToken);
        if (user == null)
            throw new UnauthorizedException();

        if (user.getPasswordResetTokenCreationDate() == null || System.currentTimeMillis() - user.getPasswordResetTokenCreationDate().getTime() > PASSWORD_RESET_TOKEN_TIMEOUT)
            throw new UnauthorizedException();

        user.setPassword(hashPassword(newPassword));
        user.setPasswordResetToken(null);
        user.setPasswordResetTokenCreationDate(null);

        repository.updateUser(user);
    }

    @Override
    public boolean isUserPasswordTokenValid(String passwordResetToken) {
        UserDataModel user = this.repository.readUserByPasswordResetToken(passwordResetToken);
        if (user == null)
            return false;

        return System.currentTimeMillis() - user.getPasswordResetTokenCreationDate().getTime() <= PASSWORD_RESET_TOKEN_TIMEOUT;
    }
    //#endregion

    @Override
    public void addUserMagicLink(String userEmail, String redirectUrl) {
        UserDataModel existingUser = repository.readUserByEmail(userEmail.toLowerCase());
        if (existingUser == null)
            return;

        EmailTemplateBusinessModel template = emailService.getTemplate(null, EmailServiceConstants.EMAIL_TEMPLATE_CODE_MAGIC_LINK);
        if (template == null)
            throw new IllegalStateException("Unable to read the email template");

        String availableMagicCode;
        UserDataModel conflictingUser;
        do {
            availableMagicCode = TokenUtils.generateRandomString(32);
            conflictingUser = repository.readUserByMagicLinkCode(availableMagicCode, false);
        }
        while (conflictingUser != null);

        UserMagicLinkDataModel magicLink = new UserMagicLinkDataModel() {{
            setUserId(existingUser.getId());
            setCreatedAt(new java.sql.Timestamp(System.currentTimeMillis()));
            setUsed(false);
        }};
        magicLink.setCode(availableMagicCode);
        repository.createUserMagicLink(magicLink);

        String redirectUrlSuffix = "";
        if (!redirectUrl.isNullOrEmpty())
            redirectUrlSuffix = "&redirect-url=" + URLEncoder.encode(redirectUrl, StandardCharsets.UTF_8);

        String magicLinkUrl = properties.getUiBaseUrl() + "/login?code=" + availableMagicCode + redirectUrlSuffix;
        emailService.sendEmailAsync(new EmailBusinessModel() {{
            setTo(Collections.singletonList(new EmailAddressBusinessModel(existingUser.getEmail(), existingUser.getFirstName() + ' ' + existingUser.getLastName())));
            setSubject(template.getSubject());
            setContent(template.getContent());
        }}, new HashMap<>() {{
            put(EmailPlaceholderBusinessModel.LASTNAME, existingUser.getLastName());
            put(EmailPlaceholderBusinessModel.FIRSTNAME, existingUser.getFirstName());
            put(EmailPlaceholderBusinessModel.MAGIC_LINK, magicLinkUrl);
        }});
    }

    @Override
    public void setUserEmailConfirmed(String emailConfirmationToken) {
        UserDataModel user = this.repository.readUserByEmailConfirmationToken(emailConfirmationToken);
        if (user == null)
            throw BadRequestDetail.INVALID_EMAIL_CONFIRMATION_TOKEN.toException();

        user.setEmailConfirmationToken(null);
        user.setEmailConfirmed(true);
        this.repository.updateUser(user);
    }

    @Override
    public List<UserBusinessModel> getUsers(List<Long> userIds) {
        return this.repository.readUsers(userIds).map(user -> UserBusinessModel.fromDataModel(user, repositoryService));
    }

    @Override
    public List<UserBusinessModel> getUsers(UsersFiltersBusinessModel filters) {
        if (filters.getLimit() == null || filters.getLimit() > 50)
            filters.setLimit(50);

        if (filters.getOffset() == null || filters.getOffset() < 0)
            filters.setOffset(0);

        return this.repository.readUsers(filters.toDataModel()).map(user -> UserBusinessModel.fromDataModel(user, repositoryService));
    }

    @Override
    public void setUserPermission(Long userId, UserPermissionBusinessModel permission, boolean enabled) {
        UserDataModel user = repository.readUser(userId);
        if (user == null)
            throw new BadRequestException("Invalid user id");

        if (enabled)
            user.setPermission(FlagUtils.addFlag(user.getPermission(), permission.value));
        else
            user.setPermission(FlagUtils.removeFlag(user.getPermission(), permission.value));

        repository.updateUser(user);
    }

    @Override
    public void sendUserConfirmationEmail(Long userId) {
        UserDataModel user = repository.readUser(userId);
        if (user == null)
            throw new BadRequestException("Invalid user id");

        sendUserConfirmationEmail(user);
    }

    private void sendUserConfirmationEmail(UserDataModel user) {
        if (user.isEmailConfirmed())
            throw BadRequestDetail.EMAIL_ALREADY_CONFIRMED.toException();

        EmailTemplateBusinessModel template = emailService.getTemplate(null, EmailServiceConstants.EMAIL_TEMPLATE_CODE_EMAIL_CONFIRMATION);
        if (template == null)
            throw new IllegalStateException("Unable to read the email template");

        if (user.getEmailConfirmationToken() == null || user.getEmailConfirmationToken().isBlank()) {
            user.setEmailConfirmationToken(this.generateValidEmailConfirmationToken());
            this.repository.updateUser(user);
        }

        String emailConfirmationUrl = properties.getUiBaseUrl() + "/email-confirm?code=" + user.getEmailConfirmationToken();
        emailService.sendEmailAsync(new EmailBusinessModel() {{
            setTo(Collections.singletonList(new EmailAddressBusinessModel(user.getEmail(), user.getFirstName() + ' ' + user.getLastName())));
            setSubject(template.getSubject());
            setContent(template.getContent());
        }}, new HashMap<>() {{
            put(EmailPlaceholderBusinessModel.LASTNAME, user.getLastName());
            put(EmailPlaceholderBusinessModel.FIRSTNAME, user.getFirstName());
            put(EmailPlaceholderBusinessModel.EMAIL_CONFIRMATION_LINK, emailConfirmationUrl);
        }});
    }
}
