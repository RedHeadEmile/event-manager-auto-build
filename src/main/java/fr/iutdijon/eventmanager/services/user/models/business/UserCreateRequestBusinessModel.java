package fr.iutdijon.eventmanager.services.user.models.business;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCreateRequestBusinessModel {
    private String email;
    private String firstName;
    private String lastName;
    private String password;
}
