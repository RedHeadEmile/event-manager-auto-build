package fr.iutdijon.eventmanager.services.user.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
@Setter
public class UserMagicLinkDataModel {
    @JDapperColumnName("usermagiclinkid")
    private Long id;
    @JDapperColumnName("user_userid")
    private Long userId;
    @JDapperColumnName("code")
    private String code;
    @JDapperColumnName("createdat")
    private java.sql.Timestamp createdAt;
    @JDapperColumnName("used")
    private Boolean used;
}
