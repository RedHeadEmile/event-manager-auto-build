package fr.iutdijon.eventmanager.services.log.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
@Setter
public class LogDataModel {
    @JDapperColumnName("logid")
    private Long id;

    @JDapperColumnName("calledat")
    private java.sql.Timestamp calledAt;
    @JDapperColumnName("caller")
    private String caller;
    @JDapperColumnName("elapsedtime")
    private Long elapsedTime;
    @JDapperColumnName("requestcontent")
    private String requestContent;
    @JDapperColumnName("requestmethod")
    private String requestMethod;
    @JDapperColumnName("requestroute")
    private String requestRoute;
    @JDapperColumnName("responsecode")
    private Integer responseCode;
    @JDapperColumnName("responsecontent")
    private String responseContent;
}
