package fr.iutdijon.eventmanager.services.log.models.data;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LogFiltersDataModel {
    private java.sql.Timestamp minCallTime;
    private java.sql.Timestamp maxCallTime;

    private Integer responseCode;
}
