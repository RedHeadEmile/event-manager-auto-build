package fr.iutdijon.eventmanager.services.log.models.business;

import fr.iutdijon.eventmanager.services.log.models.data.LogDataModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LogBusinessModel {
    private Long id;
    private Long calledAt;
    private String caller;
    private Long elapsedTime;
    private String requestContent;
    private String requestMethod;
    private String requestRoute;
    private Integer responseCode;
    private String responseContent;

    public LogDataModel toDataModel() {
        return new LogDataModel() {{
           setId(id);
           setCalledAt(new java.sql.Timestamp(calledAt));
           setCaller(caller);
           setElapsedTime(elapsedTime);
           setRequestContent(requestContent);
           setRequestMethod(requestMethod);
           setRequestRoute(requestRoute);
           setResponseCode(responseCode);
           setResponseContent(responseContent);
        }};
    }

    public static LogBusinessModel fromDataModel(LogDataModel dataModel) {
        return new LogBusinessModel() {{
            setId(dataModel.getId());
            setCalledAt(dataModel.getCalledAt().getTime());
            setCaller(dataModel.getCaller());
            setElapsedTime(dataModel.getElapsedTime());
            setRequestContent(dataModel.getRequestContent());
            setRequestMethod(dataModel.getRequestMethod());
            setRequestRoute(dataModel.getRequestRoute());
            setResponseCode(dataModel.getResponseCode());
            setResponseContent(dataModel.getResponseContent());
        }};
    }
}
