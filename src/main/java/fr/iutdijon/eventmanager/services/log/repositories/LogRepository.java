package fr.iutdijon.eventmanager.services.log.repositories;

import fr.iutdijon.eventmanager.infrastructure.RequestDatabaseConnection;
import fr.iutdijon.eventmanager.services.log.models.data.LogDataModel;
import fr.iutdijon.eventmanager.services.log.models.data.LogFiltersDataModel;
import net.redheademile.jdapper.JDapper;
import org.intellij.lang.annotations.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.RequestScope;

import java.util.ArrayList;
import java.util.List;

@Repository
@RequestScope
public class LogRepository implements ILogRepository {

    private final RequestDatabaseConnection databaseConnection;

    @Autowired
    public LogRepository(RequestDatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    @Override
    public void createLog(LogDataModel log) {
        databaseConnection.update(
                "INSERT INTO log (calledat, caller, elapsedtime, requestcontent, requestmethod, requestroute, responsecode, responsecontent) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                log.getCalledAt(), log.getCaller(), log.getElapsedTime(), log.getRequestContent(), log.getRequestMethod(), log.getRequestRoute(), log.getResponseCode(), log.getResponseContent()
        );
    }

    @Override
    public List<LogDataModel> readLogs(LogFiltersDataModel filters) {
        @Language("SQL") String query = "SELECT * FROM log";

        List<String> whereClauses = new ArrayList<>();
        List<Object> params = new ArrayList<>();

        if (filters.getMinCallTime() != null) {
            whereClauses.add("calledat >= ?");
            params.add(filters.getMinCallTime());
        }

        if (filters.getMaxCallTime() != null) {
            whereClauses.add("calledat <= ?");
            params.add(filters.getMaxCallTime());
        }

        if (filters.getResponseCode() != null) {
            whereClauses.add("responsecode = ?");
            params.add(filters.getResponseCode());
        }

        if (!whereClauses.isEmpty())
            query += " WHERE " + String.join(" AND ", whereClauses);

        query += " ORDER BY calledat ASC";

        return this.databaseConnection.query(query, JDapper.getMapper(LogDataModel.class), params.toArray());
    }
}
