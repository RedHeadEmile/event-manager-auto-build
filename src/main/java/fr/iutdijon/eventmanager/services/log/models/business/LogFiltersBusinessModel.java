package fr.iutdijon.eventmanager.services.log.models.business;

import fr.iutdijon.eventmanager.services.log.models.data.LogFiltersDataModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LogFiltersBusinessModel {
    private Long minCallTime;
    private Long maxCallTime;

    private Integer responseCode;

    public LogFiltersDataModel toDataModel() {
        return new LogFiltersDataModel() {{
           setMinCallTime(minCallTime != null ? new java.sql.Timestamp(minCallTime) : null);
           setMaxCallTime(maxCallTime != null ? new java.sql.Timestamp(maxCallTime) : null);
           setResponseCode(responseCode);
        }};
    }
}
