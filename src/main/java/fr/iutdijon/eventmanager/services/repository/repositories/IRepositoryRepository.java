package fr.iutdijon.eventmanager.services.repository.repositories;

import fr.iutdijon.eventmanager.services.repository.models.data.CityDataModel;
import fr.iutdijon.eventmanager.services.repository.models.data.DiplomaDataModel;

import java.util.List;

public interface IRepositoryRepository {
    List<CityDataModel> readCities();
    List<DiplomaDataModel> readDiplomas();
}
