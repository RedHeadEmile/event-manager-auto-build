package fr.iutdijon.eventmanager.services.repository.models.business;

import fr.iutdijon.eventmanager.services.repository.models.data.DiplomaDataModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DiplomaBusinessModel {
    private Integer id;
    private String label;
    private String normalizedLabel;

    public static DiplomaBusinessModel fromDataModel(DiplomaDataModel dataModel) {
        return new DiplomaBusinessModel() {{
            setId(dataModel.getId());
            setLabel(dataModel.getLabel());
        }};
    }
}
