package fr.iutdijon.eventmanager.services.repository.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
@Setter
public class CityDataModel {
    @JDapperColumnName("cityid")
    private Integer id;
    @JDapperColumnName("label")
    private String label;
    @JDapperColumnName("postcode")
    private String postCode;
}
