package fr.iutdijon.eventmanager.services.statistics;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

@Service
@RequestScope
public class StatisticsService implements IStatisticsService {

    public StatisticsService() {

    }

    @Override
    public byte[] getStatisticsExcelFile(List<Long> eventIds) {
        try (XSSFWorkbook workbook = new XSSFWorkbook())
        {
            Sheet sheet = workbook.createSheet("Statistiques");

            Row row = sheet.createRow(0);
            row.createCell(0).setCellValue("Nom de l'événement");
            row.createCell(1).setCellValue("Nombre de participants");
            row.createCell(2).setCellValue("Nombre de participants ayant payé");

            ByteArrayOutputStream output = new ByteArrayOutputStream();
            workbook.write(output);

            return output.toByteArray();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
