package fr.iutdijon.eventmanager.services.statistics;

import java.util.List;

public interface IStatisticsService {
    /**
     * Get statistics document from a wanted events
     * @param eventIds List of the ids of the events you want to get the statistics from.
     * @return A byte array representing an Excel file
     */
    byte[] getStatisticsExcelFile(List<Long> eventIds);
}
