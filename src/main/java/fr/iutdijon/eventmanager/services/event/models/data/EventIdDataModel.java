package fr.iutdijon.eventmanager.services.event.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
@Setter
public class EventIdDataModel {

    @JDapperColumnName("event_eventid")
    private Long id;

}
