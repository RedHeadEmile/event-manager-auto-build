package fr.iutdijon.eventmanager.services.event;

import fr.iutdijon.eventmanager.exceptions.BadRequestDetail;
import fr.iutdijon.eventmanager.exceptions.BadRequestException;
import fr.iutdijon.eventmanager.exceptions.ForbiddenException;
import fr.iutdijon.eventmanager.exceptions.UnauthorizedException;
import fr.iutdijon.eventmanager.infrastructure.DatabaseTransaction;
import fr.iutdijon.eventmanager.infrastructure.RequestDatabaseConnection;
import fr.iutdijon.eventmanager.services.document.IDocumentService;
import fr.iutdijon.eventmanager.services.document.models.business.DocumentBusinessModel;
import fr.iutdijon.eventmanager.services.email.IEmailService;
import fr.iutdijon.eventmanager.services.email.models.business.EmailAddressBusinessModel;
import fr.iutdijon.eventmanager.services.email.models.business.EmailBusinessModel;
import fr.iutdijon.eventmanager.services.email.models.business.EmailPlaceholderBusinessModel;
import fr.iutdijon.eventmanager.services.event.models.business.*;
import fr.iutdijon.eventmanager.services.event.models.data.*;
import fr.iutdijon.eventmanager.services.event.repositories.IEventRepository;
import fr.iutdijon.eventmanager.services.user.IUserService;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import fr.iutdijon.eventmanager.services.user.models.business.UserPermissionBusinessModel;
import fr.iutdijon.eventmanager.utils.TokenUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.CompletableFuture;

@Service
@RequestScope
public class EventService implements IEventService {
    private final RequestDatabaseConnection databaseConnection;
    private final IEventRepository repository;

    private final IDocumentService documentService;
    private final IEmailService emailService;
    private final IUserService userService;

    private final List<IEventServiceObservable> observables;

    public EventService(
            RequestDatabaseConnection databaseConnection,
            IEventRepository repository,

            IDocumentService documentService,
            IEmailService emailService,
            IUserService userService,

            List<IEventServiceObservable> observables
    ) {
        this.databaseConnection = databaseConnection;
        this.repository = repository;
        this.documentService = documentService;
        this.emailService = emailService;
        this.userService = userService;
        this.observables = observables;
    }

    /**
     * To generate a valid event code : Code not already generated for another event.
     * @return String - Generated event code valid
     */
    private String generateValidEventCode() {
        String eventCode;
        EventDataModel existingEvent;
        do {
            eventCode = TokenUtils.generateRandomString(8);
            existingEvent = this.repository.readEventbyCode(eventCode);
        }
        while (existingEvent != null);

        return eventCode;
    }

    private final List<String> allowedDocumentRequirementMimeType = Arrays.asList(MediaType.IMAGE_GIF_VALUE, MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.APPLICATION_PDF_VALUE);

    /**
     * Update the document of a {@link EventDocumentRequirementBusinessModel}.
     * @param originalInstance The {@link EventDocumentRequirementBusinessModel} before the edition
     * @param updatedInstance The {@link EventDocumentRequirementBusinessModel} after the edition
     * @return The previous documentId which need to me removed, or null or none.
     */
    private Long setEventDocumentRequirementDocument(EventDocumentRequirementBusinessModel originalInstance, EventDocumentRequirementBusinessModel updatedInstance) {
        // Creation
        if (originalInstance == null || originalInstance.getDocumentId() == null) {
            if (updatedInstance.getDocumentId() != null)
                throw new BadRequestException("Cannot use an existing document.");

            if (updatedInstance.getNewDocumentContent().isNullOrEmpty())
                return null;

            if (updatedInstance.getDocumentMimeType().isEmpty())
                throw new BadRequestException("You must provide both document mime type and document content.");

            if (!allowedDocumentRequirementMimeType.contains(updatedInstance.getDocumentMimeType()))
                throw BadRequestDetail.INVALID_DOCUMENT_MIME_TYPE.toException();

            DocumentBusinessModel document = documentService.addDocument(new DocumentBusinessModel() {{
                setName(updatedInstance.getName());
                setType(updatedInstance.getDocumentMimeType());
                setContent(Base64.getDecoder().decode(updatedInstance.getNewDocumentContent()));
            }});

            updatedInstance.setDocumentId(document.getId());
            updatedInstance.setNewDocumentContent(null);
            updatedInstance.setDocumentMimeType(null);
            return null;
        }

        // Update or delete
        if (updatedInstance.getDocumentId() == null) {
            // Remove previous document
            Long originalDocumentId = originalInstance.getDocumentId();

            // Deletion
            if (updatedInstance.getNewDocumentContent().isNullOrEmpty())
                return originalDocumentId;

            if (updatedInstance.getDocumentMimeType().isNullOrEmpty())
                throw new BadRequestException("You must provide both document mime type and document content.");

            // Update
            if (!allowedDocumentRequirementMimeType.contains(updatedInstance.getDocumentMimeType()))
                throw BadRequestDetail.INVALID_DOCUMENT_MIME_TYPE.toException();

            DocumentBusinessModel document = documentService.addDocument(new DocumentBusinessModel() {{
                setName(updatedInstance.getName());
                setType(updatedInstance.getDocumentMimeType());
                setContent(Base64.getDecoder().decode(updatedInstance.getNewDocumentContent()));
            }});

            updatedInstance.setDocumentId(document.getId());
            updatedInstance.setNewDocumentContent(null);
            updatedInstance.setDocumentMimeType(null);

            return originalDocumentId;
        }

        return null;
    }

    @Override
    public EventBusinessModel addEvent(EventBusinessModel eventCreateRequest) {
        if (eventCreateRequest.getBeginning() != null
            && eventCreateRequest.getRegisterBeginning() != null
            && eventCreateRequest.getRegisterEnding() != null
            && (eventCreateRequest.getRegisterBeginning() > eventCreateRequest.getBeginning()
                || eventCreateRequest.getRegisterBeginning() > eventCreateRequest.getRegisterEnding()
            )
        )
            throw BadRequestDetail.INVALID_TIMELINE_EVENT_DATES.toException();

        if (eventCreateRequest.isAllowTeamName()) {
            if (eventCreateRequest.getMaximalTeamSize() < 1 || eventCreateRequest.getMinimalTeamSize() < 1
                    || eventCreateRequest.getMinimalTeamSize() > eventCreateRequest.getMaximalTeamSize())
                throw BadRequestDetail.INVALID_EVENT_SIZE_TEAM.toException();
        }

        eventCreateRequest.setCode(this.generateValidEventCode());
        eventCreateRequest.setOwnerId(this.userService.getCurrentUser().getId());
        
        try (var trx = databaseConnection.beginTransaction()) {
            EventDataModel eventDataModel = this.repository.createEvent(eventCreateRequest.toDataModel());
            EventBusinessModel event = EventBusinessModel.fromDataModel(eventDataModel);

            for (EventDocumentRequirementBusinessModel documentRequirement : eventCreateRequest.getDocumentRequirements()) {
                setEventDocumentRequirementDocument(null, documentRequirement);
                EventDocumentRequirementDataModel dataModel = documentRequirement.toDataModel();
                dataModel.setEventId(event.getId());
                dataModel = repository.createEventDocumentRequirement(dataModel);
                documentRequirement.setId(dataModel.getId());
                event.getDocumentRequirements().add(documentRequirement);
            }

            trx.commit();
            return event;
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setEvent(EventBusinessModel eventUpdateRequest) {
        if (eventUpdateRequest.getBeginning() != null
                && eventUpdateRequest.getRegisterBeginning() != null
                && eventUpdateRequest.getRegisterEnding() != null
                && (eventUpdateRequest.getRegisterBeginning() > eventUpdateRequest.getBeginning()
                || eventUpdateRequest.getRegisterBeginning() > eventUpdateRequest.getRegisterEnding())
        )
            throw BadRequestDetail.INVALID_TIMELINE_EVENT_DATES.toException();

        if (eventUpdateRequest.isAllowTeamName()) {
            if (eventUpdateRequest.getMaximalTeamSize() < 1 || eventUpdateRequest.getMinimalTeamSize() < 1
                    || eventUpdateRequest.getMinimalTeamSize() > eventUpdateRequest.getMaximalTeamSize())
                throw BadRequestDetail.INVALID_EVENT_SIZE_TEAM.toException();
        }

        EventBusinessModel existingEvent = getEventById(eventUpdateRequest.getId());
        if (existingEvent == null)
            throw BadRequestDetail.UNKNOWN_EVENT.toException();

        eventUpdateRequest.setOwnerId(existingEvent.getOwnerId());
        eventUpdateRequest.setCode(existingEvent.getCode());

        List<EventUserDocumentDataModel> userDocuments = repository.readEventUserDocuments(existingEvent.getId());
        try (var trx = databaseConnection.beginTransaction()) {
            this.repository.updateEvent(eventUpdateRequest.toDataModel());

            // To delete
            existingEvent.getDocumentRequirements().stream()
                    .filter(edr -> !eventUpdateRequest.getDocumentRequirements().contains(edr1 -> edr.getId().equals(edr1.getId())))
                    .forEach(edr -> {
                        observables.forEach(observable -> observable.notifyEventDocumentRequirementBeforeDeletion(edr));
                        if (edr.isIndividual()) {
                            repository.deleteEventUsersDocuments(edr.getId());
                            documentService.removeDocuments(userDocuments.filter(doc -> Objects.equals(doc.getEventDocumentRequirementId(), edr.getId())).map(EventUserDocumentDataModel::getDocumentId));
                        }
                        repository.deleteEventDocumentRequirement(edr.getId());
                        if (edr.getDocumentId() != null)
                            documentService.removeDocument(edr.getDocumentId());
                    });

            // To Create
            eventUpdateRequest.getDocumentRequirements().stream()
                    .filter(edr -> edr.getId() == null || !existingEvent.getDocumentRequirements().contains(edr1 -> edr.getId().equals(edr1.getId())))
                    .forEach(edr -> {
                        setEventDocumentRequirementDocument(null, edr);
                        EventDocumentRequirementDataModel dm = edr.toDataModel();
                        dm.setEventId(existingEvent.getId());
                        dm = repository.createEventDocumentRequirement(dm);
                        edr.setId(dm.getId());
                    });

            // To Update
            eventUpdateRequest.getDocumentRequirements().stream()
                    .filter(edr -> existingEvent.getDocumentRequirements().contains(edr1 -> edr.getId().equals(edr1.getId())))
                    .forEach(edr -> {
                        Long documentIdToDelete = setEventDocumentRequirementDocument(existingEvent.getDocumentRequirements().single(edr1 -> edr.getId().equals(edr1.getId())), edr);
                        EventDocumentRequirementDataModel dm = edr.toDataModel();
                        dm.setEventId(existingEvent.getId());
                        repository.updateEventDocumentRequirement(dm);
                        if (documentIdToDelete != null)
                            documentService.removeDocument(documentIdToDelete);
                    });

            trx.commit();
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void removeEvent(Long eventID) {
        try (DatabaseTransaction trx = databaseConnection.beginTransaction()) {
            repository.deleteUsersFromEvent(eventID);

            EventBusinessModel event = getEventById(eventID);
            if (event == null)
                throw BadRequestDetail.UNKNOWN_EVENT.toException();

            List<EventUserDocumentDataModel> userDocuments = repository.readEventUserDocuments(eventID);
            event.getDocumentRequirements().forEach(edr -> {
                observables.forEach(observable -> observable.notifyEventDocumentRequirementBeforeDeletion(edr));
                if (edr.isIndividual()) {
                    repository.deleteEventUsersDocuments(edr.getId());
                    documentService.removeDocuments(userDocuments.filter(doc -> Objects.equals(doc.getEventDocumentRequirementId(), edr.getId())).map(EventUserDocumentDataModel::getDocumentId));
                }
                repository.deleteEventDocumentRequirement(edr.getId());
                if (edr.getDocumentId() != null)
                    documentService.removeDocument(edr.getDocumentId());
            });

            observables.forEach(observable -> observable.notifyEventBeforeDeletion(event));
            this.repository.deleteEvent(eventID);

            trx.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Fetch and set all the documents associated with an {@link EventBusinessModel}
     * @param event The {@link EventBusinessModel} to fill
     */
    private void fillEventDocuments(EventBusinessModel event) {
        List<Long> documentIds = event.getDocumentRequirements().map(EventDocumentRequirementBusinessModel::getDocumentId);
        List<DocumentBusinessModel> documents = documentService.getDocuments(documentIds, false);

        event.getDocumentRequirements().forEach(requirement -> {
            DocumentBusinessModel document = documents.singleOrNull(doc -> doc.getId().equals(requirement.getDocumentId()));
            if (document != null)
                requirement.setDocumentMimeType(document.getType());
        });
    }

    /**
     * Fetch and set all the documents associated with a buch of {@link EventBusinessModel}
     * @param events The {@link EventBusinessModel}s to fill
     */
    private void fillEventsDocuments(List<EventBusinessModel> events) {
        List<Long> documentIds = events.map(e -> e.getDocumentRequirements().map(EventDocumentRequirementBusinessModel::getDocumentId)).stream().flatMap(List::stream).toList();
        List<DocumentBusinessModel> documents = documentService.getDocuments(documentIds, false);

        events.forEach(event -> event.getDocumentRequirements().forEach(requirement -> {
            DocumentBusinessModel document = documents.singleOrNull(doc -> doc.getId().equals(requirement.getDocumentId()));
            if (document != null)
                requirement.setDocumentMimeType(document.getType());
        }));
    }

    @Override
    public List<EventBusinessModel> getEventsToModerator() {
        Long id = this.userService.getCurrentUser().getId();
        List<EventBusinessModel> events = this.repository.readEventsByOwnerId(id).map(EventBusinessModel::fromDataModel);
        fillEventsDocuments(events);
        return events;
    }

    @Override
    public List<EventBusinessModel> getEventsJoinedByCurrentUser() {
        Long userId = this.userService.getCurrentUser().getId();
        List<EventBusinessModel> events = this.repository.readEventsJoinedByUserId(userId).map(EventBusinessModel::fromDataModel);
        fillEventsDocuments(events);
        return events;
    }

    @Override
    public List<EventIdDataModel> getEventsIdJoined(Long userId) {
        return this.repository.readIdEventsJoinedByUserId(userId);
    }

    @Override
    public EventBusinessModel getEventById(Long eventId) {
        EventDataModel dataModel = this.repository.readEventById(eventId);
        if (dataModel == null)
            return null;
        EventBusinessModel event = EventBusinessModel.fromDataModel(dataModel);
        fillEventDocuments(event);
        return event;
    }

    @Override
    public EventBusinessModel getEventByCode(String eventCode) {
        EventDataModel eventDataModel = this.repository.readEventbyCode(eventCode);
        if (eventDataModel == null)
            return null;
        EventBusinessModel event = EventBusinessModel.fromDataModel(eventDataModel);
        fillEventDocuments(event);
        return event;
    }

    @Override
    public void addUserToEvent(Long eventId, Long userId) {
        if (!getEventsIdJoined(userId).contains(event -> event.getId().equals(eventId)))
            this.repository.updateUserJoinedEvent(eventId, userId);
    }

    @Override
    public void addCurrentUserToEvent(Long eventId) {
        Long userId = this.userService.getCurrentUser().getId();

        List<EventIdDataModel> eventIdDataModels = this.getEventsIdJoined(userId);

        for (EventIdDataModel eventIdDataModel : eventIdDataModels) {
            if(eventIdDataModel.getId().equals(eventId)) {
                throw BadRequestDetail.EVENT_ALREADY_JOINED.toException();
            }
        }

        this.addUserToEvent(eventId, userId);
    }

    @Override
    public void removeUserToEvent(Long eventId, Long userId) {
        this.repository.deleteUserToEvent(eventId, userId);
    }

    @Override
    public void removeCurrentUserToEvent(Long eventId) {
        observables.forEach(observable -> observable.notifyBeforeCurrentUserLeaveEvent(eventId));
        Long userId = this.userService.getCurrentUser().getId();
        this.removeUserToEvent(eventId, userId);
    }

    //#region DocumentRequirement
    @Override
    public EventDocumentRequirementBusinessModel getEventDocumentRequirement(Long eventDocumentRequirementId, boolean fetchContent) {
        EventDocumentRequirementDataModel dataModel = repository.readEventDocumentRequirement(eventDocumentRequirementId);
        if (dataModel == null)
            return null;

        EventDocumentRequirementBusinessModel businessModel = EventDocumentRequirementBusinessModel.fromDataModel(dataModel);
        if (fetchContent && businessModel.getDocumentId() != null) {
            DocumentBusinessModel document = documentService.getDocument(businessModel.getDocumentId(), false);
            if (document != null)
                businessModel.setDocumentMimeType(document.getType());
        }

        return businessModel;
    }

    @Override
    public List<EventUserDocumentBusinessModel> getUserEventDocumentsFromUserIds(List<Long> userIds) {
        if (userIds.isEmpty())
            return Collections.emptyList();

        List<EventUserDocumentDataModel> eventUserDocuments = repository.readEventUserDocumentsFromUserIds(userIds);
        List<DocumentBusinessModel> documents = documentService.getDocuments(eventUserDocuments.map(EventUserDocumentDataModel::getDocumentId), false);

        return eventUserDocuments.map(eventUserDocument -> {
            EventUserDocumentBusinessModel bm = EventUserDocumentBusinessModel.fromDataModel(eventUserDocument);
            DocumentBusinessModel document = documents.singleOrNull(doc -> doc.getId().equals(eventUserDocument.getDocumentId()));
            if (document != null)
                bm.setDocumentMimeType(document.getType());

            return bm;
        });
    }

    @Override
    public List<EventUserDocumentBusinessModel> getCurrentUserEventDocuments(Long eventId) {
        UserBusinessModel currentUser = userService.getCurrentUser();
        if (currentUser == null)
            throw new UnauthorizedException();

        List<EventUserDocumentDataModel> eventUserDocuments = repository.readEventUserDocuments(eventId, currentUser.getId());
        List<DocumentBusinessModel> documents = documentService.getDocuments(eventUserDocuments.map(EventUserDocumentDataModel::getDocumentId), false);

        return eventUserDocuments.map(eventUserDocument -> {
            EventUserDocumentBusinessModel bm = EventUserDocumentBusinessModel.fromDataModel(eventUserDocument);
            DocumentBusinessModel document = documents.singleOrNull(doc -> doc.getId().equals(eventUserDocument.getDocumentId()));
            if (document != null)
                bm.setDocumentMimeType(document.getType());

            return bm;
        });
    }

    @Override
    public EventUserDocumentBusinessModel setCurrentUserEventDocument(EventUserDocumentBusinessModel eventUserDocument) {
        UserBusinessModel currentUser = userService.getCurrentUser();
        if (currentUser == null)
            throw new UnauthorizedException();

        if (eventUserDocument.getEventDocumentRequirementId() == null)
            throw BadRequestDetail.UNKNOWN_EVENT_DOCUMENT_REQUIREMENT.toException();

        EventDocumentRequirementDataModel documentRequirement = repository.readEventDocumentRequirement(eventUserDocument.getEventDocumentRequirementId());
        if (documentRequirement == null)
            throw BadRequestDetail.UNKNOWN_EVENT_DOCUMENT_REQUIREMENT.toException();

        List<EventUserDocumentDataModel> eventUserDocuments = repository.readEventUserDocuments(documentRequirement.getEventId(), currentUser.getId());
        EventUserDocumentDataModel originalUserDocument = eventUserDocuments.singleOrNull(eud -> eud.getEventDocumentRequirementId().equals(eventUserDocument.getEventDocumentRequirementId()));

        boolean isDeletion = eventUserDocument.getNewDocumentContent().isNullOrEmpty();

        if (!isDeletion) {
            if (eventUserDocument.getDocumentMimeType().isNullOrEmpty())
                throw new BadRequestException("You must provide a document mime type");

            if (!allowedDocumentRequirementMimeType.contains(eventUserDocument.getDocumentMimeType()))
                throw BadRequestDetail.INVALID_DOCUMENT_MIME_TYPE.toException();
        }

        if (originalUserDocument != null) {
            repository.deleteEventUserDocument(originalUserDocument);
            documentService.removeDocument(originalUserDocument.getDocumentId());
        }

        if (isDeletion)
            return null;

        DocumentBusinessModel document = documentService.addDocument(new DocumentBusinessModel() {{
            setName(documentRequirement.getName());
            setType(eventUserDocument.getDocumentMimeType());
            setContent(Base64.getDecoder().decode(eventUserDocument.getNewDocumentContent()));
        }});

        eventUserDocument.setDocumentId(document.getId());
        eventUserDocument.setNewDocumentContent(null);

        EventUserDocumentDataModel dm = eventUserDocument.toDataModel();
        dm.setUserId(currentUser.getId());
        repository.createEventUserDocument(dm);

        return eventUserDocument;
    }
    //#endregion

    //#region EventEmail
    @Override
    public EventEmailBusinessModel addEventEmail(Long eventId, EventEmailAddRequestBusinessModel email) {
        EventBusinessModel event = getEventById(eventId);
        if (event == null)
            throw BadRequestDetail.UNKNOWN_EVENT.toException();

        UserBusinessModel currentUser = userService.getCurrentUser();
        if (!currentUser.havePermission(UserPermissionBusinessModel.ADMINISTRATOR)
                && (currentUser.havePermission(UserPermissionBusinessModel.MODERATOR) && !Objects.equals(event.getOwnerId(), currentUser.getId())))
            throw new ForbiddenException();

        EmailAddressBusinessModel fromAddress = null;
        if (email.getShowMyAddressAsFrom() != null && email.getShowMyAddressAsFrom())
            fromAddress = currentUser.toEmailAddress();

        EmailAddressBusinessModel replyToAddress = null;
        if (email.getShowMyAddressAsReplyTo() != null && email.getShowMyAddressAsReplyTo())
            replyToAddress = currentUser.toEmailAddress();

        long now = System.currentTimeMillis();

        List<CompletableFuture<Void>> futures = new ArrayList<>();
        List<UserBusinessModel> recipients = userService.getUsers(repository.readEventUsers(eventId).map(EventUserDataModel::getUserId));
        for (UserBusinessModel recipient : recipients) {
            EmailBusinessModel emailToSend = new EmailBusinessModel() {{
                setTo(Collections.singletonList(recipient.toEmailAddress()));
                setSubject(email.getSubject());
                setContent(email.getContent());
                setAttachments(email.getAttachments());
            }};
            emailToSend.setFrom(fromAddress);
            if (replyToAddress != null)
                emailToSend.setReplyTo(Collections.singletonList(replyToAddress));

            futures.add(emailService.sendEmailAsync(emailToSend, new HashMap<>() {{
                put(EmailPlaceholderBusinessModel.FIRSTNAME, recipient.getFirstName());
                put(EmailPlaceholderBusinessModel.LASTNAME, recipient.getLastName());
                put(EmailPlaceholderBusinessModel.EVENT_NAME, event.getName());
            }}));
        }

        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();

        EventEmailDataModel historizedEmail = repository.createEventEmail(new EventEmailDataModel() {{
            setEventId(eventId);
            setAttachmentAmount(email.getAttachments().size());
            setContent(email.getContent());
            setCreatedAt(new Timestamp(now));
            setSubject(email.getSubject());
        }});

        return EventEmailBusinessModel.fromDataModel(historizedEmail);
    }

    @Override
    public List<EventEmailBusinessModel> getEventEmails(Long eventId, EventEmailFiltersBusinessModel filters) {
        Timestamp minDate = null;
        if (filters.getMinDate() != null)
            minDate = new Timestamp(filters.getMinDate().getTime());

        Timestamp maxDate = null;
        if (filters.getMinDate() != null)
            maxDate = new Timestamp(filters.getMaxDate().getTime());

        return repository.readEventEmails(eventId, minDate, maxDate).map(EventEmailBusinessModel::fromDataModel);
    }
    //#endregion
}
