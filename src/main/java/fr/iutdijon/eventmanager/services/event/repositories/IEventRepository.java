package fr.iutdijon.eventmanager.services.event.repositories;

import fr.iutdijon.eventmanager.services.event.models.data.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public interface IEventRepository {

    /**
     * Insert an event in database
     * @param event EventDataModel - Event to create
     * @return EventDataModel - Event created (with id set)
     */
    EventDataModel createEvent(EventDataModel event);

    /**
     * Return the events created by the user in parameter.
     * @param userId Long
     * @return EventDataModel[]
     */
    List<EventDataModel> readEventsByOwnerId(Long userId);

    /**
     * Return the events joined by the user in parameter.
     * @param userId Long
     * @return EventDataModel[]
     */
    List<EventDataModel> readEventsJoinedByUserId(Long userId);

    /**
     * Read the events joined by the user in parameter.
     * @param userId Long
     * @return EventDataModel[]
     */
    List<EventIdDataModel> readIdEventsJoinedByUserId(Long userId);

    /**
     * Read all the users id who have joined an event
     * @param eventId The event id
     * @return List of both event id and user id representing each user who have joined the event
     */
    List<EventUserDataModel> readEventUsers(Long eventId);

    /**
     * Return the event which have the id in parameter.
     * Throw an error if more than 1 event found, and null if no one is found.
     * @param eventId Long
     * @return EventDataModel
     */
    EventDataModel readEventById(Long eventId);


    /**
     * Return the event which have same code in parameters.
     * Throw an error if more than 1 event found, and null if no one is found.
     * @param codeEvent String
     * @return EventDataModel
     */
    EventDataModel readEventbyCode(String codeEvent);


    /**
     * To update an event already created. Update all the fields with parameter event.
     * @param event EventDataModel - Event to update / to push
     */
    void updateEvent(EventDataModel event);


    /**
     * To delete an event in the database by its id.
     * @param eventId Long - Event id to delete
     */
    void deleteEvent(Long eventId);


    /**
     * To link a user to an event which has joined.
     * @param eventId Long - eventId the user joined
     * @param userId Long - userId of the user who join
     */
    void updateUserJoinedEvent(Long eventId, Long userId);


    /**
     * To remove a user to an event, in parameters.
     * @param eventId Long - Event's id
     * @param userId Long - User's id
     */
    void deleteUserToEvent(Long eventId, Long userId);

    /**
     * Remove all the users from an event
     * @param eventId The event's id to remove the users from
     */
    void deleteUsersFromEvent(Long eventId);

    //#region EventDocumentRequirement
    EventDocumentRequirementDataModel createEventDocumentRequirement(EventDocumentRequirementDataModel eventDocumentRequirement);
    EventDocumentRequirementDataModel readEventDocumentRequirement(Long eventDocumentRequirementId);
    void updateEventDocumentRequirement(EventDocumentRequirementDataModel eventDocumentRequirement);
    void deleteEventDocumentRequirement(Long eventDocumentRequirementId);
    //#endregion

    //#region UserEventDocument
    void createEventUserDocument(EventUserDocumentDataModel eventUserDocument);
    List<EventUserDocumentDataModel> readEventUserDocuments(Long eventId);
    List<EventUserDocumentDataModel> readEventUserDocumentsFromUserIds(List<Long> userIds);
    List<EventUserDocumentDataModel> readEventUserDocuments(Long eventId, Long userId);
    void deleteEventUsersDocuments(Long eventDocumentRequirementId);
    void deleteEventUserDocument(EventUserDocumentDataModel eventUserDocument);
    //#endregion

    //#region EventEmail
    EventEmailDataModel createEventEmail(EventEmailDataModel eventEmail);
    List<EventEmailDataModel> readEventEmails(Long eventId, Timestamp minDate, Timestamp maxDate);
    //#endregion
}
