package fr.iutdijon.eventmanager.services.event.repositories;

import fr.iutdijon.eventmanager.infrastructure.RequestDatabaseConnection;
import fr.iutdijon.eventmanager.services.event.models.data.*;
import net.redheademile.jdapper.JDapper;
import org.intellij.lang.annotations.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.RequestScope;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Repository
@RequestScope
public class EventRepository implements IEventRepository {

    private final RequestDatabaseConnection databaseConnection;

    @Autowired
    public EventRepository(RequestDatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    private RowMapper<Void> getEventRowMapper(List<EventDataModel> output) {
        return JDapper.getMapper(
                EventDataModel.class, EventDocumentRequirementDataModel.class,
                (event, eventDocumentRequirement) -> {
                    EventDataModel eventToWorkWith = output.singleOrNull(eventDataModel -> eventDataModel.getId().equals(event.getId()));
                    if (eventToWorkWith == null) {
                        output.add(event);
                        eventToWorkWith = event;
                    }

                    if (eventDocumentRequirement != null
                            && eventDocumentRequirement.getId() != null
                            && !eventToWorkWith.getDocumentRequirements().contains(doc -> doc.getId().equals(eventDocumentRequirement.getId())))
                        eventToWorkWith.getDocumentRequirements().add(eventDocumentRequirement);

                    return null;
                },
                "eventid", "eventdocumentrequirementid"
        );
    }

    @Override
    public EventDataModel createEvent(EventDataModel event) {
        Long eventId = databaseConnection.insertAndGetGeneratedLongKey(
                "INSERT INTO event (user_userid, allowteamname, beginning, code, description, maximalteamsize, " +
                        "minimalteamsize, name, registerbeginning, registerending) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                event.getOwnerId(), event.getAllowTeamName(), event.getBeginning(),
                event.getCode(), event.getDescription(), event.getMaximalTeamSize(),
                event.getMinimalTeamSize(), event.getName(), event.getRegisterBeginning(),
                event.getRegisterEnding()
        );
        event.setId(eventId);

        return event;
    }

    @Override
    public List<EventDataModel> readEventsByOwnerId(Long userId) {
        List<EventDataModel> events = new ArrayList<>();
        databaseConnection.query(
                "SELECT event.*, eventdocumentrequirement.*" +
                        "FROM event " +
                        "LEFT JOIN eventdocumentrequirement ON eventdocumentrequirement.event_eventid = event.eventid " +
                        "WHERE user_userid = ?",
                getEventRowMapper(events),
                userId
        );

        return events;
    }

    @Override
    public List<EventDataModel> readEventsJoinedByUserId(Long userId) {
        List<EventDataModel> events = new ArrayList<>();
        databaseConnection.query(
                "SELECT event.*, eventdocumentrequirement.* FROM event " +
                        "JOIN user_event ON user_event.event_eventid = eventid " +
                        "LEFT JOIN eventdocumentrequirement ON eventdocumentrequirement.event_eventid = event.eventid " +
                        "WHERE user_event.user_userid = ?",
                getEventRowMapper(events),
                userId
        );

        return events;
    }
    @Override
    public List<EventIdDataModel> readIdEventsJoinedByUserId(Long userId) {
        return databaseConnection.query(
                "SELECT user_event.event_eventid FROM user_event WHERE user_event.user_userid = ?",
                JDapper.getMapper(EventIdDataModel.class),
                userId
        );
    }

    @Override
    public List<EventUserDataModel> readEventUsers(Long eventId) {
        return databaseConnection.query(
                "SELECT * FROM user_event WHERE event_eventid = ?",
                JDapper.getMapper(EventUserDataModel.class),
                eventId
        );
    }

    @Override
    public EventDataModel readEventById(Long eventId) {
        List<EventDataModel> events = new ArrayList<>();
        databaseConnection.query(
                "SELECT event.*, eventdocumentrequirement.* " +
                        "FROM event " +
                        "LEFT JOIN eventdocumentrequirement ON eventdocumentrequirement.event_eventid = event.eventid " +
                        "WHERE eventid = ?",
                getEventRowMapper(events),
                eventId
        );

        return events.singleOrNull();
    }

    @Override
    public EventDataModel readEventbyCode(String codeEvent) {
        List<EventDataModel> events = new ArrayList<>();
        databaseConnection.query(
                "SELECT event.*, eventdocumentrequirement.* " +
                        "FROM event " +
                        "LEFT JOIN eventdocumentrequirement ON eventdocumentrequirement.event_eventid = event.eventid " +
                        "WHERE code = ?",
                getEventRowMapper(events),
                codeEvent
        );

        return events.singleOrNull();
    }

    @Override
    public void updateEvent(EventDataModel event) {
        databaseConnection.update(
                "UPDATE event SET allowteamname = ?, beginning = ?, description = ?, maximalteamsize = ?, " +
                        "minimalteamsize = ?, name = ?, registerbeginning = ?, registerending = ? WHERE eventid = ?",
                event.getAllowTeamName(), event.getBeginning(), event.getDescription(),
                event.getMaximalTeamSize(), event.getMinimalTeamSize(), event.getName(),
                event.getRegisterBeginning(), event.getRegisterEnding(), event.getId()
        );
    }

    @Override
    public void deleteEvent(Long eventId) {
        databaseConnection.update(
                "DELETE FROM event WHERE eventid = ?",
                eventId
        );
    }

    @Override
    public void updateUserJoinedEvent(Long eventId, Long userId) {
        databaseConnection.update("INSERT INTO user_event (user_userid, event_eventid) VALUES (?, ?)",
                userId, eventId
        );
    }

    @Override
    public void deleteUserToEvent(Long eventId, Long userId) {
        databaseConnection.update("DELETE FROM user_event WHERE event_eventid = ? AND user_userid = ?",
                eventId, userId
        );
    }

    @Override
    public void deleteUsersFromEvent(Long eventId) {
        databaseConnection.update("DELETE FROM user_event WHERE event_eventid = ?", eventId);
    }

    //#region EventDocumentRequirement
    @Override
    public EventDocumentRequirementDataModel createEventDocumentRequirement(EventDocumentRequirementDataModel eventDocumentRequirement) {
        Long newId = databaseConnection.insertAndGetGeneratedLongKey(
                "INSERT INTO eventdocumentrequirement(document_documentid, event_eventid, individualdocument, name, optionnal) VALUES(?, ?, ?, ?, ?)",
                eventDocumentRequirement.getDocumentId(), eventDocumentRequirement.getEventId(), eventDocumentRequirement.getIndividual(), eventDocumentRequirement.getName(), eventDocumentRequirement.getOptional()
        );
        eventDocumentRequirement.setId(newId);
        return eventDocumentRequirement;
    }

    @Override
    public EventDocumentRequirementDataModel readEventDocumentRequirement(Long eventDocumentRequirementId) {
        return databaseConnection.query(
                "SELECT * FROM eventdocumentrequirement WHERE eventdocumentrequirementid = ?",
                JDapper.getMapper(EventDocumentRequirementDataModel.class),
                eventDocumentRequirementId
        ).singleOrNull();
    }

    @Override
    public void updateEventDocumentRequirement(EventDocumentRequirementDataModel eventDocumentRequirement) {
        databaseConnection.update(
                "UPDATE eventdocumentrequirement SET document_documentid = ?, event_eventid = ?, individualdocument = ?, name = ?, optionnal = ? WHERE eventdocumentrequirementid = ?",
                eventDocumentRequirement.getDocumentId(), eventDocumentRequirement.getEventId(), eventDocumentRequirement.getIndividual(), eventDocumentRequirement.getName(), eventDocumentRequirement.getOptional(), eventDocumentRequirement.getId()
        );
    }

    @Override
    public void deleteEventDocumentRequirement(Long eventDocumentRequirementId) {
        databaseConnection.update("DELETE FROM eventdocumentrequirement WHERE eventdocumentrequirementid = ?", eventDocumentRequirementId);
    }
    //#endregion

    //#region UserEventDocument
    @Override
    public void createEventUserDocument(EventUserDocumentDataModel eventUserDocument) {
        databaseConnection.update(
                "INSERT INTO user_document(eventdocumentrequirement_eventdocumentrequirementid, user_userid, document_documentid) VALUES (?, ?, ?)",
                eventUserDocument.getEventDocumentRequirementId(), eventUserDocument.getUserId(), eventUserDocument.getDocumentId()
        );
    }

    @Override
    public List<EventUserDocumentDataModel> readEventUserDocuments(Long eventId) {
        return databaseConnection.query(
                "SELECT user_document.* FROM user_document JOIN eventdocumentrequirement ON eventdocumentrequirement.eventdocumentrequirementid = user_document.eventdocumentrequirement_eventdocumentrequirementid AND event_eventid = ?",
                JDapper.getMapper(EventUserDocumentDataModel.class),
                eventId
        );
    }

    @Override
    public List<EventUserDocumentDataModel> readEventUserDocumentsFromUserIds(List<Long> userIds) {
        if (userIds.isEmpty())
            return Collections.emptyList();

        return databaseConnection.query(
                "SELECT user_document.* FROM user_document JOIN eventdocumentrequirement ON eventdocumentrequirement.user_userid IN (" + String.join(", ", userIds.map(Object::toString)) + ")",
                JDapper.getMapper(EventUserDocumentDataModel.class)
        );
    }

    @Override
    public List<EventUserDocumentDataModel> readEventUserDocuments(Long eventId, Long userId) {
        return databaseConnection.query(
                "SELECT user_document.* FROM user_document JOIN eventdocumentrequirement ON eventdocumentrequirement.eventdocumentrequirementid = user_document.eventdocumentrequirement_eventdocumentrequirementid AND event_eventid = ? WHERE user_userid = ?",
                JDapper.getMapper(EventUserDocumentDataModel.class),
                eventId, userId
        );
    }

    @Override
    public void deleteEventUsersDocuments(Long eventDocumentRequirementId) {
        databaseConnection.update(
                "DELETE user_document FROM user_document WHERE eventdocumentrequirement_eventdocumentrequirementid = ?",
                eventDocumentRequirementId
        );
    }

    @Override
    public void deleteEventUserDocument(EventUserDocumentDataModel eventUserDocument) {
        databaseConnection.update(
                "DELETE FROM user_document WHERE eventdocumentrequirement_eventdocumentrequirementid = ? AND user_userid = ? AND document_documentid = ?",
                eventUserDocument.getEventDocumentRequirementId(), eventUserDocument.getUserId(), eventUserDocument.getDocumentId()
        );
    }
    //#endregion

    //#region EventEmail
    @Override
    public EventEmailDataModel createEventEmail(EventEmailDataModel eventEmail) {
        Long newId = databaseConnection.insertAndGetGeneratedLongKey(
                "INSERT INTO eventemailhistory(event_eventid, attachmentamount, content, createdat, subject) VALUES (?, ?, ?, ?, ?)",
                eventEmail.getEventId(), eventEmail.getAttachmentAmount(), eventEmail.getContent(), eventEmail.getCreatedAt(), eventEmail.getSubject()
        );

        eventEmail.setId(newId);
        return eventEmail;
    }

    @Override
    public List<EventEmailDataModel> readEventEmails(Long eventId, Timestamp minDate, Timestamp maxDate) {
        List<String> whereClauses = new ArrayList<>();
        List<Object> params = new ArrayList<>();

        whereClauses.add("event_eventid = ?");
        params.add(eventId);

        if (minDate != null) {
            whereClauses.add("createdat >= ?");
            params.add(minDate);
        }

        if (maxDate != null) {
            whereClauses.add("createdat <= ?");
            params.add(maxDate);
        }

        return databaseConnection.query(
                "SELECT * FROM eventemailhistory WHERE " + String.join(" AND ", whereClauses) + " ORDER BY createdAt ASC",
                JDapper.getMapper(EventEmailDataModel.class),
                params.toArray()
        );
    }
    //#endregion
}
