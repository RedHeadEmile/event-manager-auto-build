package fr.iutdijon.eventmanager.services.event.models.business;

import fr.iutdijon.eventmanager.services.event.models.data.EventDataModel;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class EventBusinessModel {

    private Long id;
    private Long ownerId;
    private boolean allowTeamName;
    private Date beginning;
    private String code;
    private String description;
    private int maximalTeamSize;
    private int minimalTeamSize;
    private String name;
    private Date registerBeginning;
    private Date registerEnding;

    private List<EventDocumentRequirementBusinessModel> documentRequirements = new ArrayList<>();

    public EventDataModel toDataModel() {
        return new EventDataModel() {{
            setId(id);
            setAllowTeamName(allowTeamName);
            setOwnerId(ownerId);
            setBeginning(beginning != null ? new java.sql.Date(beginning.getTime()) : null);
            setCode(code);
            setDescription(description);
            setMaximalTeamSize(allowTeamName ? maximalTeamSize : 0);
            setMinimalTeamSize(allowTeamName ? minimalTeamSize : 0);
            setName(name);
            setRegisterBeginning(registerBeginning != null ? new java.sql.Date(registerBeginning.getTime()) : null);
            setRegisterEnding(registerEnding != null ? new java.sql.Date(registerEnding.getTime()) : null);
        }};
    }

    public static EventBusinessModel fromDataModel(EventDataModel dataModel) {
        return new EventBusinessModel() {{
            setId(dataModel.getId());
            setOwnerId(dataModel.getOwnerId());
            setAllowTeamName(dataModel.getAllowTeamName());
            setBeginning(dataModel.getBeginning());
            setCode(dataModel.getCode());
            setDescription(dataModel.getDescription());
            setMaximalTeamSize(dataModel.getMaximalTeamSize());
            setMinimalTeamSize(dataModel.getMinimalTeamSize());
            setName(dataModel.getName());
            setRegisterBeginning(dataModel.getRegisterBeginning());
            setRegisterEnding(dataModel.getRegisterEnding());

            setDocumentRequirements(dataModel.getDocumentRequirements().map(EventDocumentRequirementBusinessModel::fromDataModel));
        }};
    }

}
