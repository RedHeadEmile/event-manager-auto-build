package fr.iutdijon.eventmanager.services.event.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
@Setter
public class EventEmailDataModel {
    @JDapperColumnName("eventemailhistoryid ")
    private Long id;

    @JDapperColumnName("event_eventid")
    private Long eventId;

    @JDapperColumnName("attachmentamount")
    private Integer attachmentAmount;
    @JDapperColumnName("content")
    private String content;
    @JDapperColumnName("createdat")
    private java.sql.Timestamp createdAt;
    @JDapperColumnName("subject")
    private String subject;
}
