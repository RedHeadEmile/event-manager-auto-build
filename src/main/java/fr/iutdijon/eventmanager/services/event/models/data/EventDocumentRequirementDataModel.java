package fr.iutdijon.eventmanager.services.event.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
@Setter
public class EventDocumentRequirementDataModel {
    @JDapperColumnName("eventdocumentrequirementid")
    private Long id;

    @JDapperColumnName("document_documentid")
    private Long documentId;

    @JDapperColumnName("event_eventid")
    private Long eventId;

    @JDapperColumnName("individualdocument")
    private Boolean individual;

    @JDapperColumnName("name")
    private String name;

    @JDapperColumnName("optionnal")
    private Boolean optional;
}
