package fr.iutdijon.eventmanager.services.event.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
@Setter
public class EventUserDataModel {
    @JDapperColumnName("event_eventid")
    private Long eventId;

    @JDapperColumnName("user_userid")
    private Long userId;
}
