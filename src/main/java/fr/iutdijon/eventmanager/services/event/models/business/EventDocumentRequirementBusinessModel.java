package fr.iutdijon.eventmanager.services.event.models.business;

import fr.iutdijon.eventmanager.services.event.models.data.EventDocumentRequirementDataModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventDocumentRequirementBusinessModel {
    private Long id;
    private Long documentId;
    private boolean individual;
    private String name;
    private boolean optional;

    private String documentMimeType;
    private String newDocumentContent;

    public EventDocumentRequirementDataModel toDataModel() {
        return new EventDocumentRequirementDataModel() {{
            setId(EventDocumentRequirementBusinessModel.this.getId());
            setDocumentId(EventDocumentRequirementBusinessModel.this.getDocumentId());
            setIndividual(EventDocumentRequirementBusinessModel.this.isIndividual());
            setName(EventDocumentRequirementBusinessModel.this.getName());
            setOptional(EventDocumentRequirementBusinessModel.this.isOptional());
        }};
    }

    public static EventDocumentRequirementBusinessModel fromDataModel(EventDocumentRequirementDataModel dataModel) {
        return new EventDocumentRequirementBusinessModel() {{
            setId(dataModel.getId());
            setDocumentId(dataModel.getDocumentId());
            setIndividual(dataModel.getIndividual());
            setName(dataModel.getName());
            setOptional(dataModel.getOptional());
        }};
    }
}
