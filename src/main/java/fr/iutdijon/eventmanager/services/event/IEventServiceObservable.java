package fr.iutdijon.eventmanager.services.event;

import fr.iutdijon.eventmanager.services.event.models.business.EventBusinessModel;
import fr.iutdijon.eventmanager.services.event.models.business.EventDocumentRequirementBusinessModel;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;

public interface IEventServiceObservable {
    void notifyBeforeCurrentUserLeaveEvent(long eventId);
    void notifyEventBeforeDeletion(EventBusinessModel event);
    void notifyEventDocumentRequirementBeforeDeletion(EventDocumentRequirementBusinessModel eventDocumentRequirement);
}
