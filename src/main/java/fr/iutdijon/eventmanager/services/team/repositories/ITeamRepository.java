package fr.iutdijon.eventmanager.services.team.repositories;

import fr.iutdijon.eventmanager.services.team.models.data.TeamDataModel;
import fr.iutdijon.eventmanager.services.team.models.data.TeamDocumentDataModel;
import fr.iutdijon.eventmanager.services.team.models.data.TeamEmailDataModel;
import fr.iutdijon.eventmanager.services.team.models.data.TeamInvitationDataModel;

import java.sql.Timestamp;
import java.util.List;

public interface ITeamRepository {
    TeamDataModel createTeam(TeamDataModel team);
    TeamDataModel readTeam(Long teamId);
    TeamDataModel readTeam(String teamCode);
    TeamDataModel readTeamByUserId(Long eventId, Long userId);
    List<TeamDataModel> readTeams(Long eventId);
    List<TeamDataModel> readTeams(Long userId, List<Long> eventIds);
    void updateTeam(TeamDataModel team);
    void deleteTeam(Long teamId);
    void deleteTeams(List<Long> teamIds);

    void createTeamUser(Long teamId, Long userId);
    void deleteTeamUser(Long teamId, Long userId);
    void deleteTeamsUsers(List<Long> teamIds);

    void createTeamDocument(TeamDocumentDataModel teamDocument);
    List<TeamDocumentDataModel> readTeamDocuments(Long eventDocumentRequirementId);
    void deleteTeamDocument(TeamDocumentDataModel teamDocument);
    void deleteTeamDocuments(List<TeamDocumentDataModel> teamDocuments);

    TeamInvitationDataModel createTeamInvitation(TeamInvitationDataModel teamInvitation);
    TeamInvitationDataModel readTeamInvitation(Long teamInvitationId);
    void deleteTeamInvitation(Long teamInvitationId);
    void deleteAllTeamInvitations(Long teamId);
    void deleteAllTeamsInvitations(List<Long> teamIds);

    //#region TeamEmail
    TeamEmailDataModel createTeamEmail(TeamEmailDataModel teamEmail);
    List<TeamEmailDataModel> readTeamEmails(Long teamId, Timestamp minDate, Timestamp maxDate);
    //#endregion
}
