package fr.iutdijon.eventmanager.services.team.models.business;

import fr.iutdijon.eventmanager.services.team.models.data.TeamInvitationDataModel;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamInvitationBusinessModel {
    private Long id;

    private UserBusinessModel source;
    private TeamBusinessModel team;

    private String recipient;

    public static TeamInvitationBusinessModel fromDataModel(TeamInvitationDataModel dataModel) {
        return new TeamInvitationBusinessModel() {{
            setId(dataModel.getId());
            setRecipient(dataModel.getRecipient());
        }};
    }

    public TeamInvitationDataModel toDataModel() {
        return new TeamInvitationDataModel() {{
            setId(TeamInvitationBusinessModel.this.getId());
            setUserId(getSource() != null ? getSource().getId() : null);
            setTeamId(getTeam() != null ? getTeam().getId() : null);
            setRecipient(TeamInvitationBusinessModel.this.getRecipient());
        }};
    }
}
