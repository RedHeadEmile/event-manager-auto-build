package fr.iutdijon.eventmanager.services.team.repositories;

import fr.iutdijon.eventmanager.infrastructure.RequestDatabaseConnection;
import fr.iutdijon.eventmanager.services.team.models.data.*;
import net.redheademile.jdapper.JDapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.RequestScope;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Repository
@RequestScope
public class TeamRepository implements ITeamRepository  {

    private final RequestDatabaseConnection databaseConnection;

    @Autowired
    public TeamRepository(RequestDatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    @Override
    public TeamDataModel createTeam(TeamDataModel team) {
        Long teamId = databaseConnection.insertAndGetGeneratedLongKey(
                "INSERT INTO team (event_eventid, code, name, validated) VALUES (?, ?, ?, ?)",
                team.getEventId(), team.getCode(), team.getName(), team.isValidated()
        );
        team.setId(teamId);

        return team;
    }

    /**
     * Get a {@link RowMapper} for {@link TeamDataModel}. You must join tables in this order: team, team_user, teaminvitation and team_document
     * @param teams The {@link TeamDataModel} list to store the teams in.
     * @return The generated {@link RowMapper}
     */
    private RowMapper<Void> getTeamMapper(List<TeamDataModel> teams) {
        return JDapper.getMapper(TeamDataModel.class, TeamUserDataModel.class, TeamInvitationDataModel.class, TeamDocumentDataModel.class,
                (team, teamUser, teamInvitation, teamDocument) -> {
            if (team.getId() == null)
                throw new IllegalStateException("Team id cannot be null");

            TeamDataModel teamToWorkWith = teams.singleOrNull(teamDataModel -> teamDataModel.getId().equals(team.getId()));
            if (teamToWorkWith == null) {
                teamToWorkWith = team;
                teams.add(teamToWorkWith);
            }

            if (teamUser.getUserId() != null && !teamToWorkWith.getUserIds().contains(teamUser.getUserId()))
                teamToWorkWith.getUserIds().add(teamUser.getUserId());

            if (teamInvitation.getId() != null
                    && !teamToWorkWith.getInvitations().contains(ti -> ti.getTeamId().equals(teamInvitation.getId())))
                teamToWorkWith.getInvitations().add(teamInvitation);

            if (teamDocument.getEventDocumentRequirementId() != null
                    && !teamToWorkWith.getDocuments().contains(doc -> doc.getEventDocumentRequirementId().equals(teamDocument.getEventDocumentRequirementId())))
                teamToWorkWith.getDocuments().add(teamDocument);

            return null;
        }, "teamid", "user_userid", "teaminvitationid", "eventdocumentrequirement_eventdocumentrequirementid");
    }

    @Override
    public TeamDataModel readTeam(Long teamId) {
        List<TeamDataModel> teams = new ArrayList<>();

        databaseConnection.query(
                "SELECT team.*, team_user.*, teaminvitation.*, team_document.* " +
                        "FROM team " +
                        "LEFT JOIN team_document ON team.teamid = team_document.team_teamid " +
                        "LEFT JOIN team_user ON team.teamid = team_user.team_teamid " +
                        "LEFT JOIN teaminvitation ON team.teamid = teaminvitation.team_teamid " +
                        "WHERE teamid = ?",
                getTeamMapper(teams),
                teamId
        );

        return teams.singleOrNull();
    }

    @Override
    public TeamDataModel readTeam(String teamCode) {
        List<TeamDataModel> teams = new ArrayList<>();

        databaseConnection.query(
                "SELECT team.*, team_user.*, teaminvitation.*, team_document.* " +
                        "FROM team " +
                        "LEFT JOIN team_document ON team.teamid = team_document.team_teamid " +
                        "LEFT JOIN team_user ON team.teamid = team_user.team_teamid " +
                        "LEFT JOIN teaminvitation ON team.teamid = teaminvitation.team_teamid " +
                        "WHERE team.code = ?",
                getTeamMapper(teams),
                teamCode
        );

        return teams.singleOrNull();
    }

    @Override
    public TeamDataModel readTeamByUserId(Long eventId, Long userId) {
        List<TeamDataModel> teams = new ArrayList<>();

        databaseConnection.query(
                "SELECT team.*, team_user.*, teaminvitation.*, team_document.* " +
                        "FROM team " +
                        "JOIN team_user ON team.teamid = team_user.team_teamid AND team_user.user_userid = ? " +
                        "LEFT JOIN team_document on team.teamid = team_document.team_teamid " +
                        "LEFT JOIN teaminvitation ON team.teamid = teaminvitation.team_teamid " +
                        "WHERE team.event_eventId = ?",
                getTeamMapper(teams),
                userId, eventId
        );

        return teams.singleOrNull();
    }

    @Override
    public List<TeamDataModel> readTeams(Long eventId) {
        List<TeamDataModel> teams = new ArrayList<>();

        databaseConnection.query(
                "SELECT team.*, team_user.*, teaminvitation.*, team_document.* " +
                        "FROM team " +
                        "LEFT JOIN team_document ON team.teamid = team_document.team_teamid " +
                        "LEFT JOIN team_user ON team.teamid = team_user.team_teamid " +
                        "LEFT JOIN teaminvitation ON team.teamid = teaminvitation.team_teamid " +
                        "WHERE event_eventid = ?",
                getTeamMapper(teams),
                eventId
        );

        return teams;
    }

    @Override
    public List<TeamDataModel> readTeams(Long userId, List<Long> eventIds) {
        List<TeamDataModel> teams = new ArrayList<>();

        StringBuilder eventIdsClause = new StringBuilder();
        if (eventIds != null && !eventIds.isEmpty()) {
            eventIdsClause = new StringBuilder("AND team.event_eventid IN (" + eventIds.getFirst());

            for (int i = 1; i < eventIds.size(); i++)
                eventIdsClause.append(", ").append(eventIds.get(i));

            eventIdsClause.append(") ");
        }

        databaseConnection.query(
                "SELECT team.*, team_user.*, teaminvitation.*, team_document.* " +
                        "FROM team " +
                        "LEFT JOIN teaminvitation ON team.teamid = teaminvitation.team_teamid " +
                        "LEFT JOIN team_user ON team.teamid = team_user.team_teamid " +
                        "LEFT JOIN team_document ON team.teamid = team_document.team_teamid " +
                        "WHERE EXISTS (SELECT 1 FROM team_user WHERE team_user.user_userid = ? AND team_user.team_teamid = team.teamid) " + eventIdsClause,
                getTeamMapper(teams),
                userId
        );

        return teams;
    }

    @Override
    public List<TeamDocumentDataModel> readTeamDocuments(Long eventDocumentRequirementId) {
        return databaseConnection.query(
                "SELECT * FROM team_document WHERE eventdocumentrequirement_eventdocumentrequirementid = ?",
                JDapper.getMapper(TeamDocumentDataModel.class),
                eventDocumentRequirementId);
    }

    @Override
    public void updateTeam(TeamDataModel team) {
        databaseConnection.update(
                "UPDATE team SET " +
                        "code = ?, " +
                        "name = ?, " +
                        "validated = ? " +
                        "WHERE teamid = ?",
                team.getCode(),
                team.getName(),
                team.isValidated(),
                team.getId()
        );
    }

    @Override
    public void deleteTeam(Long teamId) {
        databaseConnection.update("DELETE FROM team WHERE teamid = ?", teamId);
    }

    @Override
    public void deleteTeams(List<Long> teamIds) {
        if (teamIds.isEmpty())
            return;

        databaseConnection.update("DELETE FROM team WHERE teamid IN (" + String.join(", ", teamIds.map(teamId -> teamId.toString())) + ")");
    }

    @Override
    public void createTeamUser(Long teamId, Long userId) {
        databaseConnection.update(
                "INSERT INTO team_user (user_userid, team_teamid) VALUES (?, ?)",
                userId, teamId
        );
    }

    @Override
    public void deleteTeamUser(Long teamId, Long userId) {
        databaseConnection.update(
                "DELETE FROM team_user WHERE user_userid = ? AND team_teamid = ?",
                userId, teamId
        );
    }

    @Override
    public void deleteTeamsUsers(List<Long> teamIds) {
        if (teamIds.isEmpty())
            return;

        databaseConnection.update("DELETE FROM team_user WHERE team_teamid IN (" + String.join(", ", teamIds.map(teamId -> teamId.toString()) + ")"));
    }

    @Override
    public void createTeamDocument(TeamDocumentDataModel teamDocument) {
        databaseConnection.update(
                "INSERT INTO team_document(eventdocumentrequirement_eventdocumentrequirementid, team_teamid, document_documentid) VALUES (?, ?, ?)",
                teamDocument.getEventDocumentRequirementId(), teamDocument.getTeamId(), teamDocument.getDocumentId()
        );
    }

    @Override
    public void deleteTeamDocument(TeamDocumentDataModel teamDocument) {
        databaseConnection.update(
                "DELETE FROM team_document WHERE eventdocumentrequirement_eventdocumentrequirementid = ? AND team_teamid = ? AND document_documentid = ?",
                teamDocument.getEventDocumentRequirementId(), teamDocument.getTeamId(), teamDocument.getDocumentId()
        );
    }

    @Override
    public void deleteTeamDocuments(List<TeamDocumentDataModel> teamDocuments) {
        if (teamDocuments.isEmpty())
            return;

        List<String> clauses = new ArrayList<>();
        List<Object> params = new ArrayList<>();

        for (TeamDocumentDataModel teamDocument : teamDocuments) {
            clauses.add("eventdocumentrequirement_eventdocumentrequirementid = ? AND team_teamid = ? AND document_documentid = ?");
            params.add(teamDocument.getEventDocumentRequirementId());
            params.add(teamDocument.getTeamId());
            params.add(teamDocument.getDocumentId());
        }

        databaseConnection.update("DELETE FROM team_document WHERE " + String.join(" OR ", clauses), params.toArray());
    }

    @Override
    public TeamInvitationDataModel createTeamInvitation(TeamInvitationDataModel teamInvitation) {
        Long teamInvitationId = databaseConnection.insertAndGetGeneratedLongKey(
                "INSERT INTO teaminvitation (user_userid, team_teamid, recipient) VALUES (?, ?, ?)",
                teamInvitation.getUserId(), teamInvitation.getTeamId(), teamInvitation.getRecipient()
        );
        teamInvitation.setId(teamInvitationId);

        return teamInvitation;
    }

    @Override
    public TeamInvitationDataModel readTeamInvitation(Long teamInvitationId) {
        return databaseConnection.query(
                "SELECT * from teaminvitation WHERE teaminvitationid = ?",
                JDapper.getMapper(TeamInvitationDataModel.class),
                teamInvitationId
        ).singleOrNull();
    }

    @Override
    public void deleteTeamInvitation(Long teamInvitationId) {
        databaseConnection.update(
                "DELETE FROM teaminvitation WHERE teaminvitationid = ?",
                teamInvitationId
        );
    }

    @Override
    public void deleteAllTeamInvitations(Long teamId) {
        databaseConnection.update(
                "DELETE FROM teaminvitation WHERE team_teamid = ?",
                teamId
        );
    }

    @Override
    public void deleteAllTeamsInvitations(List<Long> teamIds) {
        if (teamIds.isEmpty())
            return;

        databaseConnection.update("DELETE FROM teaminvitation WHERE team_teamid IN (" + String.join(", ", teamIds.map(Object::toString)) + ")");
    }

    //#region TeamEmails
    @Override
    public TeamEmailDataModel createTeamEmail(TeamEmailDataModel teamEmail) {
        Long newId = databaseConnection.insertAndGetGeneratedLongKey(
                "INSERT INTO teamemailhistory(team_teamid, attachmentamount, content, createdat, subject) VALUES (?, ?, ?, ?, ?)",
                teamEmail.getTeamId(), teamEmail.getAttachmentAmount(), teamEmail.getContent(), teamEmail.getCreatedAt(), teamEmail.getSubject()
        );

        teamEmail.setId(newId);
        return teamEmail;
    }

    @Override
    public List<TeamEmailDataModel> readTeamEmails(Long teamId, Timestamp minDate, Timestamp maxDate) {
        List<String> whereClauses = new ArrayList<>();
        List<Object> params = new ArrayList<>();

        whereClauses.add("team_teamid = ?");
        params.add(teamId);

        if (minDate != null) {
            whereClauses.add("createdat >= ?");
            params.add(minDate);
        }

        if (maxDate != null) {
            whereClauses.add("createdat <= ?");
            params.add(maxDate);
        }

        return databaseConnection.query(
                "SELECT * FROM teamemailhistory WHERE " + String.join(" AND ", whereClauses) + " ORDER BY createdAt ASC",
                JDapper.getMapper(TeamEmailDataModel.class),
                params.toArray()
        );
    }
    //#endregion
}
