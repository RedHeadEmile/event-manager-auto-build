package fr.iutdijon.eventmanager.services.team.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class TeamDataModel {
    @JDapperColumnName("teamid")
    private Long id;

    @JDapperColumnName("event_eventid")
    private Long eventId;

    @JDapperColumnName("code")
    private String code;
    @JDapperColumnName("name")
    private String name;
    @JDapperColumnName("validated")
    private boolean validated;

    private List<TeamDocumentDataModel> documents = new ArrayList<>();
    private List<TeamInvitationDataModel> invitations = new ArrayList<>();
    private List<Long> userIds = new ArrayList<>();
}
