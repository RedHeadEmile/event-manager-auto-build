package fr.iutdijon.eventmanager.services.team.models.data;

import lombok.Getter;
import lombok.Setter;
import net.redheademile.jdapper.JDapperColumnName;

@Getter
@Setter
public class TeamEmailDataModel {
    @JDapperColumnName("teamemailhistoryid ")
    private Long id;

    @JDapperColumnName("team_teamid")
    private Long teamId;

    @JDapperColumnName("attachmentamount")
    private Integer attachmentAmount;
    @JDapperColumnName("content")
    private String content;
    @JDapperColumnName("createdat")
    private java.sql.Timestamp createdAt;
    @JDapperColumnName("subject")
    private String subject;
}
