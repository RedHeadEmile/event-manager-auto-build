package fr.iutdijon.eventmanager.services.team.models.business;

import fr.iutdijon.eventmanager.services.event.models.business.EventBusinessModel;
import fr.iutdijon.eventmanager.services.team.models.data.TeamDataModel;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class TeamBusinessModel {
    private Long id;
    private EventBusinessModel event;
    private String code;
    private String name;
    private boolean validated;

    private List<TeamDocumentBusinessModel> documents = new ArrayList<>();
    private List<TeamInvitationBusinessModel> invitations = new ArrayList<>();
    private List<UserBusinessModel> members = new ArrayList<>();

    public static TeamBusinessModel fromDataModel(TeamDataModel dataModel) {
        return new TeamBusinessModel() {{
            setId(dataModel.getId());
            setCode(dataModel.getCode());
            setName(dataModel.getName());
            setValidated(dataModel.isValidated());

            setDocuments(dataModel.getDocuments().map(TeamDocumentBusinessModel::fromDataModel));
            setInvitations(dataModel.getInvitations().map(TeamInvitationBusinessModel::fromDataModel));
        }};
    }

    public TeamDataModel toDataModel() {
        return new TeamDataModel() {{
            setId(TeamBusinessModel.this.getId());
            setEventId(getEvent() != null ? getEvent().getId() : null);
            setCode(TeamBusinessModel.this.getCode());
            setName(TeamBusinessModel.this.getName());
            setValidated(TeamBusinessModel.this.isValidated());
        }};
    }
}
