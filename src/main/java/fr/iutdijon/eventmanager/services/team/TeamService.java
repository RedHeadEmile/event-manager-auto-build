package fr.iutdijon.eventmanager.services.team;

import fr.iutdijon.eventmanager.EventManagerProperties;
import fr.iutdijon.eventmanager.exceptions.BadRequestDetail;
import fr.iutdijon.eventmanager.exceptions.BadRequestException;
import fr.iutdijon.eventmanager.exceptions.ForbiddenException;
import fr.iutdijon.eventmanager.exceptions.UnauthorizedException;
import fr.iutdijon.eventmanager.infrastructure.DatabaseTransaction;
import fr.iutdijon.eventmanager.infrastructure.RequestDatabaseConnection;
import fr.iutdijon.eventmanager.services.document.IDocumentService;
import fr.iutdijon.eventmanager.services.document.models.business.DocumentBusinessModel;
import fr.iutdijon.eventmanager.services.email.EmailServiceConstants;
import fr.iutdijon.eventmanager.services.email.IEmailService;
import fr.iutdijon.eventmanager.services.email.models.business.EmailAddressBusinessModel;
import fr.iutdijon.eventmanager.services.email.models.business.EmailBusinessModel;
import fr.iutdijon.eventmanager.services.email.models.business.EmailPlaceholderBusinessModel;
import fr.iutdijon.eventmanager.services.email.models.business.EmailTemplateBusinessModel;
import fr.iutdijon.eventmanager.services.event.IEventService;
import fr.iutdijon.eventmanager.services.event.models.business.EventBusinessModel;
import fr.iutdijon.eventmanager.services.event.models.business.EventDocumentRequirementBusinessModel;
import fr.iutdijon.eventmanager.services.event.models.business.EventEmailBusinessModel;
import fr.iutdijon.eventmanager.services.event.models.business.EventUserDocumentBusinessModel;
import fr.iutdijon.eventmanager.services.team.models.business.*;
import fr.iutdijon.eventmanager.services.team.models.data.TeamDataModel;
import fr.iutdijon.eventmanager.services.team.models.data.TeamDocumentDataModel;
import fr.iutdijon.eventmanager.services.team.models.data.TeamEmailDataModel;
import fr.iutdijon.eventmanager.services.team.models.data.TeamInvitationDataModel;
import fr.iutdijon.eventmanager.services.team.repositories.ITeamRepository;
import fr.iutdijon.eventmanager.services.user.IUserService;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import fr.iutdijon.eventmanager.services.user.models.business.UserPermissionBusinessModel;
import fr.iutdijon.eventmanager.utils.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static fr.iutdijon.eventmanager.extensions.java.util.List.ListExtension.flatten;

@Service
@RequestScope
public class TeamService implements ITeamService {

    private final ITeamRepository repository;
    private final RequestDatabaseConnection databaseConnection;

    private final EventManagerProperties properties;

    private final IDocumentService documentService;
    private final IEmailService emailService;
    private final IEventService eventService;
    private final IUserService userService;

    @Autowired
    public TeamService(
            ITeamRepository repository,
            RequestDatabaseConnection databaseConnection,

            EventManagerProperties properties,

            IDocumentService documentService,
            IEmailService emailService,
            IEventService eventService,
            IUserService userService
    ) {
        this.repository = repository;
        this.databaseConnection = databaseConnection;

        this.properties = properties;

        this.documentService = documentService;
        this.emailService = emailService;
        this.eventService = eventService;
        this.userService = userService;
    }

    //#region Team

    /**
     * Generate an available team code
     * @return An unused team code
     */
    private String generateValidTeamCode() {
        String teamCode;
        TeamDataModel existingTeam;
        do {
            teamCode = TokenUtils.generateRandomString(8);
            existingTeam = repository.readTeam(teamCode);
        }
        while (existingTeam != null);

        return teamCode;
    }

    @Override
    public TeamBusinessModel addTeam(TeamBusinessModel team) {
        UserBusinessModel currentUser = userService.getCurrentUser();
        if (currentUser == null)
            throw new UnauthorizedException();

        if (team.getEvent() == null)
            throw new BadRequestException("Cannot create a team without an event");

        EventBusinessModel realEvent = eventService.getEventById(team.getEvent().getId());
        if (realEvent == null)
            throw BadRequestDetail.UNKNOWN_EVENT.toException();

        if (repository.readTeamByUserId(realEvent.getId(), currentUser.getId()) != null)
            throw BadRequestDetail.USER_ALREADY_IN_A_TEAM.toException();

        team.setCode(generateValidTeamCode());
        TeamBusinessModel newTeam = TeamBusinessModel.fromDataModel(repository.createTeam(team.toDataModel()));
        newTeam.setEvent(realEvent);

        // Add current user to the team
        eventService.addUserToEvent(realEvent.getId(), currentUser.getId());
        repository.createTeamUser(newTeam.getId(), currentUser.getId());
        newTeam.getMembers().add(currentUser);

        return newTeam;
    }

    @Override
    public TeamBusinessModel getTeam(Long teamId) {
        TeamDataModel dataModel = repository.readTeam(teamId);
        if (dataModel == null)
            return null;

        TeamBusinessModel team = TeamBusinessModel.fromDataModel(dataModel);

        team.setEvent(this.eventService.getEventById(dataModel.getEventId()));
        team.setMembers(this.userService.getUsers(dataModel.getUserIds()));

        List<DocumentBusinessModel> documents = documentService.getDocuments(dataModel.getDocuments().map(TeamDocumentDataModel::getDocumentId), false);
        List<UserBusinessModel> invitationSources = this.userService.getUsers(dataModel.getInvitations().map(TeamInvitationDataModel::getUserId));

        team.getDocuments().forEach(document -> {
            DocumentBusinessModel bm = documents.singleOrNull(d -> d.getId().equals(document.getDocumentId()));
            if (bm != null)
                document.setDocumentMimeType(bm.getType());
        });

        team.setInvitations(dataModel.getInvitations().map(inv -> {
            TeamInvitationBusinessModel businessModel = TeamInvitationBusinessModel.fromDataModel(inv);
            businessModel.setSource(invitationSources.single(s -> s.getId().equals(inv.getUserId())));
            return businessModel;
        }));

        return team;
    }

    @Override
    public List<TeamBusinessModel> getTeams(Long eventId) {
        EventBusinessModel event = eventService.getEventById(eventId);
        if (event == null)
            throw BadRequestDetail.UNKNOWN_TEAM.toException();

        List<TeamDataModel> dataModels = repository.readTeams(eventId);

        List<Long> requiredUsersId = new ArrayList<>();
        List<Long> requiredDocumentIds = new ArrayList<>();
        dataModels.forEach(dataModel -> {
            requiredUsersId.addAll(dataModel.getUserIds());
            requiredUsersId.addAll(dataModel.getInvitations().map(TeamInvitationDataModel::getUserId));
            requiredDocumentIds.addAll(dataModel.getDocuments().map(TeamDocumentDataModel::getDocumentId));
        });

        List<UserBusinessModel> requiredUsers = userService.getUsers(requiredUsersId);
        List<DocumentBusinessModel> requiredDocuments = documentService.getDocuments(requiredDocumentIds, false);

        return dataModels.map(dataModel -> {
            TeamBusinessModel team = TeamBusinessModel.fromDataModel(dataModel);
            team.setEvent(event);
            team.setMembers(requiredUsers.stream().filter(user -> dataModel.getUserIds().contains(user.getId())).toList());
            team.getDocuments().forEach(document -> {
                DocumentBusinessModel bm = requiredDocuments.singleOrNull(d -> d.getId().equals(document.getDocumentId()));
                if (bm != null)
                    document.setDocumentMimeType(bm.getType());
            });
            team.setInvitations(dataModel.getInvitations().map(inv -> {
                TeamInvitationBusinessModel businessModel = TeamInvitationBusinessModel.fromDataModel(inv);
                businessModel.setSource(requiredUsers.single(s -> s.getId().equals(inv.getUserId())));
                return businessModel;
            }));

            return team;
        });
    }

    @Override
    public void setTeam(Long teamId, Consumer<TeamBusinessModel> updater) {
        TeamBusinessModel team = getTeam(teamId);
        if (team == null)
            throw BadRequestDetail.UNKNOWN_TEAM.toException();

        TeamBusinessModel original = team.toBuilder().build();

        updater.accept(team);
        team.setId(original.getId());
        team.setCode(original.getCode());
        team.setValidated(original.isValidated());

        if (!team.getEvent().isAllowTeamName())
            team.setName(null);

        repository.updateTeam(team.toDataModel());
    }

    @Override
    public void setTeamValidated(Long teamId) {
        TeamBusinessModel team = getTeam(teamId);
        if (team == null)
            throw BadRequestDetail.UNKNOWN_TEAM.toException();

        if (team.isValidated())
            return;

        if (team.getMembers().size() > team.getEvent().getMaximalTeamSize())
            throw BadRequestDetail.MISSING_TEAM_REQUIREMENT.toException();

        if (team.getMembers().size() < team.getEvent().getMinimalTeamSize())
            throw BadRequestDetail.MISSING_TEAM_REQUIREMENT.toException();

        if (team.getEvent().isAllowTeamName() && (team.getName() == null || team.getName().trim().isEmpty()))
            throw BadRequestDetail.MISSING_TEAM_REQUIREMENT.toException();

        if (team.getEvent().getRegisterEnding() != null && team.getEvent().getRegisterEnding().after(new Date()))
            throw BadRequestDetail.MISSING_TEAM_REQUIREMENT.toException();

        List<EventUserDocumentBusinessModel> allUserDocuments = eventService.getUserEventDocumentsFromUserIds(team.getMembers().map(UserBusinessModel::getId));
        for (EventDocumentRequirementBusinessModel documentRequirement : team.getEvent().getDocumentRequirements()) {
            if (documentRequirement.isOptional())
                continue;

            if (documentRequirement.isIndividual()) {
                for (UserBusinessModel member : team.getMembers())
                    if (!allUserDocuments.contains(ud -> ud.getUserId().equals(member.getId()) && ud.getEventDocumentRequirementId().equals(documentRequirement.getId())))
                        throw BadRequestDetail.MISSING_TEAM_REQUIREMENT.toException();
            }
            else {
                if (!team.getDocuments().contains(td -> td.getEventDocumentRequirementId().equals(documentRequirement.getDocumentId())))
                    throw BadRequestDetail.MISSING_TEAM_REQUIREMENT.toException();
            }
        }

        team.setValidated(true);
        repository.updateTeam(team.toDataModel());
    }

    @Override
    public void removeTeams(Long eventId) {
        List<TeamBusinessModel> teams = getTeams(eventId);
        try (DatabaseTransaction  trx = databaseConnection.beginTransaction()) {
            List<Long> teamIds = teams.map(TeamBusinessModel::getId);
            repository.deleteTeamsUsers(teamIds);
            repository.deleteTeamDocuments(flatten(teams.map(team -> team.getDocuments().map(TeamDocumentBusinessModel::toDataModel))));
            documentService.removeDocuments(flatten(teams.map(team -> team.getDocuments().map(TeamDocumentBusinessModel::getDocumentId))));
            repository.deleteAllTeamsInvitations(teamIds);
            repository.deleteTeams(teamIds);
            trx.commit();
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    //#endregion

    //#region CurrentUser
    @Override
    public TeamBusinessModel addCurrentUserToTeam(Long teamId) {
        UserBusinessModel currentUser = userService.getCurrentUser();
        if (currentUser == null)
            throw new UnauthorizedException();

        TeamBusinessModel team = getTeam(teamId);
        if (team == null)
            throw BadRequestDetail.UNKNOWN_TEAM.toException();

        if (team.getMembers().contains(user -> user.getId().equals(currentUser.getId())))
            return team;

        if (repository.readTeamByUserId(team.getEvent().getId(), currentUser.getId()) != null)
            throw BadRequestDetail.USER_ALREADY_IN_A_TEAM.toException();

        try (DatabaseTransaction trx = databaseConnection.beginTransaction()) {
            eventService.addUserToEvent(team.getEvent().getId(), currentUser.getId());
            repository.createTeamUser(teamId, currentUser.getId());
            team.getMembers().add(currentUser);

            for (TeamInvitationBusinessModel invitation : team.getInvitations())
                if (invitation.getRecipient().equals(currentUser.getEmail()))
                    repository.deleteTeamInvitation(invitation.getId());

            trx.commit();
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return team;
    }

    @Override
    public TeamBusinessModel addCurrentUserToTeam(String teamCode) {
        UserBusinessModel currentUser = userService.getCurrentUser();
        if (currentUser == null)
            throw new UnauthorizedException();

        TeamDataModel team = repository.readTeam(teamCode);
        if (team == null)
            throw BadRequestDetail.UNKNOWN_TEAM.toException();

        if (team.getUserIds().contains(currentUser.getId()))
            return getTeam(team.getId());

        if (repository.readTeamByUserId(team.getEventId(), currentUser.getId()) != null)
            throw BadRequestDetail.USER_ALREADY_IN_A_TEAM.toException();

        try (DatabaseTransaction trx = databaseConnection.beginTransaction()) {
            eventService.addUserToEvent(team.getEventId(), currentUser.getId());
            TeamBusinessModel teamBm = getTeam(team.getId());
            repository.createTeamUser(team.getId(), currentUser.getId());
            teamBm.getMembers().add(currentUser);

            for (TeamInvitationBusinessModel invitation : teamBm.getInvitations())
                if (invitation.getRecipient().equals(currentUser.getEmail()))
                    repository.deleteTeamInvitation(invitation.getId());

            trx.commit();

            return teamBm;
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<TeamLiteBusinessModel> getCurrentUserTeams(List<Long> eventIds) {
        UserBusinessModel currentUser = userService.getCurrentUser();
        if (currentUser == null)
            throw new UnauthorizedException();

        return repository.readTeams(currentUser.getId(), eventIds).map(TeamLiteBusinessModel::fromDataModel);
    }

    @Override
    public void removeCurrentUserFromTeam(Long teamId) {
        UserBusinessModel currentUser = userService.getCurrentUser();
        if (currentUser == null)
            throw new UnauthorizedException();

        TeamBusinessModel team = getTeam(teamId);
        if (team == null)
            throw BadRequestDetail.UNKNOWN_TEAM.toException();

        if (!team.getMembers().contains(user -> user.getId().equals(currentUser.getId())))
            return;

        try (DatabaseTransaction trx = databaseConnection.beginTransaction()) {
            repository.deleteTeamUser(teamId, currentUser.getId());
            if (team.getMembers().size() - 1 == 0) {
                repository.deleteTeamDocuments(team.getDocuments().map(TeamDocumentBusinessModel::toDataModel));
                documentService.removeDocuments(team.getDocuments().map(TeamDocumentBusinessModel::getDocumentId));
                repository.deleteAllTeamInvitations(teamId);
                repository.deleteTeam(teamId);
            }
            trx.commit();
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    //#endregion

    //#region TeamDocument
    private final List<String> allowedDocumentRequirementMimeType = Arrays.asList(MediaType.IMAGE_GIF_VALUE, MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.APPLICATION_PDF_VALUE);
    @Override
    public TeamDocumentBusinessModel setTeamDocument(TeamDocumentBusinessModel teamDocument) {
        UserBusinessModel currentUser = userService.getCurrentUser();
        if (currentUser == null)
            throw new UnauthorizedException();

        if (teamDocument.getEventDocumentRequirementId() == null)
            throw BadRequestDetail.UNKNOWN_EVENT_DOCUMENT_REQUIREMENT.toException();

        EventDocumentRequirementBusinessModel documentRequirement = eventService.getEventDocumentRequirement(teamDocument.getEventDocumentRequirementId(), false);
        if (documentRequirement == null)
            throw BadRequestDetail.UNKNOWN_EVENT_DOCUMENT_REQUIREMENT.toException();

        TeamBusinessModel team = getTeam(teamDocument.getTeamId());
        if (team == null)
            throw BadRequestDetail.UNKNOWN_TEAM.toException();

        if (!team.getMembers().contains(member -> member.getId().equals(currentUser.getId())))
            throw new ForbiddenException();

        TeamDocumentBusinessModel originalDocument = team.getDocuments().singleOrNull(td -> td.getEventDocumentRequirementId().equals(teamDocument.getEventDocumentRequirementId()));

        boolean isDeletion = teamDocument.getNewDocumentContent().isNullOrEmpty();

        if (!isDeletion) {
            if (teamDocument.getDocumentMimeType().isNullOrEmpty())
                throw new BadRequestException("You must provide a document mime type");

            if (!allowedDocumentRequirementMimeType.contains(teamDocument.getDocumentMimeType()))
                throw BadRequestDetail.INVALID_DOCUMENT_MIME_TYPE.toException();
        }

        if (originalDocument != null) {
            repository.deleteTeamDocument(originalDocument.toDataModel());
            documentService.removeDocument(originalDocument.getDocumentId());
        }

        if (isDeletion)
            return null;

        DocumentBusinessModel document = documentService.addDocument(new DocumentBusinessModel() {{
            setName(documentRequirement.getName());
            setType(teamDocument.getDocumentMimeType());
            setContent(Base64.getDecoder().decode(teamDocument.getNewDocumentContent()));
        }});

        teamDocument.setDocumentId(document.getId());
        teamDocument.setNewDocumentContent(null);

        repository.createTeamDocument(teamDocument.toDataModel());

        return teamDocument;
    }

    @Override
    public void removeTeamsDocument(Long eventDocumentRequirementId) {
        List<TeamDocumentDataModel> documentsToDelete = repository.readTeamDocuments(eventDocumentRequirementId);
        try (DatabaseTransaction trx = databaseConnection.beginTransaction()) {
            repository.deleteTeamDocuments(documentsToDelete);
            documentService.removeDocuments(documentsToDelete.map(TeamDocumentDataModel::getDocumentId));
            trx.commit();
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    //#endregion

    //#region TeamInvitation
    @Override
    public TeamInvitationBusinessModel addTeamInvitation(Long teamId, String recipient) {
        UserBusinessModel currentUser = userService.getCurrentUser();
        if (currentUser == null)
            throw new UnauthorizedException();

        TeamInvitationBusinessModel invitation = new TeamInvitationBusinessModel();
        invitation.setSource(currentUser);
        if (recipient == null || !recipient.matches("^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$"))
            throw BadRequestDetail.INVALID_EMAIL.toException();

        invitation.setRecipient(recipient.trim().toLowerCase());

        TeamBusinessModel team = getTeam(teamId);
        if (team == null)
            throw BadRequestDetail.UNKNOWN_TEAM.toException();

        if (!team.getMembers().contains(member -> member.getId().equals(currentUser.getId())))
            throw new ForbiddenException();

        invitation.setTeam(team);

        EmailTemplateBusinessModel template = emailService.getTemplate(null, EmailServiceConstants.EMAIL_TEMPLATE_CODE_TEAM_INVITATION);
        if (template == null)
            throw new IllegalStateException("Unable to read the email template");

        TeamInvitationDataModel createdInvitation = repository.createTeamInvitation(invitation.toDataModel());
        emailService.sendEmailAsync(new EmailBusinessModel() {{
            setTo(Collections.singletonList(new EmailAddressBusinessModel(invitation.getRecipient(), null)));
            setContent(template.getContent());
            setSubject(template.getSubject());
        }}, Collections.singletonMap(EmailPlaceholderBusinessModel.TEAM_INVITATION_LINK, properties.getUiBaseUrl() + "/join-team/" + team.getCode()));

        invitation.setId(createdInvitation.getId());

        return invitation;
    }

    @Override
    public void removeTeamInvitation(Long teamInvitationId) {
        UserBusinessModel user = userService.getCurrentUser();
        if (user == null)
            throw new UnauthorizedException();

        TeamInvitationDataModel invitation = repository.readTeamInvitation(teamInvitationId);
        if (invitation == null)
            throw new BadRequestException("Unkown team invitation");

        TeamBusinessModel team = getTeam(invitation.getTeamId());

        if (!team.getMembers().contains(member -> member.getId().equals(user.getId())))
            throw new ForbiddenException();

        repository.deleteTeamInvitation(teamInvitationId);
    }
    //#endregion

    //#region TeamEmail

    @Override
    public TeamEmailBusinessModel addTeamEmail(Long teamId, TeamEmailAddRequestBusinessModel email) {
        TeamBusinessModel team = getTeam(teamId);
        if (team == null)
            throw BadRequestDetail.UNKNOWN_TEAM.toException();

        UserBusinessModel currentUser = userService.getCurrentUser();
        if (!currentUser.havePermission(UserPermissionBusinessModel.ADMINISTRATOR)
                && (currentUser.havePermission(UserPermissionBusinessModel.MODERATOR)) && !Objects.equals(team.getEvent().getOwnerId(), currentUser.getId()))
            throw new ForbiddenException();

        EmailAddressBusinessModel fromAddress = null;
        if (email.getShowMyAddressAsFrom() != null && email.getShowMyAddressAsFrom())
            fromAddress = currentUser.toEmailAddress();

        EmailAddressBusinessModel replyToAddress = null;
        if (email.getShowMyAddressAsReplyTo() != null && email.getShowMyAddressAsReplyTo())
            replyToAddress = currentUser.toEmailAddress();

        long now = System.currentTimeMillis();

        List<CompletableFuture<Void>> futures = new ArrayList<>();
        List<UserBusinessModel> recipients = team.getMembers();
        for (UserBusinessModel recipient : recipients) {
            EmailBusinessModel emailToSend = new EmailBusinessModel() {{
                setTo(Collections.singletonList(recipient.toEmailAddress()));
                setSubject(email.getSubject());
                setContent(email.getContent());
                setAttachments(email.getAttachments());
            }};
            emailToSend.setFrom(fromAddress);
            if (replyToAddress != null)
                emailToSend.setReplyTo(Collections.singletonList(replyToAddress));

            String teamName = team.getName().isNullOrEmpty() ? "#" + team.getId() : team.getName();

            futures.add(emailService.sendEmailAsync(emailToSend, new HashMap<>() {{
                put(EmailPlaceholderBusinessModel.FIRSTNAME, recipient.getFirstName());
                put(EmailPlaceholderBusinessModel.LASTNAME, recipient.getLastName());
                put(EmailPlaceholderBusinessModel.EVENT_NAME, team.getEvent().getName());
                put(EmailPlaceholderBusinessModel.TEAM_NAME, teamName);
            }}));
        }

        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();

        TeamEmailDataModel historizedEmail = repository.createTeamEmail(new TeamEmailDataModel() {{
            setTeamId(teamId);
            setAttachmentAmount(email.getAttachments().size());
            setContent(email.getContent());
            setCreatedAt(new Timestamp(now));
            setSubject(email.getSubject());
        }});

        return TeamEmailBusinessModel.fromDataModel(historizedEmail);
    }

    @Override
    public List<TeamEmailBusinessModel> getTeamEmails(Long teamId, TeamEmailFiltersBusinessModel filters) {
        Timestamp minDate = null;
        if (filters.getMinDate() != null)
            minDate = new Timestamp(filters.getMinDate().getTime());

        Timestamp maxDate = null;
        if (filters.getMinDate() != null)
            maxDate = new Timestamp(filters.getMaxDate().getTime());

        return repository.readTeamEmails(teamId, minDate, maxDate).map(TeamEmailBusinessModel::fromDataModel);
    }
    //#endregion
}
