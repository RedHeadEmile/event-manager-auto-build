package fr.iutdijon.eventmanager.services.team.models.business;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class TeamEmailFiltersBusinessModel {
    private Date minDate;
    private Date maxDate;
}
