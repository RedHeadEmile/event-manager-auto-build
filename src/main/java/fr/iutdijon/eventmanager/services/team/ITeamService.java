package fr.iutdijon.eventmanager.services.team;

import fr.iutdijon.eventmanager.services.team.models.business.*;

import java.util.List;
import java.util.function.Consumer;

public interface ITeamService {
    //#region Team
    TeamBusinessModel addTeam(TeamBusinessModel team);
    TeamBusinessModel getTeam(Long teamId);
    List<TeamBusinessModel> getTeams(Long eventId);
    void setTeam(Long teamId, Consumer<TeamBusinessModel> updater);
    void setTeamValidated(Long teamId);
    void removeTeams(Long eventId);
    //#endregion

    //#region CurrentUser
    TeamBusinessModel addCurrentUserToTeam(Long teamId);
    TeamBusinessModel addCurrentUserToTeam(String teamCode);
    List<TeamLiteBusinessModel> getCurrentUserTeams(List<Long> eventIds);
    void removeCurrentUserFromTeam(Long teamId);
    //#endregion

    //#region TeamDocument
    TeamDocumentBusinessModel setTeamDocument(TeamDocumentBusinessModel teamDocument);
    void removeTeamsDocument(Long eventDocumentRequirementId);
    //#endregion

    //#region TeamInvitation
    TeamInvitationBusinessModel addTeamInvitation(Long teamId, String recipient);
    void removeTeamInvitation(Long invitationId);
    //#endregion

    //#region TeamEmail
    TeamEmailBusinessModel addTeamEmail(Long teamId, TeamEmailAddRequestBusinessModel email);
    List<TeamEmailBusinessModel> getTeamEmails(Long teamId, TeamEmailFiltersBusinessModel filters);
    //#endregion
}
