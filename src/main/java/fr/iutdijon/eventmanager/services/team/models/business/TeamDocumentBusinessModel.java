package fr.iutdijon.eventmanager.services.team.models.business;

import fr.iutdijon.eventmanager.services.team.models.data.TeamDocumentDataModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamDocumentBusinessModel {
    private Long eventDocumentRequirementId;
    private Long documentId;
    private Long teamId;
    private String documentMimeType;
    private String newDocumentContent;

    public TeamDocumentDataModel toDataModel() {
        return new TeamDocumentDataModel() {{
            setEventDocumentRequirementId(TeamDocumentBusinessModel.this.getEventDocumentRequirementId());
            setDocumentId(TeamDocumentBusinessModel.this.getDocumentId());
            setTeamId(TeamDocumentBusinessModel.this.getTeamId());
        }};
    }

    public static TeamDocumentBusinessModel fromDataModel(TeamDocumentDataModel dataModel) {
        return new TeamDocumentBusinessModel() {{
            setEventDocumentRequirementId(dataModel.getEventDocumentRequirementId());
            setDocumentId(dataModel.getDocumentId());
            setTeamId(dataModel.getTeamId());
        }};
    }
}
