package fr.iutdijon.eventmanager.infrastructure;

import org.intellij.lang.annotations.Language;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public abstract class AbstractDatabaseConnection {
    private final DataSource dataSource;
    private final JdbcTemplate jdbcTemplate;
    final ThreadLocal<DatabaseTransaction> currentTransaction;

    public AbstractDatabaseConnection(DataSource dataSource, JdbcTemplate jdbcTemplate) {
        this.dataSource = dataSource;
        this.jdbcTemplate = jdbcTemplate;
        this.currentTransaction = new ThreadLocal<>();
    }

    /**
     * Execute a query and return the result as a list of objects
     * @param query The query to execute
     * @param mapper The mapper to use to map the result to objects
     * @param parameters The parameters to use in the query
     * @return The list of objects
     * @param <T> The type of the objects
     */
    public <T> List<T> query(@Language("SQL") String query, RowMapper<T> mapper, Object... parameters) {
        return this.jdbcTemplate.query(query, mapper, parameters);
    }

    /**
     * Insert a row in the database and return the generated primary key
     * @param query The query to insert the row
     * @param parameters The parameters to use in the query
     * @return The generated primary key
     */
    public Long insertAndGetGeneratedLongKey(@Language("SQL") String query, Object... parameters) {
        try {
            boolean isNewConnection = currentTransaction.get() == null;
            Connection connection = isNewConnection ? dataSource.getConnection() : currentTransaction.get().connection;

            try (var statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
                for (int i = 0; i < parameters.length; i++)
                    statement.setObject(i + 1, parameters[i]);

                statement.executeUpdate();

                ResultSet generatedRs = statement.getGeneratedKeys();
                if (!generatedRs.next())
                    throw new RuntimeException("No key was generated");

                return generatedRs.getLong(1);
            } finally {
                if (isNewConnection)
                    connection.close();
            }
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Execute an update query (INSERT, UPDATE, DELETE)
     * @param query The query to execute
     * @param parameters The parameters to use in the query
     * @return The number of rows affected
     */
    public int update(@Language("SQL") String query, Object... parameters) {
        try {
            boolean isNewConnection = currentTransaction.get() == null;
            Connection connection = isNewConnection ? dataSource.getConnection() : currentTransaction.get().connection;

            try (var statement = connection.prepareStatement(query)) {
                for (int i = 0; i < parameters.length; i++)
                    statement.setObject(i + 1, parameters[i]);

                return statement.executeUpdate();
            } finally {
                if (isNewConnection)
                    connection.close();
            }
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Execute a query and return the first result as an object, the query must return only one row and one column
     * @param query The query to execute
     * @param clazz The class of the object to return
     * @param parameters The parameters to use in the query
     * @return The object
     * @param <T> The type of the object
     */
    public <T> T queryForObject(@Language("SQL") String query, Class<T> clazz, Object... parameters) {
        return this.jdbcTemplate.queryForObject(query, clazz, parameters);
    }

    /**
     * Begin a transaction (if there is no transaction already started) and return it
     * Should be used in a try-with-resources statement
     * @return The transaction
     * @throws SQLException If an error occurs while creating the transaction
     */
    public DatabaseTransaction beginTransaction() throws SQLException {
        DatabaseTransaction currentTrx = currentTransaction.get();
        if (currentTrx != null)
            return new DatabaseTransaction(currentTrx.connection, this, currentTrx);

        Connection connection = dataSource.getConnection();
        connection.setAutoCommit(false);
        currentTransaction.set(new DatabaseTransaction(connection, this, null));
        return currentTransaction.get();
    }
}
