package fr.iutdijon.eventmanager.infrastructure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import javax.sql.DataSource;

/**
 * An {@link AbstractDatabaseConnection} scoped to the current request
 */
@Component
@RequestScope
public class RequestDatabaseConnection extends AbstractDatabaseConnection {

    @Autowired
    public RequestDatabaseConnection(DataSource dataSource, JdbcTemplate jdbcTemplate) {
        super(dataSource, jdbcTemplate);
    }
}
