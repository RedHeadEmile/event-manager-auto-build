package fr.iutdijon.eventmanager.infrastructure;

import fr.iutdijon.eventmanager.services.log.ILogService;
import fr.iutdijon.eventmanager.services.log.models.business.LogBusinessModel;
import fr.iutdijon.eventmanager.services.user.IUserService;
import fr.iutdijon.eventmanager.services.user.models.business.UserBusinessModel;
import fr.iutdijon.eventmanager.utils.ResettableStreamHttpServletRequest;
import fr.iutdijon.eventmanager.utils.ResettableStreamHttpServletResponse;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class LoggerMiddleware implements Filter {
    private final ApplicationContext context;

    /**
     * Logged {@link MediaType}
     */
    private final List<String> mediaTypesToLog = Arrays.asList(
            MediaType.TEXT_PLAIN_VALUE,
            MediaType.TEXT_HTML_VALUE,
            MediaType.TEXT_XML_VALUE,
            MediaType.APPLICATION_JSON_VALUE,
            MediaType.APPLICATION_XML_VALUE
    );

    public LoggerMiddleware(ApplicationContext context) {
        this.context = context;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        long startedAt = System.currentTimeMillis();

        if (!(request instanceof HttpServletRequest httpRequest) || !(response instanceof HttpServletResponse httpResponse))
        {
            chain.doFilter(request, response);
            return;
        }

        // Custom input and output stream to be able to read and write them twice
        ResettableStreamHttpServletRequest newRequest = new ResettableStreamHttpServletRequest(httpRequest);
        ResettableStreamHttpServletResponse newResponse = new ResettableStreamHttpServletResponse(httpResponse);

        // Main execution
        chain.doFilter(newRequest, newResponse);

        // Creating a log
        ILogService logService = context.getBean(ILogService.class);
        IUserService userService = context.getBean(IUserService.class);

        UserBusinessModel currentUser = userService.getCurrentUser();

        String requestContent = null;
        if (newRequest.getContentType() != null
                && mediaTypesToLog.contains(newRequest.getContentType().split(";")[0]))
            requestContent = newRequest.getReader().lines().collect(Collectors.joining());

        String responseContent = null;
        if (newResponse.getContentType() != null
                && mediaTypesToLog.contains(newResponse.getContentType().split(";")[0]))
            responseContent = new String(ArrayUtils.toPrimitive(newResponse.rawData.toArray(new Byte[0])), StandardCharsets.UTF_8);

        String queryParams = "";
        if (newRequest.getQueryString() != null && !newRequest.getQueryString().isBlank())
            queryParams = "?" + newRequest.getQueryString();

        long endedAt = System.currentTimeMillis();
        LogBusinessModel log = new LogBusinessModel() {{
            setCalledAt(System.currentTimeMillis());
            setCaller(currentUser != null ? currentUser.getEmail() : null);
            setElapsedTime(endedAt - startedAt);
            setRequestMethod(newRequest.getMethod());
            setResponseCode(newResponse.getStatus());
        }};
        log.setRequestRoute(newRequest.getRequestURI() + queryParams);
        log.setRequestContent(requestContent);
        log.setResponseContent(responseContent);
        logService.addLog(log);
    }
}
