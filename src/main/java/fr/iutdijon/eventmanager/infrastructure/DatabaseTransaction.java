package fr.iutdijon.eventmanager.infrastructure;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DatabaseTransaction implements AutoCloseable {
    final Connection connection;
    private final AbstractDatabaseConnection databaseConnection;
    private final DatabaseTransaction parentTransaction;
    private final List<Runnable> preCommitActions;
    private final List<Runnable> postCommitActions;

    DatabaseTransaction(Connection connection, AbstractDatabaseConnection databaseConnection, DatabaseTransaction parentTransaction) {
        this.connection = connection;
        this.databaseConnection = databaseConnection;
        this.parentTransaction = parentTransaction;
        this.preCommitActions = parentTransaction == null ? new ArrayList<>() : null;
        this.postCommitActions = parentTransaction == null ? new ArrayList<>() : null;
    }

    /**
     * Get the highest parent transaction
     * @return The highest parent transaction
     */
    private DatabaseTransaction getTopTransaction() {
        if (parentTransaction != null)
            return parentTransaction.getTopTransaction();
        return this;
    }

    /**
     * Add an action to execute just before the commit validation
     * @param preCommitAction The {@link Runnable} to run just before the commit validation
     */
    public void addPreCommitAction(Runnable preCommitAction) {
        this.getTopTransaction().preCommitActions.add(preCommitAction);
    }

    /**
     * Add an action to execute just after the commit validation
     * @param postCommitAction The {@link Runnable} to run just after the commit validation
     */
    public void addPostCommitAction(Runnable postCommitAction) {
        this.getTopTransaction().postCommitActions.add(postCommitAction);
    }

    /**
     * Commit the transaction if it is not a sub-transaction
     * @throws SQLException If an error occurs while committing the transaction
     */
    public void commit() throws SQLException {
        if (this.parentTransaction == null) {
            this.preCommitActions.forEach(Runnable::run);
            this.connection.commit();
            this.postCommitActions.forEach(Runnable::run);
        }
    }

    /**
     * Rollback the transaction
     * @throws SQLException If the rollback operation fails
     */
    public void rollback() throws SQLException {
        if (parentTransaction != null)
            this.parentTransaction.rollback();
        else
            this.connection.rollback();
    }

    @Override
    public void close() throws SQLException {
        if (parentTransaction != null)
            return;
        this.connection.rollback();
        this.connection.close();
        this.databaseConnection.currentTransaction.remove();
    }
}
