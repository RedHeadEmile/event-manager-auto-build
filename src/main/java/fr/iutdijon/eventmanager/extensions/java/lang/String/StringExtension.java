package fr.iutdijon.eventmanager.extensions.java.lang.String;

import manifold.ext.rt.api.Extension;
import manifold.ext.rt.api.This;

@Extension
public class StringExtension {
    public static boolean isNullOrEmpty(@This String input) {
        return input == null || input.isEmpty();
    }
}
