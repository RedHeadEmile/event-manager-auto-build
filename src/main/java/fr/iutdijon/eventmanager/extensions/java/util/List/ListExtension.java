package fr.iutdijon.eventmanager.extensions.java.util.List;

import manifold.ext.rt.api.Extension;
import manifold.ext.rt.api.This;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

@Extension
public class ListExtension {

    /**
     * Convert a list instance into another list with a converter
     * @param input The original list
     * @param mapper The function to convert each object inside of this list into another object
     * @return A new list with all the previous items converted
     * @param <E> The object type of the input list
     * @param <F> The object type of the output list
     */
    public static <E, F> List<F> map(@This List<E> input, Function<E, F> mapper) {
        List<F> output = new ArrayList<>();
        for (E element : input)
            output.add(mapper.apply(element));
        return output;
    }

    /**
     * Flatten a 2 dimensions list into a single dimension list
     * @param input The list to reduce
     * @return The flatten list
     * @param <E> The object type of the list
     */
    public static <E> List<E> flatten(@This List<List<E>> input) {
        List<E> output = new ArrayList<>();
        for (List<E> element : input)
            output.addAll(element);
        return output;
    }

    /**
     * Check if any element of this list match a predicate
     * @param input The list to iterate from
     * @param predicate The predicate to match
     * @return True if at least one element of the list match the predicate, False otherwise
     * @param <E> The element type
     */
    public static <E> boolean contains(@This List<E> input, Predicate<E> predicate) {
        for (E e : input)
            if (predicate.test(e))
                return true;
        return false;
    }

    /**
     * Apply filter on a list
     * @param input The list to filter
     * @param predicate The predicate to indicate if the item is kept
     * @return The filtered list
     * @param <E> The element type
     */
    public static <E> List<E> filter(@This List<E> input, Predicate<E> predicate) {
        List<E> output = new ArrayList<>();
        for (E e : input)
            if (predicate.test(e))
                output.add(e);
        return output;
    }

    /**
     * Apply filter on a list
     * @param input The list to filter
     * @param predicate The predicate to indicate if the item is kept
     * @param limit Maximum amount of item
     * @return The filtered list
     * @param <E> The element type
     */
    public static <E> List<E> filter(@This List<E> input, Predicate<E> predicate, int limit) {
        List<E> output = new ArrayList<>();
        for (int i = 0; i < input.size() && output.size() < limit; i++) {
            E e = input.get(i);
            if (predicate.test(e))
                output.add(e);
        }

        return output;
    }

    //#region Single methods

    /**
     * Find an element in a list matching a predicate, only one element in the list must match the predicate.<br/>
     * If more than one element match the predicate, an {@link IllegalStateException} if thrown
     * @param input The list to iterate from
     * @param predicate The predicate to match
     * @return The only element matching the predicate, null if no element found
     * @param <E> The element type
     */
    public static <E> E singleOrNull(@This List<E> input, Predicate<E> predicate) {
        E toReturn = null;
        for (E e : input)
            if (predicate.test(e)) {
                if (toReturn != null)
                    throw new IllegalStateException("More than one element found");

                toReturn = e;
            }

        return toReturn;
    }

    /**
     * Retrieve the unique element of a list.<br/>
     * If the list contains more than one element, an {@link IllegalStateException} is thrown<br/>
     * @param input The list to iterate from
     * @return The unique element of this list or null if there is no element in this list.
     * @param <E> The element type
     */
    public static <E> E singleOrNull(@This List<E> input) {
        return singleOrNull(input, ignore -> true);
    }

    /**
     * Find an element in a list matching a predicate, only one element in the list must match the predicate.<br/>
     * If not exactly one element match the predicate, an {@link IllegalStateException} if thrown
     * @param input The list to iterate from
     * @param predicate The predicate to match
     * @return The only element matching the predicate which cannot be null
     * @param <E> The element type
     */
    public static <E> E single(@This List<E> input, Predicate<E> predicate) {
        E toReturn = singleOrNull(input, predicate);
        if (toReturn == null)
            throw new IllegalStateException("No element match the predicate");

        return toReturn;
    }


    /**
     * Retrieve the unique element of a list.<br/>
     * If the list doesn't contain one element, an {@link IllegalStateException} is thrown<br/>
     * @param input The list to iterate from
     * @return The unique element of this list which cannot be null.
     * @param <E> The element type
     */
    public static <E> E single(@This List<E> input) {
        return single(input, ignore -> true);
    }
    //#endregion

    //#region First methods

    /**
     * Find the first element in a list matching a predicate
     * @param input The list to iterate from
     * @param predicate The predicate to match
     * @return The first element matching the predicate, null if no element found
     * @param <E> The element type
     */
    public static <E> E firstOrNull(@This List<E> input, Predicate<E> predicate) {
        for (E e : input)
            if (predicate.test(e))
                return e;
        return null;
    }
    /**
     * Retrieve the first element of a list.
     * @param input The list to iterate from
     * @return The first element of this list or null if there is no element in this list.
     * @param <E> The element type
     */
    public static <E> E firstOrNull(@This List<E> input) {
        return firstOrNull(input, ignore -> true);
    }

    /**
     * Find the first element in a list matching a predicate
     * If not element match the predicate, an {@link IllegalStateException} if thrown
     * @param input The list to iterate from
     * @param predicate The predicate to match
     * @return The only element matching the predicate which cannot be null
     * @param <E> The element type
     */
    public static <E> E first(@This List<E> input, Predicate<E> predicate) {
        E toReturn = firstOrNull(input, predicate);
        if (toReturn == null)
            throw new IllegalStateException("No element match the predicate");

        return toReturn;
    }

    /**
     * Retrieve the first element of a list.<br/>
     * If the list doesn't contain any element, an {@link IllegalStateException} is thrown<br/>
     * @param input The list to iterate from
     * @return The first element of this list which cannot be null.
     * @param <E> The element type
     */
    public static <E> E first(@This List<E> input) {
        return first(input, ignore -> true);
    }
    //#endregion
}
