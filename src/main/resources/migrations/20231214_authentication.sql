ALTER TABLE `user`
    ADD COLUMN `nextlogindate` DATETIME(6) NULL AFTER `lastname`,
    ADD COLUMN `remainingtries` SMALLINT UNSIGNED NOT NULL AFTER `permission`;

UPDATE `user` SET remainingtries = 3;
