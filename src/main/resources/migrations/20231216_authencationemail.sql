INSERT INTO `emailtemplate` (code, content, subject)
VALUES
    ("login-failed", "De trop nombreuses tentatives de connexions à votre compte ont échoués. Si vous n'êtes pas à l'origine de ces connexions, pensez à changer votre mot de passe.", "Echecs de connexion"),
    ("login-successful", "Une nouvelle connexion à votre compte a été effectuée.", "Nouvelle connexion");