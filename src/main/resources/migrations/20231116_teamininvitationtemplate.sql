INSERT INTO `emailtemplate` (`code`, `content`, `subject`)
VALUES ('team-invitation', 'Vous avez été invité dans une équipe! Vous pouvez la rejoindre en suivant ce lien: <a href=\"{teamInvitationLink}\">{teamInvitationLink}</a>', 'Invitation à une équipe');
