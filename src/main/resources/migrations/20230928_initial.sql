CREATE TABLE `document` (
                            `documentid` bigint unsigned NOT NULL auto_increment PRIMARY KEY,
                            `createdat` datetime(6),
                            `name` varchar(255),
                            `type` varchar(50)
);

CREATE TABLE `user` (
                        `userid` bigint unsigned NOT NULL auto_increment PRIMARY KEY,
                        `document_documentid` bigint unsigned NULL,
                        `authtoken` varchar(128) unique,
                        `createdat` datetime(6),
                        `email` varchar(255) UNIQUE,
                        `emailconfirmed` boolean,
                        `firstname` varchar(255),
                        `lastname` varchar(255),
                        `password` varchar(255),
                        `permission` bigint unsigned default 0,
                        `traininglabel` varchar(255),
                        `traininglocation` varchar(255),
                        `trainingyear` varchar(255)
);

CREATE TABLE `usermagiclink` (
                                 `usermagiclinkid` bigint unsigned NOT NULL auto_increment PRIMARY KEY,
                                 `user_userid` bigint unsigned NOT NULL,
                                 `code` varchar(32) UNIQUE,
                                 `createdat` datetime(6),
                                 `used` boolean
);

CREATE TABLE `event` (
                         `eventid` bigint unsigned NOT NULL auto_increment PRIMARY KEY,
                         `user_userid` bigint unsigned NOT NULL,
                         `allowteamname` boolean,
                         `beggining` datetime(6),
                         `code` varchar(10) UNIQUE,
                         `description` text,
                         `maximalteamsize` integer,
                         `minimalteamsize` integer,
                         `name` varchar(255),
                         `registerbeggining` datetime(6),
                         `registerending` datetime(6)
);

CREATE TABLE `user_event` (
                              `user_userid` bigint unsigned NOT NULL,
                              `event_eventid` bigint unsigned NOT NULL,
                              PRIMARY KEY (`user_userid`, `event_eventid`)
);

CREATE TABLE `eventdocumentrequirement` (
                                            `eventdocumentrequirementid` bigint unsigned NOT NULL auto_increment PRIMARY KEY,
                                            `event_eventid` bigint unsigned NOT NULL,
                                            `individualdocument` boolean,
                                            `name` varchar(255),
                                            `optionnal` boolean
);

CREATE TABLE `team` (
                        `teamid` bigint unsigned NOT NULL auto_increment PRIMARY KEY,
                        `event_eventid` bigint unsigned NOT NULL,
                        `code` varchar(10) UNIQUE,
                        `name` varchar(255),
                        `validated` boolean
);

CREATE TABLE `team_user` (
                             `user_userid` bigint unsigned NOT NULL,
                             `team_teamid` bigint unsigned NOT NULL,
                             PRIMARY KEY (`user_userid`, `team_teamid`)
);

CREATE TABLE `team_document` (
                                 `eventdocumentrequirement_eventdocumentrequirementid` bigint unsigned NOT NULL auto_increment,
                                 `team_teamid` bigint unsigned NOT NULL,
                                 `document_documentid` bigint unsigned NOT NULL,
                                 PRIMARY KEY (`eventdocumentrequirement_eventdocumentrequirementid`, `team_teamid`, `document_documentid`)
);

CREATE TABLE `user_document` (
                                 `eventdocumentrequirement_eventdocumentrequirementid` bigint unsigned NOT NULL,
                                 `user_userid` bigint unsigned NOT NULL,
                                 `document_documentid` bigint unsigned NOT NULL,
                                 PRIMARY KEY (`eventdocumentrequirement_eventdocumentrequirementid`, `user_userid`, `document_documentid`)
);

CREATE TABLE `teaminvitation` (
                                  `teaminvitationid` bigint unsigned NOT NULL auto_increment PRIMARY KEY,
                                  `user_userid` bigint unsigned NOT NULL,
                                  `team_teamid` bigint unsigned NOT NULL,
                                  `recipient` varchar(255)
);

CREATE TABLE `teammergesuggestion` (
                                       `first_team_teamid` bigint unsigned NOT NULL,
                                       `second_team_teamid` bigint unsigned NOT NULL,
                                       merged boolean,
                                       PRIMARY KEY (`first_team_teamid`, `second_team_teamid`)
);

CREATE TABLE `eventemailhistory` (
                                     `eventemailhistoryid` bigint unsigned NOT NULL auto_increment PRIMARY KEY,
                                     `event_eventid` bigint unsigned NOT NULL,
                                     `content` blob,
                                     `createdat` datetime(6)
);

CREATE TABLE `teamemailhistory` (
                                    `teamemailhistoryid` bigint unsigned NOT NULL auto_increment PRIMARY KEY,
                                    `team_teamid` bigint unsigned NOT NULL,
                                    `content` blob,
                                    `createdat` datetime(6)
);

CREATE TABLE `log` (
    `logid`           bigint unsigned NOT NULL auto_increment PRIMARY KEY,
    `calledat`        datetime(6) not null,
    `caller`          varchar(255),
    `elapsedtime`     bigint unsigned null,
    `requestcontent`  longtext null,
    `requestmethod`   varchar(7),
    `requestroute`    varchar(255) null,
    `responsecode`    int unsigned null,
    `responsecontent` longtext null
);

ALTER TABLE `user` ADD CONSTRAINT FK_user_document FOREIGN KEY (`document_documentid`) REFERENCES `document` (`documentid`);

ALTER TABLE `usermagiclink` ADD CONSTRAINT FK_usermagiclink_user FOREIGN KEY (`user_userid`) REFERENCES `user` (`userid`);

ALTER TABLE `event` ADD CONSTRAINT FK_event_user FOREIGN KEY (`user_userid`) REFERENCES `user` (`userid`);

ALTER TABLE `user_event` ADD CONSTRAINT FK_userevent_user FOREIGN KEY (`user_userid`) REFERENCES `user` (`userid`);

ALTER TABLE `user_event` ADD CONSTRAINT FK_userevent_event FOREIGN KEY (`event_eventid`) REFERENCES `event` (`eventid`);

ALTER TABLE `eventdocumentrequirement` ADD CONSTRAINT FK_eventdocumentrequirement_event FOREIGN KEY (`event_eventid`) REFERENCES `event` (`eventid`);

ALTER TABLE `team` ADD CONSTRAINT FK_team_event FOREIGN KEY (`event_eventid`) REFERENCES `event` (`eventid`);

ALTER TABLE `team_user` ADD CONSTRAINT FK_teamuser_user FOREIGN KEY (`user_userid`) REFERENCES `user` (`userid`);

ALTER TABLE `team_user` ADD CONSTRAINT FK_teamuser_team FOREIGN KEY (`team_teamid`) REFERENCES `team` (`teamid`);

ALTER TABLE `team_document` ADD CONSTRAINT FK_teamdocument_eventdocumentrequirement FOREIGN KEY (`eventdocumentrequirement_eventdocumentrequirementid`) REFERENCES `eventdocumentrequirement` (`eventdocumentrequirementid`);

ALTER TABLE `team_document` ADD CONSTRAINT FK_teamdocument_team FOREIGN KEY (`team_teamid`) REFERENCES `team` (`teamid`);

ALTER TABLE `team_document` ADD CONSTRAINT FK_teamdocument_document FOREIGN KEY (`document_documentid`) REFERENCES `document` (`documentid`);

ALTER TABLE `user_document` ADD CONSTRAINT FK_userdocument_eventdocumentrequirement FOREIGN KEY (`eventdocumentrequirement_eventdocumentrequirementid`) REFERENCES `eventdocumentrequirement` (`eventdocumentrequirementid`);

ALTER TABLE `user_document` ADD CONSTRAINT FK_userdocument_user FOREIGN KEY (`user_userid`) REFERENCES `user` (`userid`);

ALTER TABLE `user_document` ADD CONSTRAINT FK_userdocument_document FOREIGN KEY (`document_documentid`) REFERENCES `document` (`documentid`);

ALTER TABLE `teaminvitation` ADD CONSTRAINT FK_teaminvitation_user FOREIGN KEY (`user_userid`) REFERENCES `user` (`userid`);

ALTER TABLE `teaminvitation` ADD CONSTRAINT FK_teaminvitation_team FOREIGN KEY (`team_teamid`) REFERENCES `team` (`teamid`);

ALTER TABLE `teaminvitation` ADD CONSTRAINT UQ_teaminvitation UNIQUE (`team_teamid`, `recipient`);

ALTER TABLE `teammergesuggestion` ADD CONSTRAINT FK_teammergesuggestion_team1 FOREIGN KEY (`first_team_teamid`) REFERENCES `team` (`teamid`);

ALTER TABLE `teammergesuggestion` ADD CONSTRAINT FK_teammergesuggestion_team2 FOREIGN KEY (`second_team_teamid`) REFERENCES `team` (`teamid`);

ALTER TABLE `eventemailhistory` ADD CONSTRAINT FK_eventmailhistory_event FOREIGN KEY (`event_eventid`) REFERENCES `event` (`eventid`);

ALTER TABLE `teamemailhistory` ADD CONSTRAINT FK_teammailhistory_team FOREIGN KEY (`team_teamid`) REFERENCES `team` (`teamid`);
