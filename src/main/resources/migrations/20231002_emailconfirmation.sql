ALTER TABLE `document`
    CHANGE COLUMN `createdat` `createdat` DATETIME(6) NOT NULL ,
    CHANGE COLUMN `name` `name` VARCHAR(255) NOT NULL ,
    CHANGE COLUMN `type` `type` VARCHAR(50) NOT NULL ;
ALTER TABLE `event`
    CHANGE COLUMN `allowteamname` `allowteamname` TINYINT(1) NOT NULL ,
    CHANGE COLUMN `code` `code` VARCHAR(10) NOT NULL ;
ALTER TABLE `eventdocumentrequirement`
    CHANGE COLUMN `individualdocument` `individualdocument` TINYINT(1) UNSIGNED NOT NULL ,
    CHANGE COLUMN `name` `name` VARCHAR(255) NOT NULL ,
    CHANGE COLUMN `optionnal` `optionnal` TINYINT(1) UNSIGNED NOT NULL ;
ALTER TABLE `eventemailhistory`
    CHANGE COLUMN `content` `content` BLOB NOT NULL ,
    CHANGE COLUMN `createdat` `createdat` DATETIME(6) NOT NULL ;
ALTER TABLE `team`
    CHANGE COLUMN `code` `code` VARCHAR(10) NOT NULL ,
    CHANGE COLUMN `name` `name` VARCHAR(255) NOT NULL ,
    CHANGE COLUMN `validated` `validated` TINYINT(1) NOT NULL ;
ALTER TABLE `event`
    CHANGE COLUMN `name` `name` VARCHAR(255) NOT NULL ;
ALTER TABLE `teamemailhistory`
    CHANGE COLUMN `content` `content` BLOB NOT NULL ,
    CHANGE COLUMN `createdat` `createdat` DATETIME(6) NOT NULL ;
ALTER TABLE `teaminvitation`
    CHANGE COLUMN `recipient` `recipient` VARCHAR(255) NOT NULL ;
ALTER TABLE `teammergesuggestion`
    CHANGE COLUMN `merged` `merged` TINYINT(1) UNSIGNED NOT NULL ;
ALTER TABLE `user`
    CHANGE COLUMN `createdat` `createdat` DATETIME(6) NOT NULL ,
    CHANGE COLUMN `email` `email` VARCHAR(255) NOT NULL ,
    CHANGE COLUMN `emailconfirmed` `emailconfirmed` TINYINT(1) UNSIGNED NOT NULL ,
    CHANGE COLUMN `permission` `permission` BIGINT UNSIGNED NOT NULL DEFAULT '0' ;
ALTER TABLE `usermagiclink`
    CHANGE COLUMN `code` `code` VARCHAR(32) NOT NULL ,
    CHANGE COLUMN `createdat` `createdat` DATETIME(6) NOT NULL ,
    CHANGE COLUMN `used` `used` TINYINT(1) NOT NULL ;
ALTER TABLE `user`
    ADD COLUMN `emailconfirmationtoken` VARCHAR(32) NULL DEFAULT NULL AFTER `email`,
ADD UNIQUE INDEX `emailconfirmationtoken_UNIQUE` (`emailconfirmationtoken` ASC);

INSERT INTO `emailtemplate` (code, content, subject)
VALUES
    ("email-confirmation", "Lien pour confirmer votre email: <a href=\"{emailConfirmLink}\">{emailConfirmLink}</a>", "Validation de votre email");