ALTER TABLE `user_document`
    ADD UNIQUE INDEX `UQ_userdocument` (`eventdocumentrequirement_eventdocumentrequirementid` ASC, `user_userid` ASC);
