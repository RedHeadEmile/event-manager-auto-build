ALTER TABLE `emailtemplate`
    ADD COLUMN `user_userid` BIGINT UNSIGNED NULL DEFAULT NULL AFTER `emailtemplateid`,
    ADD COLUMN `label` VARCHAR(45) NULL DEFAULT NULL AFTER `content`,
    ADD COLUMN `shared` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `label`,
    ADD INDEX `FK_emailtemplate_user_idx` (`user_userid` ASC);

ALTER TABLE `emailtemplate`
    ADD CONSTRAINT `FK_emailtemplate_user`
        FOREIGN KEY (`user_userid`)
            REFERENCES `user` (`userid`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION;
