ALTER TABLE `teamemailhistory`
    ADD COLUMN `attachmentamount` INT UNSIGNED NOT NULL AFTER `team_teamid`,
    ADD COLUMN `subject` VARCHAR(255) NOT NULL AFTER `createdat`;

ALTER TABLE `eventemailhistory`
    ADD COLUMN `attachmentamount` INT UNSIGNED NOT NULL AFTER `event_eventid`,
    ADD COLUMN `subject` VARCHAR(255) NOT NULL AFTER `createdat`;
