import {Component, HostListener, OnInit} from '@angular/core';
import {AuthenticationService} from "../modules/shared/services/authentication.service";
import {ModalService} from "../modules/shared/services/modal.service";
import {SettingsService} from "../modules/shared/services/settings.service";
import {Router} from "@angular/router";

export type MenuItem = {
  label: string;
  route?: string;
  action?: () => void;
  mustBeAdministrator?: boolean;
  mustBeDeveloper?: boolean;
  mustBeModerator?: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  host: {
    class: 'flex-column flex-fill'
  }
})
export class AppComponent implements OnInit {
  readonly menuItems: MenuItem[] = [
    {
      label: 'Paramètres',
      route: '/settings',
    },
    {
      label: 'Gérer les événements',
      route: '/events',
      mustBeModerator: true,
    },
    {
      label: 'Gérer les utilisateurs',
      route: '/users',
      mustBeAdministrator: true,
    },
    {
      label: 'Gérer les emails',
      route: '/email-template',
      mustBeAdministrator: true,
    },
    {
      label: 'Gérer les emails',
      route: '/email-template',
      mustBeModerator: true,
    },
    {
      label: 'Se déconnecter',
      action: async () => {
        await this.logout();
        await this._router.navigate(['/login']);
      },
    }
  ];

  constructor(
    private readonly _authenticationService: AuthenticationService,
    private readonly _modalService: ModalService,
    private readonly _settingsService: SettingsService,
    private readonly _router: Router
  ) {
  }

  get isUserLogged(): boolean {
    return this._authenticationService.currentUser !== undefined;
  }

  async logout(): Promise<void> {
    await this._authenticationService.logout();
  }

  async ngOnInit(): Promise<void> {
    await this._authenticationService.refreshUser();
  }

  private _isMenuOpened = false;

  get isMenuOpened(): boolean {
    return this._isMenuOpened;
  }

  toggleMenu(): void {
    this._isMenuOpened = !this._isMenuOpened;
    console.log(this._isMenuOpened)
    if (this._isMenuOpened)
      document.body.className += "no-scrolling";
    else {
      document.body.classList.remove("no-scrolling");
    }
  }

  closeMenu() {
    if (this._isMenuOpened)
      this._isMenuOpened = false;
  }

  canBeShown(menuItem: MenuItem): boolean {
    if (menuItem.mustBeDeveloper)
      return this._authenticationService.isCurrentUserDeveloper;

    if (menuItem.mustBeAdministrator)
      return this._authenticationService.isCurrentUserDeveloper || this._authenticationService.isCurrentUserAdministrator;

    if (menuItem.mustBeModerator)
      return this._authenticationService.isCurrentUserDeveloper || this._authenticationService.isCurrentUserAdministrator || this._authenticationService.isCurrentUserModerator;

    return true;
  }

  openQrCodeModal() {
    this._modalService.openQRCodeModal({
      code: 'coucou',
      url: 'http://localhost:4200/bonjour_les_gens'
    });
  }

  downloadStatistics() {
    const statisticsUrl = this._settingsService.API_BASE_URL + '/statistics';
    window.open(statisticsUrl, '_blank');
  }

  protected readonly event = event;
}
