import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {developerGuard} from "../modules/shared/guards/developer.guard";
import {anonymousGuard} from "../modules/shared/guards/anonymous.guard";
import {authenticationGuard} from "../modules/shared/guards/authentication.guard";
import {administratorGuard} from "../modules/shared/guards/administrator.guard";
import {moderatorGuard} from "../modules/shared/guards/moderator.guard";
import {administratorOrModeratorGuard} from "../modules/shared/guards/administrator-or-moderator.guard";

const routes: Routes = [
  {
    path: 'dev-tools',
    canActivate: [developerGuard],
    loadChildren: () => import('../modules/dev-tools/dev-tools.module').then(m => m.DevToolsModule)
  },
  {
    path: 'email-confirm',
    loadChildren: () => import('../modules/email-confirm/email-confirm.module').then(m => m.EmailConfirmModule)
  },
  {
    path: 'email-template',
    canActivate: [administratorOrModeratorGuard],
    loadChildren: () => import('../modules/email-template/email-template.module').then(m => m.EmailTemplateModule)
  },
  {
    path: 'events',
    canActivate: [authenticationGuard],
    loadChildren: () => import('../modules/event/event.module').then(m => m.EventModule)
  },
  {
    path: 'join-team',
    canActivate: [authenticationGuard],
    loadChildren: () => import('../modules/join-team/join-team.module').then(m => m.JoinTeamModule)
  },
  {
    path: 'login',
    canActivate: [anonymousGuard],
    loadChildren: () => import('../modules/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'register',
    canActivate: [anonymousGuard],
    loadChildren: () => import('../modules/register/register.module').then(m => m.RegisterModule)
  },
  {
    path: 'password-reset',
    canActivate: [anonymousGuard],
    loadChildren: () => import('../modules/reset-password/reset-password.module').then(m => m.ResetPasswordModule)
  },
  {
    path: '',
    canActivate: [authenticationGuard],
    loadChildren: () => import('../modules/event/event.module').then(m => m.EventModule)
  },
  {
    path: 'settings',
    canActivate: [authenticationGuard],
    loadChildren: () => import('../modules/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'users',
    canActivate: [administratorGuard],
    loadChildren: () => import('../modules/user/user.module').then(m => m.UserModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
