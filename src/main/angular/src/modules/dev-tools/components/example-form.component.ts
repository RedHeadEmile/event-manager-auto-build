import {Component, ViewChild} from "@angular/core";
import {NgForm} from "@angular/forms";

interface ExampleViewModel {
  firstname?: string;
  lastname?: string;
  birthday?: Date;
  preferredColor?: 'black' | 'white' | 'gray';
}

@Component({
  templateUrl: './example-form.component.html'
})
export class ExampleFormComponent {
  private _model: ExampleViewModel = {};

  @ViewChild('myForm', { static: true }) private _myForm!: NgForm;

  get model(): ExampleViewModel {
    return this._model;
  }

  get isMyFormValid(): boolean {
    return this._myForm.valid ?? false;
  }

  submit(): void {

  }
}
