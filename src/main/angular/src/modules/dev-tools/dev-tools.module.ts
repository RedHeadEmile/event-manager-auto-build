import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import {ExampleFormComponent} from "./components/example-form.component";

@NgModule({
  declarations: [
    ExampleFormComponent
  ],
  imports: [
    SharedModule,

    RouterModule.forChild([
      {
        path: 'example-form',
        pathMatch: 'full',
        component: ExampleFormComponent
      }
    ])
  ],
  exports: []
})
export class DevToolsModule { }
