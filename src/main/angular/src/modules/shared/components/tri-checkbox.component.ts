import {Component, HostBinding, HostListener, Input} from "@angular/core";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'tri-checkbox',
  template: '<input type="checkbox" [name]="name" [checked]="checked" [indeterminate]="indeterminate" [disabled]="disabled" style="pointer-events: none">',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: TriCheckboxComponent
    }
  ]
})
export class TriCheckboxComponent implements ControlValueAccessor {
  // cycle: unchecked => checked => indeterminate
  indeterminate: boolean = false;
  checked: boolean = false;
  disabled: boolean = false;

  @Input('name') name?: string;

  @HostBinding('style.cursor')
  get cursor(): string {
    return this.disabled ? 'default' : 'pointer';
  }

  @HostListener('click', ['$event'])
  onClick(event: MouseEvent) {
    event.preventDefault();
    event.stopPropagation();
    event.stopImmediatePropagation();

    if (this.disabled)
      return;

    if (this.indeterminate) {
      this.indeterminate = false;
      this.checked = false;
    }
    else if (!this.checked) {
      this.indeterminate = false;
      this.checked = true;
    }
    else if (this.checked) {
      this.indeterminate = true;
      this.checked = false;
    }

    this._onTouched();
    this._onChange(this.indeterminate ? undefined : this.checked);
  }

  writeValue(value: boolean | undefined) {
    if (value === undefined) {
      this.indeterminate = true;
    }
    else {
      this.indeterminate = false;
      this.checked = value;
    }
  }

  private _onChange: (value: boolean | undefined) => void = () => {};
  registerOnChange(fn: (value: boolean | undefined) => void) {
    this._onChange = fn;
  }

  private _onTouched: () => void = () => {};
  registerOnTouched(fn: () => void) {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
