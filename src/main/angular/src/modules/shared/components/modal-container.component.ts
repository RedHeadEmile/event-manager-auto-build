import {Component, ElementRef, HostBinding, HostListener, NgZone, ViewChild} from '@angular/core';
import {ModalService} from "../services/modal.service";
import {Subject} from "rxjs";
import {DynamicDirective} from "../directives/dynamic.directive";

@Component({
  selector: 'modal-container',
  template: `
    <div #modalContainer
         (click)="onTemplateHostClick($event)"
         [style.min-height]="minHeight"
         [style.max-height]="maxHeight"
         [style.min-width]="minWidth"
         [style.max-width]="maxWidth"
         class="position-relative standard-box p-5 flex-column bg-white">
      <div class="position-absolute right-0 top-0 m-2 flex justify-end">
        <button (click)="onHostClick()" class="button-close">
          X
        </button>
      </div>
      <ng-template dynamic/>
    </div>`,
  styleUrls: ['./modal-container.component.less'],
  host: {
    'class': 'position-fixed top-0 left-0 bottom-0 right-0 flex justify-center align-center p-8'
  }
})
export class ModalContainerComponent {

  @ViewChild(DynamicDirective, { static: true }) private dynamicHost!: DynamicDirective;
  @ViewChild('modalContainer', { static: true }) private modalContainer!: ElementRef<HTMLDivElement>;

  private _closeSubject?: Subject<any>;

  constructor(
    private readonly _modalService: ModalService,
    private readonly _zone: NgZone
  ) {
    const me = this;
    me._modalService.modalOpenObserver.subscribe(config => {
      me.dynamicHost.viewContainerRef.clear();
      const componentRef = me.dynamicHost.viewContainerRef.createComponent(config.component);
      const onCloseSubject: Subject<any> = new Subject<any>();

      componentRef.instance.input = config.data;
      componentRef.instance.output = onCloseSubject;

      onCloseSubject.subscribe(output => {
        onCloseSubject.complete();
        me._closeSubject = undefined;

        const keyFrames = [ { transform: 'scaleX(100%) scaleY(100%)' }, { transform: 'scaleX(0%) scaleY(0%)' } ];
        me.modalContainer.nativeElement.animate(keyFrames, { duration: 100, easing: 'ease-out' })
          .onfinish = () => {
            me._zone.run(() => {
              if (!!config.onClose)
                config.onClose(output);

              me.dynamicHost.viewContainerRef.clear();
              me.hidden = true;
            });
          };
      });

      me._closeSubject = onCloseSubject;
      me.hidden = false;
      me.minHeight = config.minHeight;
      me.maxHeight = config.maxHeight;
      me.minWidth = config.minWidth;
      me.maxWidth = config.maxWidth;
    });
  }

  @HostBinding('class.hidden')
  hidden: boolean = true;

  minHeight?: string;
  maxHeight?: string;
  minWidth?: string;
  maxWidth?: string;

  @HostListener("click")
  onHostClick() {
    this._closeSubject?.next(undefined);
  }

  @HostListener("document:keydown.escape")
  onHostEscaped() {
    this._closeSubject?.next(undefined);
  }

  onTemplateHostClick(event: MouseEvent) {
    event.stopPropagation();
  }
}
