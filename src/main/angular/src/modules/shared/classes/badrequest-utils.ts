import {ApiException, ErrorViewModel} from "../services/api.service";

export class BadRequestUtils {
  /**
   * Convert an object which must be an ApiException to an ErrorViewModel.
   * If the object cannot be converted to an ErrorViewModel an exception is thrown
   * @param obj The supposed ApiException to convert
   */
  static asBadRequestException(obj: any): ErrorViewModel {
    if (ApiException.isApiException(obj)) {
      if (obj.status !== 400)
        throw obj;

      try {
        return ErrorViewModel.fromJS(JSON.parse(obj.response));
      }
      catch (_) {
        throw obj;
      }
    }

    throw new Error('`obj` is not an ApiException');
  }
}
