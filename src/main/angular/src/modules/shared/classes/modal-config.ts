import {Modal} from "./modal";
import {Type} from "@angular/core";

export interface ModalConfig<I, O> {
  component: Type<Modal<I, O>>;
  minHeight?: string;
  maxHeight?: string;
  minWidth?: string;
  maxWidth?: string;

  data?: I;
  onClose?: (output?: O) => void;
}
