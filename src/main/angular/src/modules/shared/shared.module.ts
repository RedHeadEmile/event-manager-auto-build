import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {DocumentUrlPipe} from './pipes/document-url.pipe';
import {ModalContainerComponent} from './components/modal-container.component';
import {ErrorModal} from "./modals/error.modal";
import {DynamicDirective} from './directives/dynamic.directive';
import {QRCodeModule} from "angularx-qrcode";
import {QrcodeModal} from "./modals/qrcode.modal";
import {CloseButtonDirective} from "./directives/close-button.directive";
import {BigButtonDirective} from "./directives/big-button.directive";
import {DateValueDirective} from "./directives/date-value.directive";
import {MinDateDirective} from "./directives/min-date.directive";
import {MaxDateDirective} from "./directives/max-date.directive";
import {YesNoModal} from "./modals/yes-no.modal";
import {FileModal} from "./modals/file.modal";
import {QuillConfigModule, QuillModule, QuillModules} from "ngx-quill";
import {PlaceholderTypePipe} from "./pipes/placeholder-type.pipe";
import {TriCheckboxComponent} from "./components/tri-checkbox.component";
import {EmailContentModal} from "./modals/email-content.modal";
import {EmailRedactionModal} from "./modals/email-redaction.modal";

const _quillModules: QuillModules = {
  clipboard: true,
  history: true,
  keyboard: true,
  syntax: false,
  toolbar: [
    ['bold', 'italic', 'underline', 'strike', { 'align': [] }],

    ['link', 'image', 'blockquote'],

    [{ 'list': 'ordered'}, { 'list': 'bullet' }],

    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

    [{ 'color': [] }, { 'background': [] }],
    [{ 'font': [] }]
  ]
};

@NgModule({
  declarations: [
    EmailContentModal,
    EmailRedactionModal,
    ErrorModal,
    FileModal,
    QrcodeModal,
    YesNoModal,

    ModalContainerComponent,
    TriCheckboxComponent,

    BigButtonDirective,
    CloseButtonDirective,
    DateValueDirective,
    DynamicDirective,
    MaxDateDirective,
    MinDateDirective,

    DocumentUrlPipe,
    PlaceholderTypePipe,
  ],
  imports: [
    CommonModule,
    FormsModule,
    QRCodeModule,
    QuillModule.forRoot(),
    QuillConfigModule.forRoot({
      modules: _quillModules,
      theme: 'snow'
    }),
  ],
  exports: [
    CommonModule,
    FormsModule,
    QRCodeModule,
    QuillModule,
    QuillConfigModule,

    ModalContainerComponent,
    TriCheckboxComponent,

    BigButtonDirective,
    CloseButtonDirective,
    DateValueDirective,
    MaxDateDirective,
    MinDateDirective,

    DocumentUrlPipe,
    PlaceholderTypePipe,
  ]
})
export class SharedModule { }
