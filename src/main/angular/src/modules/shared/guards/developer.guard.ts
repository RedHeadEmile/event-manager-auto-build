import {ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {inject} from "@angular/core";
import {AuthenticationService} from "../services/authentication.service";

export const developerGuard: CanActivateFn = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree => {
  return new Promise(async accept => {
    const router = inject(Router);
    try {
      const authenticationService = inject(AuthenticationService);
      const user = await authenticationService.getCurrentUserPromise();
      if (user == null)
        await router.navigate(['/login'], { queryParams: { 'redirect-url': state.url } });

      accept(authenticationService.isCurrentUserDeveloper)
    }
    catch (e) {
      await router.navigate(['/login'], { queryParams: { 'redirect-url': state.url } });
      accept(false);
    }
  });
};
