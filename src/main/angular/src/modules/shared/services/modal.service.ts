import {Injectable} from "@angular/core";
import {ModalConfig} from "../classes/modal-config";
import {Observable, Subject} from "rxjs";
import {QrcodeModal, QRCodeModalConfig} from "../modals/qrcode.modal";
import {YesNoModal, YesNoModalVariant} from "../modals/yes-no.modal";
import {FileModal, FileModalConfig} from "../modals/file.modal";
import {EmailContentModal} from "../modals/email-content.modal";
import {EmailStoreRequestViewModel, PlaceholderContextViewModel} from "./api.service";
import {EmailRedactionModal} from "../modals/email-redaction.modal";

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  private readonly _modalOpenSubject = new Subject<ModalConfig<any, any>>();

  constructor() { }

  public get modalOpenObserver(): Observable<ModalConfig<any, any>> {
    return this._modalOpenSubject.asObservable();
  }

  public openModal<I, O>(config: ModalConfig<I, O>) {
    this._modalOpenSubject.next(config);
  }

  public openEmailContentModal(content: string, onClose?: () => void) {
    this.openModal({
      component: EmailContentModal,
      onClose: onClose,
      data: content,
      minWidth: "100%",
      minHeight: "100%",
    });
  }

  public openEmailRedactionModal(context: PlaceholderContextViewModel, onClose: (email: EmailStoreRequestViewModel | undefined) => void) {
    this.openModal({
      component: EmailRedactionModal,
      onClose: onClose,
      data: context,
      minWidth: "100%",
      minHeight: "100%",
    });
  }

  public openQRCodeModal(config: QRCodeModalConfig, onClose?: () => void) {
    this.openModal({
      component: QrcodeModal,
      onClose: onClose,
      data: config,
    });
  }

  public openYesNoModal(title: string, message: string, onClose: (validated: boolean | undefined) => void, variant: YesNoModalVariant = 'yes-no') {
    this.openModal({
      component: YesNoModal,
      data: {
        title: title,
        message: message,
        variant: variant
      },
      onClose: onClose
    });
  }

  public openFileModal(content: FileModalConfig, onClose?: () => void) {
    const config: ModalConfig<FileModalConfig, undefined> = {
      component: FileModal,
      data: content,
      onClose: onClose
    };

    if (content.fileType === 'application/pdf') {
      config.minHeight = '100%';
      config.minWidth = '100%';
    }

    this.openModal(config);
  }
}

