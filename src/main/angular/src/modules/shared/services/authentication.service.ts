import {Injectable} from '@angular/core';
import {
  ApiException,
  ApiService, UserAuthenticationBasicRequestViewModel,
  UserAuthenticationMagicLinkRequestViewModel,
  UserRegistrationRequestViewModel, UserSettingsUpdateRequestViewModel,
  UserViewModel
} from "./api.service";
import {lastValueFrom} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private _connectionStatus: 'not-logged' | 'logged' | 'internal-error' = 'not-logged';
  private _currentUser?: UserViewModel;
  private _currentUserPromise?: Promise<UserViewModel | undefined>;

  constructor(private readonly _apiService: ApiService) { }

  public async refreshUser(): Promise<UserViewModel | undefined> {
    return this._currentUserPromise = new Promise<UserViewModel | undefined>(async accept => {
      try {
        this._currentUser = await lastValueFrom(this._apiService.showCurrentUser());
        this._connectionStatus = 'logged';
        accept(this._currentUser);
      }
      catch (e) {
        this._currentUser = undefined;
        if (ApiException.isApiException(e) && e.status === 401)
          this._connectionStatus = 'not-logged';
        else
          this._connectionStatus = 'internal-error';
        accept(undefined);
      }
    });
  }

  public connectionStatus(): 'not-logged' | 'logged' | 'internal-error' {
    return this._connectionStatus;
  }

  public get currentUser(): UserViewModel | undefined {
    return this._currentUser;
  }

  public get isCurrentUserAdministrator(): boolean {
    if (!this._currentUser)
      return false;

    return (this._currentUser.permission & 1) > 0;
  }

  public get isCurrentUserModerator(): boolean {
    if (!this._currentUser)
      return false;

    return (this._currentUser.permission & 2) > 0;
  }

  public get isCurrentUserDeveloper(): boolean {
    if (!this._currentUser)
      return false;

    return (this._currentUser.permission & 4) > 0;
  }

  public async getCurrentUserPromise(): Promise<UserViewModel | undefined> {
    if (!this._currentUserPromise)
      return await this.refreshUser();
    return await this._currentUserPromise;
  }

  public async register(registrationRequest: UserRegistrationRequestViewModel) {
    const registeredUser = await lastValueFrom(this._apiService.storeUserRegistration(registrationRequest));
    this._currentUser = registeredUser;
    this._currentUserPromise = Promise.resolve(registeredUser);
  }

  public async loginBasic(email: string, password: string) {
    const loggedUser = await lastValueFrom(this._apiService.storeCurrentAuthenticationRequest(new UserAuthenticationBasicRequestViewModel({ email: email, password: password })))
    this._currentUser = loggedUser;
    this._currentUserPromise = Promise.resolve(loggedUser);
  }

  public async loginMagicLink(magicLinkCode: string) {
    const loggedUser = await lastValueFrom(this._apiService.storeCurrentAuthenticationRequest(new UserAuthenticationMagicLinkRequestViewModel({ magicLinkCode: magicLinkCode })));
    this._currentUser = loggedUser;
    this._currentUserPromise = Promise.resolve(loggedUser);
  }

  public async logout(): Promise<void> {
    await lastValueFrom(this._apiService.deleteCurrentUserAuthentication());
    this._currentUser = undefined;
    this._currentUserPromise = Promise.resolve(undefined);
  }

  public async setSettings(settings: UserSettingsUpdateRequestViewModel) {
    const updatedUser = await lastValueFrom(this._apiService.setCurrentUserSettings(settings));
    this._currentUser = updatedUser;
    this._currentUserPromise = Promise.resolve(updatedUser);
  }
}
