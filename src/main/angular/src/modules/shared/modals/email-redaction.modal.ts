import {Component, OnInit} from "@angular/core";
import {Modal} from "../classes/modal";
import {
  ApiService, EmailPlaceholderViewModel,
  EmailStoreRequestViewModel,
  EmailTemplateViewModel,
  PlaceholderContextViewModel
} from "../services/api.service";
import {lastValueFrom, Subject} from "rxjs";

@Component({
  templateUrl: './email-redaction.modal.html',
  host: {
    class: 'content-width'
  },
})
export class EmailRedactionModal implements Modal<PlaceholderContextViewModel, EmailStoreRequestViewModel>, OnInit {
  input?: PlaceholderContextViewModel;
  output!: Subject<EmailStoreRequestViewModel | undefined>;

  model: EmailStoreRequestViewModel = new EmailStoreRequestViewModel({ subject: '', content: '', attachments: [], showMyAddressAsFrom: false, showMyAddressAsReplyTo: false });
  templates: EmailTemplateViewModel[] = [];
  placeholders: EmailPlaceholderViewModel[] = [];

  constructor(
    private readonly _apiService: ApiService
  ) { }

  async ngOnInit(): Promise<void> {
    const templateWithPlaceholders = await lastValueFrom(this._apiService.indexEmailTemplate(this.input ?? PlaceholderContextViewModel.ANY));
    this.templates = templateWithPlaceholders.templates;
    this.placeholders = templateWithPlaceholders.placeholders;
  }

  private _selectedTemplate?: EmailTemplateViewModel = undefined;
  get selectedTemplate(): EmailTemplateViewModel | undefined {
    return this._selectedTemplate;
  }

  set selectedTemplate(value: EmailTemplateViewModel | undefined) {
    this.model.subject = value?.subject ?? '';
    this.model.content = value?.content ?? '';
    this._selectedTemplate = value;
  }

  get canChangeSelectedTemplate(): boolean {
    return true;
  }

  needSendConfirmation: boolean = false;
  tryToSendEmail() {
    this.needSendConfirmation = true;
  }

  async cancelSending(): Promise<void> {
    this.needSendConfirmation = false;
  }

  async confirmSending(): Promise<void> {
    this.output.next(this.model);
  }

  async sendSampleEmail(): Promise<void> {
    await lastValueFrom(this._apiService.storeSendEmailRequest(this.model));
  }
}
