import {Modal} from "../classes/modal";
import {Subject} from "rxjs";
import {Component} from "@angular/core";

@Component({
  template: `
    <quill-view-html class="w-100percent" [content]="input ?? ''" />
  `,
  host: {
    style: 'overflow: auto;'
  }
})
export class EmailContentModal implements Modal<string, undefined> {
  input?: string;
  output!: Subject<undefined>;
}
