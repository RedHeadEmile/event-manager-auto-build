import {Component} from "@angular/core";
import {Subject} from "rxjs";
import {Modal} from "../classes/modal";

export interface QRCodeModalConfig {
    url: string,
    code: string
};

@Component({
    template: `
      <ng-container *ngIf="!!input">
        <qrcode elementType="svg" [qrdata]="input.url" [margin]="0" />
        <span>Code: {{ input.code }}</span>
      </ng-container>
    `,
  styles: [`
    :host {
      width: 40vw;
      max-width: 30rem;

      display: flex;
      flex-direction: column;
    }

    qrcode {
      display: flex;

      max-height: 100%;
      max-width: 100%;

      aspect-ratio: 1 / 1;
    }
  `]
})
export class QrcodeModal implements Modal<QRCodeModalConfig | undefined, undefined> {
    input?: QRCodeModalConfig;
    output!: Subject<undefined>;
}
