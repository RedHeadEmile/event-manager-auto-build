import {Component} from "@angular/core";
import {Modal} from "../classes/modal";
import {Subject} from "rxjs";

export type YesNoModalVariant = 'yes-no' | 'yes-cancel' | 'confirm-cancel';
export type YesNoModalConfig = {
  title: string,
  message: string,
  variant: YesNoModalVariant
};

@Component({
  template: `
    <h1 class="uppercase my-3">{{ title }}</h1>
    <div class="flex-column gap-5">
      <span>{{ message }}</span>
      <span class="flex justify-evenly">
        <button (click)="output.next(false)" class="button button-negatif">
            {{ cancelLabel }}
        </button>
        <button (click)="output.next(true)" class="button button-white">
          {{ confirmLabel }}
        </button>
      </span>

    </div>
  `,
  host: {
    class: 'position-relative flex-column align-center justify-center'
  },
  styles: [`
    :host {
      max-width: 100%;
      max-height: 100%;
    }
  `]
})
export class YesNoModal implements Modal<YesNoModalConfig, boolean> {
  title?: string;
  message?: string;

  cancelLabel?: string;
  confirmLabel?: string;

  set input(value: YesNoModalConfig | undefined) {
    this.title = value?.title;
    this.message = value?.message;

    switch (value?.variant) {
      case 'yes-no':
        this.cancelLabel = 'Non';
        this.confirmLabel = 'Oui';
        break;

      case 'yes-cancel':
        this.cancelLabel = 'Annuler';
        this.confirmLabel = 'Oui';
        break;

      case 'confirm-cancel':
        this.cancelLabel = 'Annuler';
        this.confirmLabel = 'Confirmer';
        break;
    }
  }

  output!: Subject<boolean | undefined>;
}
