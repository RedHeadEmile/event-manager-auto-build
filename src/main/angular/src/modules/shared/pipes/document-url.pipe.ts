import { Pipe, PipeTransform } from '@angular/core';
import {SettingsService} from "../services/settings.service";

@Pipe({
  name: 'documentUrl'
})
export class DocumentUrlPipe implements PipeTransform {

  constructor(private readonly _settingsService: SettingsService) {
  }

  transform(documentId: number, downloadUrl: boolean = false): string {
    return this._settingsService.API_BASE_URL + '/documents/' + documentId + '?download=' + downloadUrl;
  }

}
