import {Directive, forwardRef, Input} from "@angular/core";
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";

@Directive({
  selector: 'input[type=date][dateValue][minDate]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => MinDateDirective),
    multi: true
  }]
})
export class MinDateDirective implements Validator {

  private _minDate?: Date;

  @Input()
  set minDate(value: string | Date | undefined) {
    if (value === undefined) {
      this._minDate = undefined;
      return;
    }

    if (value instanceof Date)
      this._minDate = value;
    else
      this._minDate = new Date(value);

    this._onValidatorChange();
  }

  validate(control: AbstractControl): ValidationErrors | null {
    if (!control.value || !this._minDate)
      return null;

    if (!(control.value instanceof Date))
      return null;

    if (control.value < this._minDate)
      return { minDate: true };

    return null;
  }

  private _onValidatorChange: () => void = () => {};
  registerOnValidatorChange(fn: () => void) {
    this._onValidatorChange = fn;
  }
}
