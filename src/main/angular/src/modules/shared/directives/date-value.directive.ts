import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {Directive, ElementRef, forwardRef, HostListener, Renderer2} from "@angular/core";

@Directive({
  selector: 'input[type=date][dateValue]',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => DateValueDirective),
    multi: true
  }]
})
export class DateValueDirective implements ControlValueAccessor {
  @HostListener('input', ['$event.target.valueAsDate']) onChange = (_: any) => { };
  @HostListener('blur', []) onTouched = () => { };

  constructor(private _renderer: Renderer2, private _elementRef: ElementRef) { }

  writeValue(value: Date): void {
    this._renderer.setProperty(this._elementRef.nativeElement, 'valueAsDate', value);
  }

  registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
  registerOnTouched(fn: () => void): void { this.onTouched = fn; }

  setDisabledState(isDisabled: boolean): void {
    this._renderer.setProperty(this._elementRef.nativeElement, 'disabled', isDisabled);
  }
}
