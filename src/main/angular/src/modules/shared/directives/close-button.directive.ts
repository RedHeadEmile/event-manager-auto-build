import {Directive, ElementRef} from "@angular/core";

@Directive({
  selector: 'button[close-button], input[type=submit][close-button], input[type=button][close-button]',
  host: {
    '[class.close-button]': 'true'
  }
})
export class CloseButtonDirective {
  constructor() {}
}
