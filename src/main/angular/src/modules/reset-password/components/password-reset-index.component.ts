import {Component, OnInit} from "@angular/core";
import {ApiService, UserPasswordResetRequestViewModel} from "../../shared/services/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {lastValueFrom} from "rxjs";

@Component({
  templateUrl: './password-reset-index.component.html'
})
export class PasswordResetIndexComponent implements OnInit {
  code?: string;
  newPassword: string = "";

  passwordResetState: 'checking-validity' | 'invalid-code' | 'ready-for-submission' | 'success' = 'checking-validity';

  constructor(
    private readonly _apiService: ApiService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _router: Router
  ) {
  }

  async ngOnInit(): Promise<void> {
    this.code = this._activatedRoute.snapshot.queryParams['code']
    if (!this.code) {
      await this._router.navigate(['/']);
      return;
    }

    const response = await lastValueFrom(this._apiService.showPasswordResetTokenValidity(this.code));
    if (response.valid)
      this.passwordResetState = 'ready-for-submission';
    else
      this.passwordResetState = 'invalid-code';
  }

  async submit(): Promise<void> {
    if (!this.code)
      throw new Error("Token cannot be undefined at this state!");

    await lastValueFrom(this._apiService.storeUserPasswordReset(new UserPasswordResetRequestViewModel({ passwordResetToken: this.code, newPassword: this.newPassword })));
    this.passwordResetState = 'success';
  }
}
