import {Component} from '@angular/core';
import {Modal} from "../../shared/classes/modal";
import {lastValueFrom, Subject} from "rxjs";
import {ApiService} from "../../shared/services/api.service";
import {Router} from "@angular/router";

@Component({
  templateUrl: './event-leave.modal.html',
  host: {
    class: 'position-relative flex-column align-center justify-center'
  },
  styles: [`
    :host {
      max-width: 100%;
      max-height: 100%;
    }
  `]
})
export class EventLeaveModal implements Modal<number, undefined> {
  input?: number;
  output!: Subject<undefined>;

  constructor(
    private readonly _apiService: ApiService,
    private readonly _router: Router,
  ) {}

  async removeUserToEvent(): Promise<void> {
    if (this.input !== undefined) {
      await lastValueFrom(this._apiService.removeCurrentUserToEvent(this.input));
      await this._router.navigate(['/events']);
      this.output.next(undefined);
    }
  }

}
