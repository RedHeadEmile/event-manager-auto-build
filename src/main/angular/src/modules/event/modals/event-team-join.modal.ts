import {Modal} from "../../shared/classes/modal";
import {lastValueFrom, Subject} from "rxjs";
import {Component} from "@angular/core";
import {BadRequestUtils} from "../../shared/classes/badrequest-utils";
import {ApiService, ErrorViewModelDetail} from "../../shared/services/api.service";
import {Router} from "@angular/router";

@Component({
  templateUrl: './event-team-join.modal.html',
  host: {
    class: 'position-relative flex-column gap-5'
  },
})
export class EventTeamJoinModal implements Modal<undefined, undefined> {
  output!: Subject<undefined>;

  invitationCode: string = '';

  status: 'loading' | 'wrong-code' | 'already-in-a-team' = 'loading';

  constructor(
    private readonly _apiService: ApiService,
    private readonly _router: Router
  ) {
  }

  async join() {
    if (!/^[a-zA-Z0-9]{8}$/.test(this.invitationCode)) {
      this.status = 'wrong-code';
      return;
    }

    try {
      const team = await lastValueFrom(this._apiService.storeTeamJoinFromCode(this.invitationCode));
      await this._router.navigate(["/event", team.event.id, "teams", team.id]);
      this.output.next(undefined);
    }
    catch (error: any) {
      const viewModel = BadRequestUtils.asBadRequestException(error);
      switch (viewModel.detail)
      {
        case ErrorViewModelDetail.UNKNOWN_TEAM:
          this.status = 'wrong-code';
          break;

        case ErrorViewModelDetail.USER_ALREADY_IN_A_TEAM:
          this.status = 'already-in-a-team';
          break;
      }
    }
  }
}
