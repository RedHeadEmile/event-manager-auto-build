import {Modal} from "../../shared/classes/modal";
import {ApiService, EventViewModel, TeamStoreRequestViewModel} from "../../shared/services/api.service";
import {lastValueFrom, Subject} from "rxjs";
import {Component} from "@angular/core";
import {Router} from "@angular/router";

@Component({
  templateUrl: './event-team-create.modal.html',
  host: {
    class: 'position-relative flex-column gap-5'
  },
})
export class EventTeamCreateModal implements Modal<EventViewModel, undefined> {
  input?: EventViewModel;
  output!: Subject<undefined>;

  teamName: string = '';

  constructor(
    private readonly _apiService: ApiService,
    private readonly _router: Router
  ) {
  }

  async create() {
    this.teamName = this.teamName.trim();
    if (this.teamName.length == 0)
      return;

    if (!this.input)
      return;

    const team = await lastValueFrom(this._apiService.storeTeam(new TeamStoreRequestViewModel({
      event: this.input,
      name: this.teamName
    })));

    await this._router.navigate(['/event', this.input.id, "teams", team.id]);
    this.output.next(undefined);
  }

}
