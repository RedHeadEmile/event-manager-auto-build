import {Component} from '@angular/core';
import {Modal} from "../../shared/classes/modal";
import {lastValueFrom, Subject} from "rxjs";
import {ApiService} from "../../shared/services/api.service";
import {Router} from "@angular/router";

@Component({
  templateUrl: './event-delete.modal.html',
  host: {
    class: 'position-relative flex-column align-center justify-center gap-4'
  },
  styles: [`
    :host {
      max-width: 100%;
      max-height: 100%;
      width: 30rem;
      height: 10rem;
    }
  `]
})
export class EventDeleteModal implements Modal<number, undefined> {
  input?: number;
  output!: Subject<undefined>;

  constructor(
      private readonly _apiService: ApiService,
      private readonly _router: Router,
  ) {}

  async removeEvent(): Promise<void> {
    if (this.input !== undefined) {
      await lastValueFrom(this._apiService.deleteEvent(this.input));
      await this._router.navigate(['/events']);
      this.output.next(undefined);
    }
  }
}

