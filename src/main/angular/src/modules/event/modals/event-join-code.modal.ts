import {Component} from '@angular/core';
import {Modal} from "../../shared/classes/modal";
import {lastValueFrom, Subject} from "rxjs";
import {
  ApiException,
  ApiService,
  ErrorViewModel,
  ErrorViewModelDetail,
  EventCodeJoinedByUserIdRequestViewModel,
  EventViewModel
} from "../../shared/services/api.service";

@Component({
  templateUrl: './event-join-code.modal.html',
  host: {
    class: 'position-relative flex-column align-center justify-center'
  },
  styles: [`
    :host {
      max-width: 100%;
      max-height: 100%;
    }
  `]
})
export class EventJoinCodeModal implements Modal<undefined, undefined> {
  input?: undefined;
  output!: Subject<undefined>;

  code: string = "";
  errorMsgActivated: boolean = false;
  errorMsg: string = "";
  events: EventViewModel[] = [];


  constructor(
    private readonly _apiService: ApiService,
  ) {}

  async submit() {
    let joinParams = new EventCodeJoinedByUserIdRequestViewModel();
    joinParams.eventCode = this.code;

    try {
      await lastValueFrom(this._apiService.addCurrentUserToEventByCode(joinParams));
      this.events = await lastValueFrom(this._apiService.indexEventsJoinedByUser());
      this.output.next(undefined);
    }
    catch (error) {
      if (ApiException.isApiException(error)) {
        if (error.status === 400) {
          let variable = ErrorViewModel.fromJS(JSON.parse(error.response))
          if (variable.detail === ErrorViewModelDetail.EVENT_ALREADY_JOINED)
            this.errorMsg = "Vous avez déjà rejoint cet évènement.";
        } else
          this.errorMsg = "Vérifier que vous avez renseigné le bon code évènement";
      }
        this.errorMsgActivated = true;
    }
  }
}
