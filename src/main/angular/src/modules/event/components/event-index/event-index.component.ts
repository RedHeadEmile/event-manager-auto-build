import {Component, OnInit} from "@angular/core";
import {AuthenticationService} from "../../../shared/services/authentication.service";

@Component({
  templateUrl: './event-index.component.html',
  host: {
    class: 'content-width'
  },
})
export class EventIndexComponent implements OnInit {

  isModerator?: boolean = undefined;
  constructor(
    private readonly _authenticationService: AuthenticationService,
  ) {
  }

  ngOnInit(): void {
    this.isModerator = this._authenticationService.isCurrentUserModerator;
  }
}
