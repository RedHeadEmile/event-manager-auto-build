import {Component, Input, OnInit} from '@angular/core';
import {
  ApiService,
  EventViewModel,
  TeamLiteViewModel,
  TeamStoreRequestViewModel
} from "../../../../shared/services/api.service";
import {lastValueFrom} from "rxjs";
import {EventTeamJoinModal} from "../../../modals/event-team-join.modal";
import {Router} from "@angular/router";
import {ModalService} from "../../../../shared/services/modal.service";

@Component({
  selector: 'event-card',
  templateUrl: './event-card.component.html',
})
export class EventCardComponent implements OnInit{

  event: EventViewModel = new EventViewModel();
  isModerator: boolean = false;
  private _userTeams: TeamLiteViewModel[] = [];
  private _events: EventViewModel[] = [];

  constructor(
    private readonly _apiService: ApiService,
    private readonly _modalService: ModalService,
    private readonly _router: Router
  ) {
  }

  async ngOnInit(): Promise<void> {
    this._events = await lastValueFrom(this._apiService.indexEventsJoinedByUser());
    this._userTeams = await lastValueFrom(this._apiService.indexCurrentUserTeams(this._events.map(event => event.id!)));
  }

  @Input()
  set eventToDisplay(value: EventViewModel) {
    this.event = value;
  }

  @Input()
  set isModeratorUser(value: boolean) {
    this.isModerator = value;
  }


  /* USER */
  isPlayerAlreadyInTeam(eventId: number | undefined): boolean {
    return !!this._userTeams.find(team => team.eventId === eventId);
  }

  getPlayerTeamId(eventId: number | undefined): number | undefined {
    return this._userTeams.find(team => team.eventId === eventId)?.id;
  }

  async createTeam() {
    const team = await lastValueFrom(this._apiService.storeTeam(new TeamStoreRequestViewModel({ event: this.event })));
    await this._router.navigate(['/event', this.event.id, "teams", team.id]);
  }

  joinTeam() {
    this._modalService.openModal({
      component: EventTeamJoinModal
    });
  }

}
