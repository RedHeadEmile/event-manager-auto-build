import {Component, OnInit} from '@angular/core';
import {ApiService, EventViewModel} from "../../../shared/services/api.service";
import {lastValueFrom} from "rxjs";
import {ModalService} from "../../../shared/services/modal.service";
import {EventJoinCodeModal} from "../../modals/event-join-code.modal";

@Component({
  selector: 'event-index-user',
  templateUrl: './event-index-user.component.html'
})
export class EventIndexUserComponent implements OnInit {

  events: EventViewModel[] = [];

  constructor(
    private readonly _apiService: ApiService,
    private readonly _modalService: ModalService,
  ) {
  }

  async ngOnInit(): Promise<void> {
    this.events = await lastValueFrom(this._apiService.indexEventsJoinedByUser());
  }

  openJoinEventModal() {
    this._modalService.openModal({
      component: EventJoinCodeModal,
      onClose: async () => {
        this.events = await lastValueFrom(this._apiService.indexEventsJoinedByUser());
      }
    });
  }

}
