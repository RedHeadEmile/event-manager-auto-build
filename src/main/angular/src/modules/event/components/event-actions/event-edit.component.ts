import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ApiService, EventViewModel} from "../../../shared/services/api.service";
import {lastValueFrom} from "rxjs";
import {EventDeleteModal} from "../../modals/event-delete.modal";
import {ModalService} from "../../../shared/services/modal.service";

@Component({
  templateUrl: './event-edit.component.html',
  host: {
    class: 'content-width'
  },
})
export class EventEditComponent implements OnInit{

  eventViewModel: EventViewModel = new EventViewModel();

  constructor(
      private readonly _apiService: ApiService,
      private readonly _activatedRoute: ActivatedRoute,
      private readonly _router: Router,
      private readonly _modalService: ModalService,
  ) {
  }

  async ngOnInit(): Promise<void> {
    const eventId = this._activatedRoute.snapshot.paramMap.get('eventId');
    this.eventViewModel = await lastValueFrom(this._apiService.showEvent(Number(eventId)));

    if (eventId == null) {
      await this._router.navigate(['../'], {relativeTo: this._activatedRoute})
      return;
    }

    const numberRegex = /^\d+$/;
    if (!numberRegex.test(eventId))
      await this._router.navigate(['../'], { relativeTo: this._activatedRoute })
  }

  openDeleteEventModal() {
    this._modalService.openModal({
      data: this.eventViewModel.id,
      component: EventDeleteModal,
      onClose: () => {
        console.log('closed');
      }
    });
  }


}
