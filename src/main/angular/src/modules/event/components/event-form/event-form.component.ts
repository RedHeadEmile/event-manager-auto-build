import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {ApiService, EventDocumentRequirementViewModel, EventViewModel} from "../../../shared/services/api.service";
import {lastValueFrom} from "rxjs";
import {NgForm} from "@angular/forms";
import {ModalService} from "../../../shared/services/modal.service";

@Component({
  selector: 'event-form-component',
  templateUrl: './event-form.component.html',
  styleUrls: ['event-form.less'],
})
export class EventFormComponent implements OnInit {

  @ViewChild("fileInput", { static: true })
  fileInput!: ElementRef<HTMLInputElement>;

  allowTeam: boolean = false;
  errorMessage : string | undefined;

  _eventViewModel: EventViewModel = new EventViewModel();
  @Input()
  set eventViewModel(value: EventViewModel) {
    this._eventViewModel = value;
    if (this.eventViewModel.maximalTeamSize != undefined
      && this.eventViewModel.maximalTeamSize != 0) {
      this.allowTeam = true;
    }
  }
  get eventViewModel(): EventViewModel { return this._eventViewModel; }

  @ViewChild('myForm', { static: true }) private _myForm!: NgForm;

  constructor(
    private readonly _apiService: ApiService,
    private readonly _router: Router,
    private readonly _modalService: ModalService
  ) {
  }

  get isMyFormValid(): boolean {
    return this._myForm.valid ?? false;
  }

  get isEditingMode(): boolean {
    return !!this.eventViewModel.id;
  }

  async submit() {
    if (!this._myForm.valid)
      return;

    if(this.allowTeam) {
      if (this.eventViewModel.minimalTeamSize === undefined
        || this.eventViewModel.maximalTeamSize === undefined) {
        this.errorMessage = 'error';
        return;
      } else if (this.eventViewModel.minimalTeamSize > this.eventViewModel.maximalTeamSize) {
        this.errorMessage = 'error';
        return;
      }
    }
    else {
      this.eventViewModel.minimalTeamSize = 0;
      this.eventViewModel.maximalTeamSize = 0;
    }

    if (this.eventViewModel.id != null)
      await lastValueFrom(this._apiService.updateEvent(this.eventViewModel));
    else
      await lastValueFrom(this._apiService.createEvent(this.eventViewModel));

    await this._router.navigate(['/']);
  }

  ngOnInit() {
    if (this.eventViewModel.maximalTeamSize != undefined
        && this.eventViewModel.maximalTeamSize != 0) {
      console.log(this.eventViewModel.maximalTeamSize);
      //this.allowTeam = true;
    }
  }

  //#region DocumentRequirement
  addDocumentRequirement() {
    this.eventViewModel.documentRequirements = [
      ...this.eventViewModel.documentRequirements,
      new EventDocumentRequirementViewModel({
        name: 'Document ' + (this.eventViewModel.documentRequirements.length + 1),
        individual: false,
        optional: false
      })
    ];
  }

  removeDocumentRequirement(documentRequirement: EventDocumentRequirementViewModel) {
    this._modalService.openYesNoModal(
      'Supprimer le document',
      'Êtes-vous sûr de vouloir supprimer ce document ?',
      validated => {
        if (validated)
          this.eventViewModel.documentRequirements = this.eventViewModel.documentRequirements.filter(doc => doc !== documentRequirement);
      }
    )
  }

  //#region DocumentRequirementDocument
  addDocumentRequirementDocument(documentRequirement: EventDocumentRequirementViewModel) {
    this._fileConsumer = (fileContent, fileType) => {
      documentRequirement.documentId = undefined;
      documentRequirement.newDocumentContent = fileContent;
      documentRequirement.documentMimeType = fileType;
    };
    this.fileInput.nativeElement.click();
  }

  seeDocumentRequirementDocument(documentRequirement: EventDocumentRequirementViewModel) {
    this._modalService.openFileModal({
      documentId: documentRequirement.documentId,
      fileType: documentRequirement.documentMimeType,
      fileContent: documentRequirement.newDocumentContent
    });
  }

  deleteDocumentRequirementDocument(documentRequirement: EventDocumentRequirementViewModel) {
    documentRequirement.documentId = undefined;
    documentRequirement.newDocumentContent = undefined;
    documentRequirement.documentMimeType = undefined;
  }
  //#endregion
  //#endregion

  private _fileConsumer?: (fileContent: string, fileType: string) => void;
  onFileSelected(event: Event): void {
    if (!(event.target instanceof HTMLInputElement))
      return;

    const files = event.target.files;
    if (!files || files.length <= 0)
      return;

    const file = files[0];
    const reader = new FileReader();
    reader.onload = () => {
      if (!!this._fileConsumer)
        this._fileConsumer((reader.result as string).split(',')[1], file.type);
    };
    reader.readAsDataURL(file);
    event.target.value = '';
  }
}
