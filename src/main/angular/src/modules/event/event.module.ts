import {NgModule} from '@angular/core';
import {SharedModule} from "../shared/shared.module";
import {EventStatsModeratorComponent} from "./components/event-stats-moderator.component";
import {EventShowUserComponent} from "./components/event-show-user.component";
import {RouterModule} from "@angular/router";
import {EventShowComponent} from "./components/event-show.component";
import {EventIndexComponent} from "./components/event-index/event-index.component";
import {EventDeleteModal} from "./modals/event-delete.modal";
import {EventJoinEventModal} from "./modals/event-join-event.modal";
import {EventEditComponent} from './components/event-actions/event-edit.component';
import {EventCreateComponent} from "./components/event-actions/event-create.component";
import {EventFormComponent} from './components/event-form/event-form.component';
import {EventIndexUserComponent} from './components/event-index/event-index-user.component';
import {EventIndexModeratorComponent} from './components/event-index/event-index-moderator.component';
import {EventJoinCodeModal} from './modals/event-join-code.modal';
import {EventLeaveModal} from "./modals/event-leave.modal";
import {EventTeamJoinModal} from "./modals/event-team-join.modal";
import {EventCardComponent} from "./components/event-index/event-card-component/event-card.component";
import {administratorOrModeratorGuard} from "../shared/guards/administrator-or-moderator.guard";
import {EventEmailComponent} from "./components/event-email.component";


@NgModule({
  declarations: [
    EventIndexComponent,
    EventDeleteModal,
    EventJoinEventModal,
    EventShowComponent,
    EventStatsModeratorComponent,
    EventShowUserComponent,
    EventCreateComponent,
    EventEditComponent,
    EventEmailComponent,
    EventFormComponent,
    EventIndexUserComponent,
    EventIndexModeratorComponent,
    EventJoinCodeModal,
    EventLeaveModal,
    EventTeamJoinModal,
    EventCardComponent
  ],
  exports: [
    EventShowComponent
  ],
  imports: [
    SharedModule,

    RouterModule.forChild([
      {
        path: '',
        component: EventIndexComponent,
        pathMatch: 'full'
      },
      {
        path: 'event',
        children: [
          {
            canActivate: [administratorOrModeratorGuard],
            path: 'create',
            component: EventCreateComponent,
            pathMatch: 'full'
          },
          {
            path: ':eventId',
            children: [
              {
                canActivate: [administratorOrModeratorGuard],
                path: 'edit',
                component: EventEditComponent
              },
              {
                canActivate: [administratorOrModeratorGuard],
                path: 'emails',
                component: EventEmailComponent,
              },
              {
                canActivate: [administratorOrModeratorGuard],
                path: 'stats',
                component: EventStatsModeratorComponent,
              },
              {
                path: 'teams',
                loadChildren: () => import('../team/team.module').then(m => m.TeamModule)
              },
            ]
          }
        ]
      },
    ])
  ]
})
export class EventModule {
}
