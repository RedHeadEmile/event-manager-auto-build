import {Component, ElementRef, Input, OnChanges, SimpleChanges, ViewChild} from "@angular/core";
import {
  ApiService,
  ErrorViewModelDetail,
  EventDocumentRequirementViewModel,
  EventUserDocumentViewModel,
  TeamDocumentViewModel,
  TeamInvitationViewModel,
  TeamMemberViewModel,
  TeamNameUpdateRequestViewModel,
  TeamViewModel
} from "../../shared/services/api.service";
import {lastValueFrom} from "rxjs";
import {ModalService} from "../../shared/services/modal.service";
import {TeamInvitationModal} from "../modals/team-invitation.modal";
import {Router} from "@angular/router";
import {BadRequestUtils} from "../../shared/classes/badrequest-utils";
import {AuthenticationService} from "../../shared/services/authentication.service";
import {EventLeaveModal} from "../../event/modals/event-leave.modal";

@Component({
  selector: 'team-edit',
  templateUrl: './team-edit.component.html',
  host: {
    class: 'content-width'
  },
})
export class TeamEditComponent implements OnChanges {
  @ViewChild("fileInput", { static: true })
  fileInput!: ElementRef<HTMLInputElement>;

  @Input()
  team: TeamViewModel = new TeamViewModel();

  constructor(
      private readonly _apiService: ApiService,
      private readonly _modalService: ModalService,
      private readonly _router: Router,
      private readonly _authenticationService: AuthenticationService
  ) {
  }

  private _userDocuments: EventUserDocumentViewModel[] = [];

  async ngOnChanges(_: SimpleChanges) {
    if (!!this.team.event.id)
      this._userDocuments = await lastValueFrom(this._apiService.indexEventCurrentUserDocuments(this.team.event.id));
  }

  async deleteInvitation(invitation: TeamInvitationViewModel) {
    await lastValueFrom(this._apiService.deleteTeamInvitation(this.team.id, invitation.id!));
    this.team.invitations = this.team.invitations.filter(inv => inv !== invitation);
  }

  openInvitationModal() {
    this._modalService.openModal({
      component: TeamInvitationModal,
      data: this.team,
    });
  }

  leaveTeam() {
    this._modalService.openYesNoModal(
      'Quitter votre équipe',
      ' Êtes-vous sûr(e) de vouloir quitter l\'équipe ?',
      async validated => {
        if (validated) {
          await lastValueFrom(this._apiService.storeCurrentUserLeavingFromTeam(this.team.id));
          await this._router.navigate(['/']);
        }
      }
    );
  }


  leaveEventModal(eventId: number|undefined) {
    this._modalService.openModal({
      component: EventLeaveModal,
      data: eventId,
      onClose: (() => {})
    });
  }

  isMe(member: TeamMemberViewModel) {
    return member.email === this._authenticationService.currentUser?.email;
  }

  //#region DocumentRequirement
  isLoading: boolean = false;
  seeDocumentRequirementDocument(documentRequirement: EventDocumentRequirementViewModel) {
    this._modalService.openFileModal({
      documentId: documentRequirement.documentId,
      fileType: documentRequirement.documentMimeType
    });
  }

  //#region Team Document
  get moreThanOneTeamDocumentRequired(): boolean {
    return this.team.event.documentRequirements.filter(docReq => !docReq.individual).length > 0;
  }

  getTeamDocument(documentRequirement: EventDocumentRequirementViewModel): TeamDocumentViewModel | undefined {
    return this.team.documents.find(teamDoc => teamDoc.eventDocumentRequirementId === documentRequirement.id);
  }

  addTeamDocument(documentRequirement: EventDocumentRequirementViewModel) {
    this._fileConsumer = async (fileContent, fileType) => {
      this.isLoading = true;

      const teamDocument = await lastValueFrom(this._apiService.updateTeamDocument(this.team.id, new TeamDocumentViewModel({
        eventDocumentRequirementId: documentRequirement.id!,
        documentMimeType: fileType,
        newDocumentContent: fileContent
      })));
      this.team.documents = [...this.team.documents, teamDocument];
      this.isLoading = false;
    };
    this.fileInput.nativeElement.click();
  }

  seeTeamDocument(documentRequirement: EventDocumentRequirementViewModel) {
    const teamDocument = this.getTeamDocument(documentRequirement);
    if (!teamDocument)
      throw new Error('The team did not submit a document for this requirement yet');

    this._modalService.openFileModal({
      documentId: teamDocument.documentId,
      fileType: teamDocument.documentMimeType,
      fileContent: teamDocument.newDocumentContent
    });
  }

  async deleteTeamDocument(documentRequirement: EventDocumentRequirementViewModel) {
    this.isLoading = true;
    await lastValueFrom(this._apiService.updateTeamDocument(this.team.id, new TeamDocumentViewModel({
      eventDocumentRequirementId: documentRequirement.id!,
      documentId: undefined
    })));
    this.team.documents = this.team.documents.filter(doc => doc.eventDocumentRequirementId !== documentRequirement.id);
    this.isLoading = false;
  }
  //#endregion

  //#region Individual Document
  getUserDocument(documentRequirement: EventDocumentRequirementViewModel): EventUserDocumentViewModel | undefined {
    return this._userDocuments.find(doc => doc.eventDocumentRequirementId === documentRequirement.id);
  }

  addUserDocument(documentRequirement: EventDocumentRequirementViewModel) {
    this._fileConsumer = async (fileContent, fileType) => {
      this.isLoading = true;

      const userDocument = await lastValueFrom(this._apiService.updateEventCurrentUserDocument(this.team.event.id!, new EventUserDocumentViewModel({
        eventDocumentRequirementId: documentRequirement.id!,
        documentMimeType: fileType,
        newDocumentContent: fileContent
      })));
      this._userDocuments = [...this._userDocuments, userDocument];
      this.isLoading = false;
    };
    this.fileInput.nativeElement.click();
  }

  seeUserDocument(documentRequirement: EventDocumentRequirementViewModel) {
    const userDocument = this.getUserDocument(documentRequirement);
    if (!userDocument)
      throw new Error('The user did not submit a document for this requirement yet');

    this._modalService.openFileModal({
      documentId: userDocument.documentId,
      fileType: userDocument.documentMimeType,
      fileContent: userDocument.newDocumentContent
    });
  }

  async deleteUserDocument(documentRequirement: EventDocumentRequirementViewModel) {
    this.isLoading = true;
    await lastValueFrom(this._apiService.updateEventCurrentUserDocument(this.team.event.id!, new EventUserDocumentViewModel({
      eventDocumentRequirementId: documentRequirement.id!,
      documentId: undefined
    })));
    this._userDocuments = this._userDocuments.filter(doc => doc.eventDocumentRequirementId !== documentRequirement.id);
    this.isLoading = false;
  }
  //#endregion
  //#endregion

  //#region TeamName
  isTeamNameSaved: boolean = true;
  private _teamSaveNameTimeout: any;
  get teamName(): string | undefined {
    return this.team.name;
  }

  set teamName(value: string | undefined) {
    this.isTeamNameSaved = false;
    this.team.name = value;
    clearTimeout(this._teamSaveNameTimeout);
    this._teamSaveNameTimeout = setTimeout(async () => {
      await lastValueFrom(this._apiService.updateTeamName(this.team.id, new TeamNameUpdateRequestViewModel({
        newTeamName: value
      })));
      this.isTeamNameSaved = true;
    }, 2000);
  }
  //#endregion

  async validate() {
    try {
      await lastValueFrom(this._apiService.storeTeamValidation(this.team.id));
    }
    catch (error) {
      const detail = BadRequestUtils.asBadRequestException(error);
      if (detail.detail === ErrorViewModelDetail.MISSING_TEAM_REQUIREMENT) {
        alert('L\'équipe ne respecte pas les conditions nécessaire à l\'inscription.');
        return;
      }

      throw error;
    }
  }

  //#region FilePicker
  private _fileConsumer?: (fileContent: string, fileType: string) => void;
  onFileSelected(event: Event): void {
    if (!(event.target instanceof HTMLInputElement))
      return;

    const files = event.target.files;
    if (!files || files.length <= 0)
      return;

    const file = files[0];
    const reader = new FileReader();
    reader.onload = () => {
      if (!!this._fileConsumer)
        this._fileConsumer((reader.result as string).split(',')[1], file.type);
    };
    reader.readAsDataURL(file);
    event.target.value = '';
  }
  //#endregion
}
