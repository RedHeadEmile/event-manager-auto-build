import {Component, OnInit} from "@angular/core";
import {
  ApiException,
  ApiService,
  EventEmailViewModel,
  PlaceholderContextViewModel, TeamEmailViewModel
} from "../../shared/services/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {lastValueFrom} from "rxjs";
import {ModalService} from "../../shared/services/modal.service";

@Component({
  templateUrl: './team-email.component.html',
  host: {
    class: 'content-width'
  },
  styles: [`
    table {
      border: 1px solid black;
      border-collapse: collapse;
    }

    th, td {
      margin: 0;
      padding: .25rem;
      border-right: 1px solid black;
      border-bottom: 1px solid black;
    }

    tbody > tr:nth-child(2n + 1) {
      background-color: rgba(255, 255, 255, 0.4);
    }
  `]
})
export class TeamEmailComponent implements OnInit {

  teamId: number = -1;
  emails: TeamEmailViewModel[] = [];

  constructor(
    private readonly _apiService: ApiService,
    private readonly _modalService: ModalService,
    private readonly _route: ActivatedRoute,
    private readonly _router: Router
  ) {
  }

  async ngOnInit() {
    const teamId = this._route.snapshot.params['teamId'];
    if (!teamId || !/^\d+$/.test(teamId)) {
      await this._router.navigate(['/']);
      return;
    }

    const eventId = this._route.snapshot.params['eventId'];
    if (!eventId || !/^\d+$/.test(eventId)) {
      await this._router.navigate(['/']);
      return;
    }

    this.teamId = Number(teamId);
    this.emails = await lastValueFrom(this._apiService.indexTeamEmails(this.teamId, undefined, undefined));
  }

  writeNewEmail() {
    this._modalService.openEmailRedactionModal( PlaceholderContextViewModel.TEAM, async email => {
      if (!email)
        return;

      const historizedEmail = await lastValueFrom(this._apiService.storeTeamEmail(this.teamId, email));
      this.emails = [...this.emails, historizedEmail];
    });
  }

  seeEmailContent(email: EventEmailViewModel) {
    this._modalService.openEmailContentModal(email.content);
  }
}
