import {NgModule} from "@angular/core";
import {TeamEditComponent} from "./components/team-edit.component";
import {SharedModule} from "../shared/shared.module";
import {TeamShowComponent} from "./components/team-show.component";
import {RouterModule} from "@angular/router";
import {TeamInvitationModal} from "./modals/team-invitation.modal";
import {QRCodeModule} from "angularx-qrcode";
import {TeamIndexComponent} from "./components/team-index.component";
import {administratorOrModeratorGuard} from "../shared/guards/administrator-or-moderator.guard";
import {TeamDetailsModal} from "./modals/team-details.modal";
import {TeamEmailComponent} from "./components/team-email.component";

@NgModule({
  declarations: [
    TeamEditComponent,
    TeamEmailComponent,
    TeamIndexComponent,
    TeamShowComponent,

    TeamDetailsModal,
    TeamInvitationModal,
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        canActivate: [administratorOrModeratorGuard],
        path: '',
        pathMatch: 'full',
        component: TeamIndexComponent
      },
      {
        path: ':teamId',
        children: [
          {
            path: '',
            pathMatch: 'full',
            component: TeamShowComponent
          },
          {
            canActivate: [administratorOrModeratorGuard],
            path: 'emails',
            component: TeamEmailComponent
          }
        ]
      }
    ]),
    QRCodeModule
  ],
})
export class TeamModule {

}
