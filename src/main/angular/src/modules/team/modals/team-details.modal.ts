import {Modal} from "../../shared/classes/modal";
import {TeamViewModel} from "../../shared/services/api.service";
import {Component} from "@angular/core";
import {Subject} from "rxjs";

@Component({
  templateUrl: './team-details.modal.html'
})
export class TeamDetailsModal implements Modal<TeamViewModel, undefined> {
  input?: TeamViewModel;
  output!: Subject<undefined>;
}
