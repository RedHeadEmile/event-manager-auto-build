import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import {EmailTemplateIndexComponent} from "./components/email-template-index.component";

@NgModule(({
  declarations: [
    EmailTemplateIndexComponent
  ],
  imports: [
    SharedModule,

    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        component: EmailTemplateIndexComponent
      }
    ])
  ]
}))
export class EmailTemplateModule {}
