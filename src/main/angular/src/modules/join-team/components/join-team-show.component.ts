import {Component, OnInit} from "@angular/core";
import {ApiService, ErrorViewModelDetail} from "../../shared/services/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {lastValueFrom} from "rxjs";
import {BadRequestUtils} from "../../shared/classes/badrequest-utils";

@Component({
  template: `
    <ng-container *ngIf="status === 'loading'">Chargement...</ng-container>
    <ng-container *ngIf="status === 'wrong-code'">Code invalide</ng-container>
    <ng-container *ngIf="status === 'already-in-a-team'">You already are in a team for this event!</ng-container>
  `
})
export class JoinTeamShowComponent implements OnInit {
  status: 'loading' | 'wrong-code' | 'already-in-a-team' = 'loading';

  constructor(
    private readonly _apiService: ApiService,
    private readonly _route: ActivatedRoute,
    private readonly _router: Router
  ) {
  }

  async ngOnInit() {
    const teamCode = this._route.snapshot.params['teamCode'];
    if (teamCode == null || !/^[a-zA-Z0-9]{8}$/.test(teamCode)) {
      this.status = 'wrong-code';
      return;
    }

    try {
      const team = await lastValueFrom(this._apiService.storeTeamJoinFromCode(teamCode));
      await this._router.navigate(["/event", team.event.id, "teams", team.id]);
    }
    catch (error: any) {
      const viewModel = BadRequestUtils.asBadRequestException(error);
      switch (viewModel.detail)
      {
        case ErrorViewModelDetail.UNKNOWN_TEAM:
          this.status = 'wrong-code';
          break;

        case ErrorViewModelDetail.USER_ALREADY_IN_A_TEAM:
          this.status = 'already-in-a-team';
          break;
      }
    }
  }
}
