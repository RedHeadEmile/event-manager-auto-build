import {NgModule} from "@angular/core";
import {JoinTeamShowComponent} from "./components/join-team-show.component";
import {RouterModule} from "@angular/router";
import {SharedModule} from "../shared/shared.module";

@NgModule({
  declarations: [
    JoinTeamShowComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: ':teamCode',
        component: JoinTeamShowComponent
      }
    ])
  ]
})
export class JoinTeamModule {

}
