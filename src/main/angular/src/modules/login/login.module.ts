import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import {LoginIndexComponent} from "./components/login-index.component";

@NgModule({
    declarations: [
        LoginIndexComponent
    ],
    imports: [
        SharedModule,

        RouterModule.forChild([
            {
                path: '',
                pathMatch: 'full',
                component: LoginIndexComponent
            },
            {
                path: '**',
                redirectTo: '/'
            }
        ])
    ],
    exports: []
})
export class LoginModule { }
