import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../../shared/services/authentication.service";
import {
  ApiException,
  ApiService,
  UserMagicLinkRequestViewModel,
  UserPasswordResetCreationRequestViewModel
} from "../../shared/services/api.service";
import {lastValueFrom} from "rxjs";

@Component({
  templateUrl: './login-index.component.html',
  host: {
    class: 'content-width'
  },
})
export class LoginIndexComponent implements OnInit {
  email: string = "";
  password: string = "";

  errorMessage?: string;

  constructor(
    private readonly _apiService: ApiService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _authenticationService: AuthenticationService,
    private readonly _router: Router
  ) {
  }

  async ngOnInit(): Promise<void> {
    const code = this._activatedRoute.snapshot.queryParams['code'];
    const redirectUrl = this._activatedRoute.snapshot.queryParams['redirect-url'];
    if (code !== undefined) {
      await this._authenticationService.loginMagicLink(code);
      await this._router.navigate([!!redirectUrl ? redirectUrl : '/']);
    }
  }

  async tryToLog(): Promise<void> {
    try {
      await this._authenticationService.loginBasic(this.email, this.password);
      if (Object.keys(this._activatedRoute.snapshot.queryParams).includes('redirect-url'))
        await this._router.navigate([this._activatedRoute.snapshot.queryParams['redirect-url']]);
      else
        await this._router.navigate(['/']);
    }
    catch (error) {
      if (!ApiException.isApiException(error)) return;

      if (error.status === 401) {
        this.errorMessage = "Mauvais identifiants";
        if (Object.keys(error.headers).includes('x-eventmanager-remainingloginattempts')) {
          const remainingTries = error.headers['x-eventmanager-remainingloginattempts'];
          if (remainingTries > 0)
            this.errorMessage += `, il vous reste ${remainingTries} tentative${remainingTries > 1 ? 's' : ''}`;
          else
            this.errorMessage += ', veuillez patienter avant de réessayer ou utilisez un lien magic';
        }
        this.errorMessage += '.';
      }

      else if (error.status === 429) {
        this.errorMessage = 'Vous avez effectué trop de tentatives de connexion récemment, veuillez patienter';
        if (Object.keys(error.headers).includes('retry-after')) {
          const retryAfter = error.headers['retry-after'];
          const minutes = Math.floor(retryAfter / 60);
          const seconds = retryAfter - (minutes * 60);

          if (minutes > 0)
            this.errorMessage += ` ${minutes} minute${minutes > 1 ? 's' : ''} et`;
          this.errorMessage += ` ${seconds} seconde${seconds > 1 ? 's' : ''}`;
        }
        this.errorMessage += '.';
      }

      else
        throw error;
    }
  }

  async sendMagicLink(): Promise<void> {
    let redirectUrl = undefined;
    if (Object.keys(this._activatedRoute.snapshot.queryParams).includes('redirect-url'))
      redirectUrl = this._activatedRoute.snapshot.queryParams['redirect-url'];
    await lastValueFrom(this._apiService.storeUserMagicLinkRequest(new UserMagicLinkRequestViewModel({ email: this.email, redirectUrl: redirectUrl })));
  }

  async resetPassword(): Promise<void> {
    await lastValueFrom(this._apiService.storeUserPasswordResetCreation(new UserPasswordResetCreationRequestViewModel({email: this.email})));
  }
}
