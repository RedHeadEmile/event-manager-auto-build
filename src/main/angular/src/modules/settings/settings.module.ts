import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import {SettingsIndexComponent} from "./components/settings-index.component";
import {NgOptimizedImage} from "@angular/common";

@NgModule({
  declarations: [
    SettingsIndexComponent
  ],
    imports: [
        SharedModule,

        RouterModule.forChild([
            {
                path: '',
                pathMatch: 'full',
                component: SettingsIndexComponent
            }
        ]),
        NgOptimizedImage
    ],
  exports: []
})
export class SettingsModule { }
