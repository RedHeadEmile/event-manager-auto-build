import {Component, OnInit} from "@angular/core";
import {ApiService, UserViewModel} from "../../shared/services/api.service";
import {lastValueFrom} from "rxjs";
import {ModalService} from "../../shared/services/modal.service";

@Component({
  templateUrl: './user-index.component.html',
  host: {
    class: 'content-width'
  },
  styles: [`
    table {
      border: 1px solid black;
      border-collapse: collapse;
    }

    th, td {
      margin: 0;
      padding: .25rem;
      border-right: 1px solid black;
      border-bottom: 1px solid black;
    }

    tbody > tr:nth-child(2n + 1) {
      background-color: rgba(255, 255, 255, 0.4);
    }
  `]
})
export class UserIndexComponent implements OnInit {
  users: UserViewModel[] = [];

  constructor(
    private readonly _apiService: ApiService,
    private readonly _modalService: ModalService
  ) {
  }

  async ngOnInit(): Promise<void> {
    this.users = await lastValueFrom(this._apiService.indexUsers(undefined, undefined, undefined, undefined, undefined));
  }

  private _updateTimeout: any;
  private _updateList(): void {
    clearTimeout(this._updateTimeout);
    this._updateTimeout = setTimeout(async () => {
      let requiredPermissions = 0;
      if (this.mustBeAdmin) requiredPermissions |= 1;
      if (this.mustBeModerator) requiredPermissions |= 2;

      this.users = await lastValueFrom(this._apiService.indexUsers(this._query, requiredPermissions, this._mustBeEmailConfirmed, undefined, undefined));
    }, 300);

  }

  //#region Query
  private _query?: string;
  get query(): string | undefined { return this._query; }
  set query(value: string | undefined) {
    this._query = value;
    this._updateList();
  }
  //#endregion

  //#region Must Be Admin
  private _mustBeAdmin: boolean = false;
  get mustBeAdmin(): boolean { return this._mustBeAdmin; }
  set mustBeAdmin(value: boolean) {
    this._mustBeAdmin = value;
    this._updateList();
  }
  //#endregion

  //#region Must Be Moderator
  private _mustBeModerator: boolean = false;
  get mustBeModerator(): boolean { return this._mustBeModerator; }
  set mustBeModerator(value: boolean) {
    this._mustBeModerator = value;
    this._updateList();
  }
  //#endregion

  //#region Email Confirmation
  private _mustBeEmailConfirmed?: boolean;
  get mustBeEmailConfirmed(): boolean | undefined { return this._mustBeEmailConfirmed; }
  set mustBeEmailConfirmed(value: boolean | undefined) {
    this._mustBeEmailConfirmed = value;
    this._updateList();
  }
  //#endregion

  //#region Admin Grant
  isUserAdmin(user: UserViewModel): boolean {
    return (user.permission & 1) > 0;
  }

  async grantAdminPermission(user: UserViewModel): Promise<void> {
    if (this.isUserAdmin(user))
      throw new Error('User is already an administrator');

    this._modalService.openYesNoModal(
      'Êtes-vous sûr ?',
      'Une fois promu, un administrateur ne peut pas être rétrograder.',
      async validated => {
        if (validated) {
          await lastValueFrom(this._apiService.storeUserGrantAdministrator(user.id));
          user.permission |= 1;
        }
      }
    );
  }
  //#endregion

  //#region Moderator Grant
  isUserModerator(user: UserViewModel): boolean {
    return (user.permission & 2) > 0;
  }

  async grantModeratorPermission(user: UserViewModel): Promise<void> {
    if (this.isUserModerator(user))
      throw new Error('User is already a moderator');

    await lastValueFrom(this._apiService.storeUserGrantModerator(user.id));
    user.permission |= 2;
  }

  async revokeModeratorPermission(user: UserViewModel): Promise<void> {
    if (!this.isUserModerator(user))
      throw new Error('User is not a moderator');

    await lastValueFrom(this._apiService.storeUserRevokeModerator(user.id));
    user.permission &= ~2;
  }
  //#endregion

  //#region Confirmation Email
  async sendConfirmationEmail(user: UserViewModel): Promise<void> {
    if (user.emailConfirmed)
      throw new Error('Email already confirmed');

    await lastValueFrom(this._apiService.storeUserConfirmationEmail(user.id));
  }
  //#endregion
}
