import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import {UserIndexComponent} from "./components/user-index.component";

@NgModule({
  declarations: [
    UserIndexComponent
  ],
  imports: [
    SharedModule,

    RouterModule.forChild([
      {
        path: '',
        component: UserIndexComponent,
        pathMatch: 'full'
      }
    ])
  ]
})
export class UserModule {}
