import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {RegisterIndexComponent} from "./components/register-index.component";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    RegisterIndexComponent
  ],
  imports: [
    SharedModule,

    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        component: RegisterIndexComponent
      },
      {
        path: '**',
        redirectTo: '/'
      }
    ])
  ],
  exports: []
})
export class RegisterModule { }
