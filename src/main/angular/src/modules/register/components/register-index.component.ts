import {Component} from "@angular/core";
import {UserRegistrationRequestViewModel} from "../../shared/services/api.service";
import {AuthenticationService} from "../../shared/services/authentication.service";
import {Router} from "@angular/router";

@Component({
  templateUrl: './register-index.component.html',
  host: {
    class: 'content-width'
  },
})
export class RegisterIndexComponent {

  email: string = "";
  firstName: string = "";
  lastName: string = "";
  password: string = "";

  constructor(
    private readonly _authenticationService: AuthenticationService,
    private readonly _router: Router) {
  }

  async submit() {
    await this._authenticationService.register(new UserRegistrationRequestViewModel({
      email: this.email,
      firstName: this.firstName,
      lastName: this.lastName,
      password: this.password
    }));

    await this._router.navigate(['/']);
  }
}
