import {Component, OnInit} from '@angular/core';
import {ApiService, EventViewModel} from "../../shared/services/api.service";
import {lastValueFrom} from "rxjs";
import {AuthenticationService} from "../../shared/services/authentication.service";

@Component({
  selector: 'home-index-component',
  templateUrl: './home-index.component.html',
})
export class HomeIndexComponent implements OnInit {

  events: EventViewModel[] = [];
  isModerator?: boolean = undefined;

  constructor(
    private readonly _apiService: ApiService,
    private readonly _authenticationService: AuthenticationService) {
  }
  async ngOnInit(): Promise<void> {
    this.isModerator = this._authenticationService.isCurrentUserModerator;
    if (this.isModerator)
      this.events = await lastValueFrom(this._apiService.indexEventsToModerator());
    else if (!this.isModerator)
      this.events = await lastValueFrom(this._apiService.indexEventsJoinedByUser());
  }

}
