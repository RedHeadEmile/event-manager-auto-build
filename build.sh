#!/bin/sh
set -e

chmod +x gradlew

(./gradlew bootJar --parallel) &
JAVA_PID="$!"

(cd src/main/angular && npm ci --no-audit --progress=false && ng build --configuration=production) &
ANGULAR_PID="$!"

set +e
wait $JAVA_PID
JAVA_EXIT_CODE="$?"

wait $ANGULAR_PID
ANGULAR_EXIT_CODE="$?"
set -e

if [ $JAVA_EXIT_CODE != 0 ]; then
  exit $JAVA_EXIT_CODE

elif [ $ANGULAR_EXIT_CODE != 0 ]; then
  exit $ANGULAR_EXIT_CODE

fi

echo Copying files...
mkdir -p output/clientapp
cp -r src/main/angular/dist/* output/clientapp

mkdir output/serverapp
cp build/libs/*.jar output/serverapp/EventManager.jar
cp -r src/main/resources/migrations output/serverapp/migrations

printf "FROM openjdk:21\n\
ENV JAR_FILE=\"\"\n\
ENV JAVA_OPTS=\"\"\n\
WORKDIR /usr/src/myapp\n\
CMD [\"sh\", \"-c\", \"java -jar \$JAR_FILE \$JAVA_OPTS\"]\n"\
> output/Dockerfile

printf "RewriteEngine On\n\
RewriteCond %%{DOCUMENT_ROOT}%%{REQUEST_URI} -f [OR]\n\
RewriteCond %%{DOCUMENT_ROOT}%%{REQUEST_URI} -d\n\
RewriteRule ^ - [L]\n\
RewriteRule ^ /index.html\n"\
> output/clientapp/.htaccess

printf "version: '3.4'\n\
name: eventmanager\n\
services:\n\
  eventmanager-backend:\n\
    build: ./\n\
    container_name: eventmanager-backend\n\
    restart: always\n\
    ports:\n\
      - \"8085:8080\"\n\
    environment:\n\
      - JAR_FILE=EventManager.jar\n\
    volumes:\n\
      - ./serverapp:/usr/src/myapp\n\
    stdin_open: true\n\
    tty: true\n"\
> output/docker-compose.yml

mv output/* /output

echo Thanks, Good Bye!
